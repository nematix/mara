<?php

/**
 * Films model config
 */

return array(

	'title' => 'Codes',

	'single' => 'code',

	'model' => 'Code',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
            'title' => 'Code'
        ),
		'value' => array(
			'title' => 'Value'
		),
	),

	/**
	 * The filter set
	 */
	'filters' => array(
        'parent' => array(
            'type' => 'relationship',
            'title' => 'Code Category',
            'name_field' => 'value',
            'options_filter' => function($query){
                $query->where('depth',0);
            }
        )
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
        'name' => array(
            'title' => 'Key'
        ),
		'value' => array(
            'title' => 'Value'
        ),
	),

);