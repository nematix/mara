<?php

/**
 * Films model config
 */

return array(

	'title' => 'Permissions',

	'single' => 'permission',

	'model' => 'Permission',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
            'title' => 'Name'
        ),
        'display_name' => array(
            'title' => 'Description'
        )
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name' => array(
            'title' => 'Name'
        ),
        'display_name' => array(
            'title' => 'Description'
        )
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name'=> array(
            'title' => 'Name'
        ),
        'display_name' => array(
            'title' => 'Description'
        ),
        'roles' => array(
            'title' => 'Roles',
            'type' => 'relationship',
            'name_field' => 'name',
        )
	),

);