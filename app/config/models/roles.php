<?php

/**
 * Films model config
 */

return array(

	'title' => 'Roles',

	'single' => 'role',

	'model' => 'Role',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
            'title' => 'Role Name'
        )
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name'
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name',
        'perms' => array(
            'title' => 'Permissions',
            'type' => 'relationship',
            'name_field' => 'display_name',
        )
	),

);