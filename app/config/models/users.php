<?php

/**
 * Actors model config
 */

return array(

	'title' => 'Users',

	'single' => 'user',

	'model' => 'User',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
        'email' => array(
            'title' => 'Email'
        ),
        'first_name' => array(
            'title' => 'First Name'
        ),
        'last_name' => array(
            'title' => 'Last Name'
        ),
        'roles' => array(
            'title' => 'Roles',
            'relationship' => 'roles',
            'select' => "(:table).name",
        )
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'first_name' => array(
			'title' => 'First Name',
		),
		'last_name' => array(
			'title' => 'Last Name',
		),
        'groups' => array(
            'type' => 'relationship',
            'title' => 'Groups',
            'name_field' => 'name'
        ),
        'roles' => array(
            'type' => 'relationship',
            'title' => 'Roles',
            'name_field' => 'name'
        )
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'first_name' => array(
			'title' => 'First Name',
			'type' => 'text',
		),
		'last_name' => array(
			'title' => 'Last Name',
			'type' => 'text',
		),
        'groups' => array(
            'title' => 'Groups',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'roles' => array(
            'title' => 'Roles',
            'type' => 'relationship',
            'name_field' => 'name',
        ),
        'activated' => array(
            'title' => 'Activated',
            'type' => 'bool',
        ),
	),


    'link' => function($model)
    {
        return URL::to('user/profile', array($model->id));
    }

);