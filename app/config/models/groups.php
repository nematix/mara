<?php

/**
 * Films model config
 */

return array(

	'title' => 'Groups',

	'single' => 'group',

	'model' => 'Group',

	/**
	 * The display columns
	 */
	'columns' => array(
		'id',
		'name' => array(
            'title' => 'Group Name'
        ),
		'permissions' => array(
			'title' => 'Permissions'
		),
		'users' => array(
			'title' => 'Users',
			'relationship' => 'users',
			'select' => 'COUNT((:table).id)',
		)
	),

	/**
	 * The filter set
	 */
	'filters' => array(
		'id',
		'name'
	),

	/**
	 * The editable fields
	 */
	'edit_fields' => array(
		'name',
        'permissions'
	),

);