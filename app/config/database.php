<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| PDO Fetch Style
	|--------------------------------------------------------------------------
	|
	| By default, database results will be returned as instances of the PHP
	| stdClass object; however, you may desire to retrieve records in an
	| array format for simplicity. Here you can tweak the fetch style.
	|
	*/

	'fetch' => PDO::FETCH_CLASS,

	/*
	|--------------------------------------------------------------------------
	| Default Database Connection Name
	|--------------------------------------------------------------------------
	|
	| Here you may specify which of the database connections below you wish
	| to use as your default connection for all database work. Of course
	| you may use many connections at once using the Database library.
	|
	*/

	'default' => 'mysql',

	/*
	|--------------------------------------------------------------------------
	| Database Connections
	|--------------------------------------------------------------------------
	|
	| Here are each of the database connections setup for your application.
	| Of course, examples of configuring each database platform that is
	| supported by Laravel is shown below to make development simple.
	|
	|
	| All database work in Laravel is done through the PHP PDO facilities
	| so make sure you have the driver for your particular database of
	| choice installed on your machine before you begin development.
	|
	*/

	'connections' => array(

		'sqlite' => array(
			'driver'   => 'sqlite',
			'database' => __DIR__.'/../database/production.sqlite',
			'prefix'   => '',
		),

		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'dbmysql02.mara.gov.my',
			'database'  => 'ed_ependahuluan',
			'username'  => 'eadv',
			'password'  => 'passme123',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'mysql_album' => array(
			'driver'    => 'mysql',
			'host'      => 'dbmysql02.mara.gov.my',
			'database'  => 'Album',
			'username'  => 'eadv',
			'password'  => 'passme123',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

		'mysql_dbblack' => array(
			'driver'    => 'mysql',
			'host'      => 'dbmysql02.mara.gov.my',
			'database'  => 'ed_dblack',
			'username'  => 'eadv',
			'password'  => 'passme123',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

        'mysql_legacy' => array(
            'driver'    => 'mysql',
            'host'      => 'dbmysql01.mara.gov.my',
            'database'  => 'ed_ependahuluan',
            'username'  => 'eadv',
            'password'  => 'passme123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'sql_legacy' => array(
            'driver'   => 'odbc',
            'dsn'      => 'Driver=FreeTDS;Server=10.1.100.59;PORT=1433;DATABASE=ed_ependahuluan;UID=sa;PWD=passme123;TDS_Version=8.0;',
            'grammar'  => 'SqlServerGrammar',
            'username' => 'sa',
            'password' => 'passme123',
            'database' => 'ed_ependahuluan',
            'charset'  => 'utf8',
            'prefix'   => 'prv_',
        ),

        'sql_legacy_current' => array(
            'driver'   => 'odbc',
            'dsn'      => 'Driver=FreeTDS;Server=10.1.100.59;PORT=1433;DATABASE=ed_ependahuluan;UID=sa;PWD=passme123;TDS_Version=8.0;',
            'grammar'  => 'SqlServerGrammar',
            'username' => 'sa',
            'password' => 'passme123',
            'database' => 'ed_ependahuluan',
            'charset'  => 'utf8'
        ),

		'pgsql' => array(
			'driver'   => 'pgsql',
			'host'     => 'localhost',
			'database' => 'database',
			'username' => 'root',
			'password' => '',
			'charset'  => 'utf8',
			'prefix'   => '',
			'schema'   => 'public',
		),

        'smbm' => array(
            'driver'    => 'odbc',
            'dsn'       => 'Driver=3kodbc;ImageDatabase0=:8105/DBPELJ,,1,8&;ServerType=1;Server=10.1.100.67;ServerPort=30007;',
            'grammar'   => 'DB2',
            'username'  => 'oprn033',
            'password'  => 'oprn033',
            'database'  => '',
        ),
	),

	/*
	|--------------------------------------------------------------------------
	| Migration Repository Table
	|--------------------------------------------------------------------------
	|
	| This table keeps track of all the migrations that have already run for
	| your application. Using this information, we can determine which of
	| the migrations on disk have not actually be run in the databases.
	|
	*/

	'migrations' => 'migrations',

	/*
	|--------------------------------------------------------------------------
	| Redis Databases
	|--------------------------------------------------------------------------
	|
	| Redis is an open source, fast, and advanced key-value store that also
	| provides a richer set of commands than a typical key-value systems
	| such as APC or Memcached. Laravel makes it easy to dig right in.
	|
	*/

	'redis' => array(

		'cluster' => false,

		'default' => array(
			'host'     => 'redis',
			'port'     => 6379,
			'database' => 0,
		),

	),

);
