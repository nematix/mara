<?php

return array(
	'connections' => array(
        'mysql' => array(
            'driver'    => 'mysql',
            'host'      => 'db',
            'database'  => 'ed_ependahuluan',
            'username'  => 'eadv',
            'password'  => 'passme123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'mysql_album' => array(
            'driver'    => 'mysql',
            'host'      => 'db',
            'database'  => 'Album',
            'username'  => 'eadv',
            'password'  => 'passme123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'mysql_dbblack' => array(
            'driver'    => 'mysql',
            'host'      => 'db',
            'database'  => 'ed_dblack',
            'username'  => 'eadv',
            'password'  => 'passme123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'mysql_legacy' => array(
            'driver'    => 'mysql',
            'host'      => 'db',
            'database'  => 'ed_ependahuluan',
            'username'  => 'eadv',
            'password'  => 'passme123',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),
	),

    'redis' => array(
        'cluster' => false,
        'default' => array(
            'host' => 'redis',
            'port' => 6379,
            'database' => 0,
        ),
    ),
);
