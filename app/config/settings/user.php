<?php

return array(
	'title' => 'User Administration Setting',

	'edit_fields' => array(
		'role_staff_officer' => array(
			'title' => 'Staff Officer Role Name',
			'type' => 'text'
		),
        'role_staff_manager' => array(
            'title' => 'Staff Manager Role Name',
            'type' => 'text'
        ),
        'role_student' => array(
            'title' => 'Student Role Name',
            'type' => 'text'
        ),
	),

	'rules' => array(
		'role_staff_officer' => 'required|max:255',
		'role_staff_manager' => 'required|max:255',
        'role_student' => 'required|max:255',
	),

	'before_save' => function(&$data){
	},

	'permission'=> function()
	{
        return true;
	},

	'actions' => array(
	),
);