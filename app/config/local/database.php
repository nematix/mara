<?php

return array(
	'connections' => array(
		'mysql' => array(
			'driver'    => 'mysql',
			'host'      => 'localhost',
			'database'  => 'mara',
			'username'  => 'root',
			'password'  => '',
			'charset'   => 'utf8',
			'collation' => 'utf8_unicode_ci',
			'prefix'    => '',
		),

        'mysql_album' => array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'Album',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'mysql_dbblack' => array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'ep_dbblack',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

        'mysql_legacy' => array(
            'driver'    => 'mysql',
            'host'      => 'localhost',
            'database'  => 'ed_ependahuluan',
            'username'  => 'root',
            'password'  => '',
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ),

	),

    'redis' => array(
        'cluster' => false,
        'default' => array(
            'host' => '127.0.0.1',
            'port' => 6379,
            'database' => 0,
        ),
    ),
);
