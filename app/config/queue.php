<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Default Queue Driver
	|--------------------------------------------------------------------------
	|
	| The Laravel queue API supports a variety of back-ends via an unified
	| API, giving you convenient access to each back-end using the same
	| syntax for each one. Here you may set the default queue driver.
	|
	| Supported: "sync", "beanstalkd", "sqs", "iron"
	|
	*/

	'default' => 'redis',

	/*
	|--------------------------------------------------------------------------
	| Queue Connections
	|--------------------------------------------------------------------------
	|
	| Here you may configure the connection information for each server that
	| is used by your application. A default configuration has been added
	| for each back-end shipped with Laravel. You are free to add more.
	|
	*/

	'connections' => array(

		'sync' => array(
			'driver' => 'sync',
		),

		'beanstalkd' => array(
			'driver' => 'beanstalkd',
			'host'   => 'localhost',
			'queue'  => 'default',
		),

		'sqs' => array(
			'driver' => 'sqs',
			'key'    => 'AKIAJKCQ4JX67OXN2VFQ',
			'secret' => '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',
			'queue'  => 'https://sqs.ap-northeast-1.amazonaws.com/247393346646/mara',
			'region' => 'ap-northeast-1',
		),

		'iron' => array(
			'driver'  => 'iron',
			'project' => 'your-project-id',
			'token'   => 'your-token',
			'queue'   => 'your-queue-name',
		),

        'redis' => array(
            'driver' => 'redis',
            'queue' => 'default',
        ),

    ),

    'failed' => array(
        'database' => 'mysql', 'table' => 'failed_jobs',
    ),

);
