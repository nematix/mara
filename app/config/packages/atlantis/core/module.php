<?php return [

    /*
    |--------------------------------------------------------------------------
    | Initial & default module to load
    |--------------------------------------------------------------------------
    |
    |
    */
    'modules' => [
        'advance' => [
            'enable' => true,
            'provider' => 'AdvanceServiceProvider'
        ],
        'mara' => [
            'enable' => true,
            'provider' => 'MaraServiceProvider'
        ]
    ]

];