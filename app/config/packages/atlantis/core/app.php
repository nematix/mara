<?php return array(

    'theme' => array(
        'default'       => 'default',
        'current'       => 'core',
        'base_path'     => public_path() . '/themes'
    ),

    'copyright' => '<a href="http://mara.gov.my">Majlis Amanah Rakyat</a>',

);