<?php return array(

    'admin' => array(
        'System Settings' => array('settings.site','settings.user'),
        'Users' => array('users','groups','roles','permissions'),
        'Code Library' => array('codes')
    ),

    'sidebar' => array(
        'applications' => array(
            'advance' => array(
                'title' => 'E-Pendahuluan',
                'route' => 'application',
                'parameter' => array(),
                'items' => array(
                    'advance.manage.staff' => trans('advance::advance.manage.staff'),
                    'advance.manage.budget' => trans('advance::advance.manage.budget'),
                    'advance.student.new' => trans('advance::advance.student.new'),
                    'advance.student.manage' => trans('advance::advance.student.manage'),
                    'advance.staff.browse' => trans('advance::advance.staff.browse'),
                    'advance.staff.manage.review' => trans('advance::advance.staff.manage.review'),
                    'advance.staff.manage.approve' => trans('advance::advance.staff.manage.approve'),
                    'advance.staff.manage.payment' => trans('advance::advance.staff.manage.payment'),
                    'advance.staff.statistic' => trans('advance::advance.staff.statistic')
                )
            ),
            'message' => array(
                'title' => trans('message::message.manage.list'),
                'route' => 'message',
                'parameter' => array(),
                'icon' => 'envelope',
                'items' => array(
                    'message.manage.list' => trans('message::message.manage.list'),
                )
            )
        ),
    ),

);