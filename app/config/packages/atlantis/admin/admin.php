<?php

return array(
    'title' => 'E-Pendahuluan Administration',

	'permissions' => array(),

    'workflow' => array(
        'status' => array(
            'action' => array(
                0 => array(1,2),    // Baru                 > Layak, Tidak Layak
                1 => array(3),      // Layak                > Terima
                2 => array(1),
                3 => array(4,5,6),  // Terima               > Sokong, Tidak Sokong, Dalam Pertimbangan
                4 => array(5,8),    // Sokong               > Tidak Sokong, Tidak Lulus
                5 => array(),       // Tidak Sokong
                6 => array(7,8),    // Dalam Pertimbangan   > Lulus, Tidak Lulus
                7 => array(),
                8 => array()
            ),
        ),
        'condition' => array(
            'action' => array(
                0 => array(
                    1 => 'documents.$valid',
                    2 => 'documents.$valid'
                ),
                1 => array(
                    3 => 'documents.$valid'
                )
            ),
            'disable' => array(
                'form' => 'detail.status.raw >= 3'
            )
        )
    ),

);
