<?php

use Illuminate\Support\Facades\Validator;


class People extends Atlantis\User\Models\People {

    protected $append = array('full_name');
    /**
     * Relation : Detail
     *
     * @return mixed
     */
    public function detail(){
        return $this->belongsTo('Detail','id');
    }


    /**
     * Check for profile validation
     *
     * @return bool
     */
    public function getIsCompletedAttribute(){
        /** @var $validation Validator Validation object */
        $validation = Validator::make(
            $this->attributes,
            array(
                'idno_ic' => 'required',
                'gender' => 'required',
                'birth_date' => 'required',
                'birth_place' => 'required',
                'address_street' => 'required',
                'address_postcode' => 'required',
                'address_state' => 'required',
                'contact_home' => 'required',
                'contact_mobile' => 'required'
            )
        );

        /** Check for validation */
        if( $validation->fails() ){
            return false;
        }

        return true;
    }


    /**
     * full_name attribute accessor
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $middle = ' ';

        if( isset($this->profile) ){
            $middle_gender = ( $this->profile->gender == 'male' ? ' Bin ' : ' Binti ');
            if( !empty($this->last_name) && $this->profile->race == 'P' ) $middle = $middle_gender;
        }

        return $this->getAttribute('first_name').$middle.$this->getAttribute('last_name');
    }

}