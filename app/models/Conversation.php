<?php


class Conversation extends Atlantis\Message\Model\Conversation {

    /**
     * Create Message Broadcast
     *
     * @param $subject
     * @param $body
     */
    public function createMessageBroadcast($subject,$body){
        try{
            $user = Sentry::getUser();
            if( !$user->isSuperUser() ) throw new Exception('Broadcast message need a super user permission');

            $conversation = Conversation::create(array('subject' => $subject));
            Participant::create(array(
                'conversation_id' => $conversation->id,
                'user_id' => $user->id
            ));
            $message = Message::create([
                'conversation_id' => $conversation->id,
                'user_id' => 0,
                'body' => $body
            ]);
            $message->meta = array(
                'type' => 'broadcast'
            );
            $message->save();

        }catch(Exception $e){

        }
    }

}