<?php


class User extends Atlantis\User\Models\User {

    /**
     * Boot
     */
    protected static function boot()
    {
        parent::boot();

        static::deleting(function($user){
            $user->details()->delete();
        });
    }


    /**
     * Relation : Detail
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function details(){
        return $this->hasMany('Record','user_id','id');
    }

}