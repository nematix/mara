<?php

namespace People;

use Atlantis\Core\Model\BaseModel;


class Student extends BaseModel {

    /** @var string table name */
    protected $table = 'people_students';

    /** @var array attributes appends */
    protected $guarded = array('id','created_at','updated_at');


    /**
     * Relation : People
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function people(){
        return $this->morphOne('People','data');
    }

}