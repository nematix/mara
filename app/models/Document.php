<?php

use Atlantis\Document\Model\Document as BaseModel;


class Document extends BaseModel {

    /**
     * Relation : Detail
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detail(){
        return $this->belongsTo('Record');
    }

}