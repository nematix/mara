<?php


class Message extends Atlantis\Message\Model\Message {

    /** @var array appends */
    protected $appends = array('created_when');


    /** Relation : User */
    public function user(){
        return $this->belongsTo('User');
    }

}