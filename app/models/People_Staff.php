<?php

namespace People;

use Atlantis\Core\Model\BaseModel;
use Code;


class Staff extends BaseModel {

    /** @var string table name */
    protected $table = 'people_staffs';

    /** @var array attributes appends */
    protected $appends = ['center_name'];


    /**
     * Relation : People
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function people(){
        return $this->morphOne('People','data');
    }


    /**
     * Get Center Name
     *
     * @return mixed
     */
    public function getCenterNameAttribute(){
        $center_id = $this->attributes['center_id'];

        $center = Code::category('pmn')->where('name',$center_id)->first();

        if($center){
            return $center->value;
        }

        return $center_id;
    }

}