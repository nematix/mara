<?php

use Atlantis\View\Portlet\Eloquent\Portlet;
use Atlantis\Context\Model\Context;


class MaraSeeder extends Seeder {

    public function run(){
        #i: Detail Seeding
        DB::table('details')->delete();
        Detail::create(array(
            'name'          => 'advance',
            'description'   => 'E-Pendahuluan',
            'config'        => '{"detail_max": 1,"detail_title": "Permohonan Pendahuluan Pinjaman Pelajaran","amount_total":2000}',
            'cards'         => '["info","application","course","guardian","guarantor", "final"]'
        ));


        #i: Portlet Seeding
        DB::table('portlets')->delete();
        Portlet::create(array('name'=>'user.tag','type'=>'model'));


        #i: Broadcast Seeding
        DB::table('conversations')->delete();
        DB::table('participants')->delete();
        DB::table('messages')->delete();

        $conversation = Conversation::create(array('subject' => 'Selamat Datang ke Sistem EPendahuluan'));
        Participant::create(array(
            'conversation_id'   => $conversation->id,
            'user_id'           => Sentry::findAllUsersWithAccess('superuser')[0]->id
        ));

        Message::create(array(
            'conversation_id'   => $conversation->id,
            'user_id'           => Sentry::findAllUsersWithAccess('superuser')[0]->id,
            'body'              => 'Skim Pendahuluan Pinjaman Pelajaran MARA kini dibuka secara online. Semua permohonan haruslah dibuat melalui laman portal ini.',
            'meta'              => json_encode(array('type' => 'broadcast'))
        ));


        #i: Contexts Seeding
        DB::table('contexts')->delete();
        $contexts = Config::get('mara::contexts');
        foreach($contexts as $context){
            Context::create($context);
        }

        #i: Budgets
        DB::table('advance_accounts')->delete();
        $accounts = Config::get('mara::accounts');
        foreach($accounts as $account){
            Modules\Advance\Account::create($account);
        }
    }

}