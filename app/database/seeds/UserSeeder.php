<?php


class UserSeeder extends Seeder {

    public function run(){
        #i: Group Seeding
        DB::table('groups')->delete();
        Sentry::getGroupProvider()->create(array(
            'name' => 'User',
            'permissions' => array(
                'user' => 1
            )
        ));

        $admin_group = Sentry::getGroupProvider()->create(array(
            'name' => 'Admin',
            'permissions' => array(
                'user' => 1,
                'admin' => 1
            )
        ));

        Sentry::getGroupProvider()->create(array(
            'name' => 'Staff',
            'permissions' => array(
                'user' => 1,
                'staff' => 1
            )
        ));

        Sentry::getGroupProvider()->create(array(
            'name' => 'Student',
            'permissions' => array(
                'user' => 1,
                'student' => 1
            )
        ));

        //
        // Create Superuser
        //
        DB::table('users')->delete();
        $admin_user = Sentry::getUserProvider()->create(array(
            'email' => 'admin@user.com',
            'password' => 'xs2admin',
            'first_name' => 'User',
            'last_name' => 'Admin',
            'activated' => 1,
            'permissions' => array(
                "superuser" => 1
            )
        ));

        #i: Staff Role
        DB::table('roles')->delete();
        $role_staff_admin = new Role();
        $role_staff_admin->name = Config::get('admin::user.role_staff_admin');
        $role_staff_admin->save();

        $role_staff_officer = new Role();
        $role_staff_officer->name = Config::get('admin::user.role_staff_officer');
        $role_staff_officer->save();

        $role_staff_manager = new Role();
        $role_staff_manager->name = Config::get('admin::user.role_staff_manager');
        $role_staff_manager->save();

        #i: Student Role
        $role_student = new Role();
        $role_student->name = Config::get('admin::user.role_student');
        $role_student->save();

        //
        // Superuser Group & Role
        //
        DB::table('users_groups')->delete();
        $admin_user->addGroup($admin_group);
        User::find($admin_user->id)->attachRole($role_staff_admin);

        //
        // Permissions
        //
        DB::table('permissions')->delete();
        $permissions = array(
            'advance' => 'Access EP application',
            'advance.new' => 'Create new EP application',
            'advance.edit' => 'Update EP application',
            'advance.manage' => 'Manage EP application',
            'advance.manage.staff' => 'Manage EP Staff',
            'advance.manage.budget' => 'Manage EP Budget',

            'advance.student.new' => 'New application',
            'advance.student.manage' => 'Manage application',
            'advance.student.edit' => 'Edit application',

            'advance.staff.browse' => 'Browse application',
            'advance.staff.edit' => 'Browse application',
            'advance.staff.statistic' => 'View application statistic',
            'advance.staff.manage' => 'Manage application',
            'advance.staff.manage.review' => 'Review application',
            'advance.staff.manage.approve' => 'Approve application',
            'advance.staff.manage.payment' => 'Payment for application',

            'detail.status.new.update' => 'Update status new (Baru)', //0
            'detail.status.eligible.update' => 'Update status eligible (Layak)', //1
            'detail.status.not_eligible.update' => 'Update status not eligible (Tidak Layak)', //2
            'detail.status.accept.update' => 'Update status accept (Terima)', //3
            'detail.status.support.update' => 'Update status support (Sokong)',
            'detail.status.not_support.update' => 'Update status not support (Tidak Sokong)',
            'detail.status.consideration.update' => 'Update status consideration (Dalam Pertimbangan)',
            'detail.status.pass.update' => 'Update status pass (Lulus)',
            'detail.status.not_pass.update' => 'Update status not pass (Tidak Lulus)',
            'detail.status.payment.update' => 'Update status pay (Bayar)',
            'detail.status.complete.update' => 'Update status complete (Lengkap)',

            'message.manage.list' => 'List a conversations',
            'message.thread' => 'Displaying conversation thread',
            'message.show' => 'Displaying message',
            'message.send' => 'Send message to user',
        );
        foreach( $permissions as $name => $display_name){
            $permission = new Permission;
            $permission->name = $name;
            $permission->display_name = $display_name;
            $permission->save();
        }

        //
        // Role - Permission Map
        //
        $role_map = array(
            $role_staff_admin->id => array(
                'advance.manage.staff',
                'advance.manage.budget'
            ),
            $role_staff_officer->id => array(
                'advance',
                'advance.staff.browse',
                'advance.staff.manage',
                'advance.staff.edit',
                'advance.staff.statistic',
                'advance.staff.manage.review',
                'detail.status.eligible.update',
                'detail.status.accept.update',

                'message.manage.list',
                'message.thread',
                'message.show',
                'message.send'
            ),
            $role_staff_manager->id => array(
                'advance',
                'advance.staff.browse',
                'advance.staff.manage',
                'advance.staff.edit',
                'advance.staff.statistic',
                'advance.staff.manage.review',
                'advance.staff.manage.approve',
                'advance.staff.manage.payment',
                'detail.status.eligible.update',
                'detail.status.not_eligible.update',
                'detail.status.accept.update',
                'detail.status.support.update',
                'detail.status.not_support.update',
                'detail.status.consideration.update',
                'detail.status.pass.update',
                'detail.status.not_pass.update',

                'message.manage.list',
                'message.thread',
                'message.show',
                'message.send'
            ),
            $role_student->id => array(
                'advance',
                'advance.student.new',
                'advance.student.edit',
                'advance.student.manage',

                'message.manage.list',
                'message.thread',
                'message.show',
            ),
        );

        DB::table('permission_role')->delete();
        foreach($role_map as $role_id => $permissions){
            $permission_array = array();

            foreach($permissions as $permission){
                $permission_id = Permission::where('name','=',$permission)->first()->id;
                $permission_array = array_merge($permission_array,array($permission_id));
            }

            $role = Role::find($role_id);
            $role->perms()->sync($permission_array);
        }
    }

}