<?php


class CodeSeeder extends Seeder {

    public function run(){
        DB::table('codes')->delete();

        Code::import(Config::get('code::state'));
        Code::import(Config::get('code::organisation_mara'));
        Code::import(Config::get('code::organisation_bank'));
        Code::import(Config::get('code::organisation_institution'));
        Code::import(Config::get('code::localization'));
        Code::import(Config::get('code::employment'));
        Code::import(Config::get('code::education'));
    }

}