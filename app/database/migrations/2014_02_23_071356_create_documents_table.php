<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('documents', function(Blueprint $table){
            $table->increments('id');
            $table->string('uuid',36);
            $table->string('detail_uuid',36);
            $table->string('image_file_name');
            $table->integer('image_file_size');
            $table->string('image_content_type');
            $table->dateTime('image_updated_at');
            $table->string('key');
            $table->integer('hit');
            $table->softDeletes();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('documents');
	}

}