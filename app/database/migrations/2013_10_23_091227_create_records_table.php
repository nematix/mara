<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecordsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('records', function(Blueprint $table)
		{
			$table->string('uuid',36);
            $table->integer('application_id');
            $table->integer('user_id');

            $table->string('institution_name');
            $table->string('institution_code');
            $table->string('institution_state');
            $table->string('application_location');
            $table->string('application_coursed');
            $table->date('application_date');
            $table->tinyInteger('application_existing');
            $table->string('course_level');
            $table->string('course_code');
            $table->date('course_start');
            $table->date('course_end');

            $table->integer('guardian_id')->nullable();
            $table->integer('guardian_salary_gross')->nullable();
            $table->integer('guardian_family_no')->nullable();
            $table->integer('guardian_employment')->nullable();

            $table->integer('guarantor_id')->nullable();
            $table->integer('guarantor_status')->nullable();
            $table->integer('guarantor_salary_gross')->nullable();

            $table->integer('amount_total');
            $table->string('payment_id');
            //$table->string('account_no');
            //$table->string('batch_no');
            //$table->string('cheque_no');
            //$table->string('voucher_no');
            //$table->string('approval_notice');
            //$table->string('amount_approved');
            //$table->string('amount_approved_text');
            $table->integer('status');
            $table->text('meta');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('records');
	}

}
