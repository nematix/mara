<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAdvanceAccountTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advance_accounts', function($table){
            $table->increments('id');
            $table->string('owner_id')->nullable();
            $table->string('account_category')->default('general');
            $table->string('account_description')->nullable();
            $table->string('account_number');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('advance_accounts');
	}

}
