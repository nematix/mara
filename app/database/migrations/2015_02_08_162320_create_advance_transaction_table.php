<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateAdvanceTransactionTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('advance_transactions', function($table)
		{
			$table->increments('id');
			$table->string('account_id');
            $table->string('transaction_type');
			$table->float('debit')->nullable();
            $table->string('debit_account_id')->nullable();
            $table->float('credit')->nullable();
            $table->string('credit_account_id')->nullable();
            $table->text('meta')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('advance_transactions');
	}

}
