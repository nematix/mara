<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeopleStudentsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('people_students', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('people_id');
            $table->string('account_bank_code');
            $table->string('account_bank_name');
            $table->string('account_bank_no');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('people_students');
	}

}
