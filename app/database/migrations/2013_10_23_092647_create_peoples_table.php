<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeoplesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('peoples', function(Blueprint $table)
		{
			$table->increments('id');
            $table->integer('user_id');
            $table->string('first_name');
            $table->string('last_name');
            $table->binary('object')->nullable();
            $table->string('idno_ic');
            $table->string('race');
            $table->string('gender');
            $table->date('birth_date');
            $table->string('birth_place');
            $table->string('address_street');
            $table->string('address_area');
            $table->string('address_district');
            $table->string('address_postcode');
            $table->string('address_city');
            $table->string('address_state');
            $table->string('contact_home');
            $table->string('contact_mobile');
            $table->text('meta')->nullable();
            $table->integer('data_id');
            $table->string('data_type');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('peoples');
	}

}
