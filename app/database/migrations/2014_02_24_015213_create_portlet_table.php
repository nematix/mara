<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortletTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('portlets', function(Blueprint $table){
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('title')->nullable();
            $table->string('type');
            $table->text('body')->nullable();
            $table->string('uri')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('portlets');
	}

}