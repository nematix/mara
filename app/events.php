<?php

Event::listen('user.registering', function($inputs,$realm){
    // Validating data before user register.
});


Event::listen('user.registered', function($user,$realm){
    #i: Capitalize $realm, 'staff' > 'Staff'
    $realm = studly_case($realm);

    $group_staff = \Config::get('admin::user.group_view.staff');
    $group_student = \Config::get('admin::user.group_view.student');

    if( $realm == $group_staff ){
        $data = Input::all();

        #i: Create user profile
        $profile = new People();
        $profile->idno_ic = $data['idno_ic'];
        $profile->save();

        #i: Create Staff data from Album
        $album = Album::find($data['salary_id']);
        $staff = new People\Staff();
        $staff['salary_id'] = $album->salary_id;
        $staff['salary_grade'] = $album->salary_grade;
        $staff['center_id'] = $album->center_id;
        $staff->save();

        #i: Attach Staff data to profile
        $staff->people()->save($profile);

        #i Associate profile to user
        $user = User::find($user->id);
        $user->profile()->save($profile);

    }elseif( $realm == $group_student ){
        #i: Create user profile
        $profile = new People();
        $profile->idno_ic = Input::old('idno_ic');
        $profile->save();

        #i: Create Student data
        $student = new People\Student();
        $student->save();

        #i: Attach Student data to profile
        $student->people()->save($profile);

        #i Associate profile to user
        $user = User::find($user->id);
        $user->profile()->save($profile);
    }

    #i: Add group to user
    $sentry_user = Sentry::findUserById($user->id);
    $group = Sentry::findGroupByName($realm);
    $sentry_user->addGroup($group);

    #i: Guess user role
    if( $realm == $group_staff) {
        /** @var $role string Get default role */
        $role = Role::where('name',Config::get('admin::user.role_staff_officer'))->first();

        /** @var $salary_id string Get the salary id*/
        $salary_grade = $user->profile->data->salary_grade;
        preg_match('/(\d+)\b/',$salary_grade,$grade_match);

        if( !empty($grade_match) ){
            $salary_grade = $grade_match[1];

            if( $salary_grade >= 44 ){
                $role = Role::where('name',Config::get('admin::user.role_staff_manager'))->first();
            }
        }
    };

    if( $realm == $group_student) $role = Role::where('name',Config::get('admin::user.role_student'))->first();

    #i: Add role to user
    if( $role ){
        $user->roles()->attach($role->id);
    }
});


Event::listen('auth.login.alternative', function($realm,$credential){
    if($realm == 'staff'){
        $login = $credential['email'];

        #i: Check for staff salary id
        $staff = People\Staff::where('salary_id',$login)->first();

        #i: Return user if found
        if( $staff ) return $staff->people->user;
    }
});