<div class="box">
    <div class="box-header">
        <div class="title">{{ trans('message::message.title.broadcast') . ' & ' . trans('message::message.title.message')}}</div>
    </div>
    <div class="box-content">
        @foreach( $broadcast->get()->take(5) as $conversation )
        <div class="box-section col-fixed">
            <div class="col-fixed-60 no-padding"><img src="{{ Gravatar::src($conversation->messages()->first()->user->email,60) }}"></div>
            <div class="content">
                <div class="title"><a href="{{ url('message/thread/'.$conversation->messages()->first()->id) }}">{{ $conversation->subject }}</a></div>
                <p>{{ $conversation->messages()->first()->body }}</p>
            </div>
            <div class="col-fixed-30 time">
                <span>{{ $conversation->updated_at->format('d') }}</span> {{ $conversation->updated_at->format('M') }}
            </div>
        </div>
        @endforeach

        @foreach( $conversations->get()->take(5) as $conversation )
            <div class="box-section col-fixed">
                <div class="col-fixed-60 no-padding"><img src="{{ Gravatar::src($conversation->messages()->first()->user->email,60) }}"></div>
                <div class="content">
                    <div class="title"><a href="{{ url('message/thread/'.$conversation->id) }}">{{ $conversation->subject }}</a></div>
                    <p>{{ $conversation->messages()->first()->body }}</p>
                </div>
                <div class="col-fixed-30 time">
                    <span>{{ $conversation->updated_at->format('d') }}</span> {{ $conversation->updated_at->format('M') }}
                </div>
            </div>
        @endforeach
    </div>
</div>