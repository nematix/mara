@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Dasar Privasi</h3>
            <hr>

            <p>Halaman ini menerangkan dasar privasi yang merangkumi penggunaan dan perlindungan maklumat yang dikemukakan oleh pengunjung.</p>

            <h5>Maklumat yang Dikumpul</h5>
            <p>Tiada maklumat peribadi akan dikumpul semasa anda melayari Portal ini kecuali maklumat yang dikemukakan oleh anda melalui e-mel.</p>

            <h5>Pautan kepada Laman Web yang Lain</h5>
            <p>Portal ini mempunyai pautan ke laman web agensi Kerajaan dan agensi bukan Kerajaan. Dasar privasi ini hanya terpakai untuk Portal ini sahaja. Perlu diingatkan bahawa laman web yang terdapat dalam pautan Portal ini mungkin mempunyai dasar privasi yang berbeza dan pengunjung dinasihatkan supaya meneliti dan memahami dasar privasi bagi setiap laman web yang dilayari.</p>

            <h5>Pindaan Dasar</h5>
            <p>Sekiranya dasar privasi ini dipinda, pindaan akan dikemas kini di halaman ini. Dengan sering melayari halaman ini, anda akan dikemas kini dengan maklumat yang dikumpul, cara ia digunakan dan dalam keadaan tertentu, bagaimana maklumat dikongsi bersama pihak yang lain.</p>
        </div>
    </div>
@stop