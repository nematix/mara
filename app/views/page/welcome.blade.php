@section('page')
    <div class="row">
        <div class="col-md-8">
            <h3>Selamat Datang</h3><hr>
            <blockquote>
                Skim Pendahuluan Pinjaman Pelajaran MARA kini dibuka secara online. Semua permohonan haruslah dibuat melalui laman portal ini.
                Untuk keterangan lanjut, sila hubungi Pejabat MARA Negeri yang berdekatan.
                Sekian, terima kasih.
            </blockquote>
        </div>
        <div class="col-md-4">
            <div class="well relative">
                <p>
                Pertama kali memohon? Klik pada butang di bawah untuk daftar sebagai pengguna.
                </p>
                <a class="btn btn-success" href="{{ url('student/verify') }}">Pendaftaran Pengguna</a>
            </div>
            <div class="well relative">
                <p>
                    Melengkapkan permohonan anda / ingin melihat status permohonan? Klik pada butang di bawah.
                </p>
                <a class="btn btn-primary" href="{{ url('user/login/student') }}">Daftar Masuk</a>
            </div>
        </div>
    </div>
@stop