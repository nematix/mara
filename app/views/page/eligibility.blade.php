@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Syarat dan kelayakan untuk mendapatkan Pendahuluan adalah:</h3><hr>
            <ul class="list-group">
                <li class="list-group-item"><strong>Pendapatan kasar bulanan ibubapa / penjaga</strong> pelajar hendaklah <strong>tidak melebihi RM4,000.00 sebulan</strong>. Walau bagaimanapun bagi ibubapa / penjaga pelajar yang berpendapatan melebihi RM4,000.00 sebulan, pertimbangan boleh diberi tertakluk kepada bilangan tanggungan mengikut garis panduan <strong>Jadual Pemarkahan Kelayakan Pendapatan bagi Permohonan Pendahuluan Pinjaman Pelajaran MARA.</strong> </li>
                <li class="list-group-item"><strong>Warganegara Malaysia</strong> dan bertaraf <strong>Bumiputera.</strong> </li>
                <li class="list-group-item">Berumur <strong>tidak melebihi 35 tahun</strong> bagi pengajian peringkat <strong>Diploma</strong> dan <strong>40 tahun</strong> bagi peringkat <strong>Ijazah Pertama.</strong></li>
                <li class="list-group-item">Telah ditawarkan mengikut pengajian secara sepenuh masa</li>
                <li class="list-group-item">Pelajar yang terdiri daripada mereka yang <strong>telah bekerja atau sedang mengikuti pengajian tidak layak dipertimbangkan.</strong></li>
                <li class="list-group-item">Pelajar yang ditawarkan ke <strong>pengajian IPTA</strong> adalah <strong>tidak layak memohon</strong> kecuali mereka yang ditawarkan pengajian di peringkat Program Persediaan/Pra-U di bawah Program Pelajar Cemerlang Lepasan SPM.</li>
                <li class="list-group-item">Pendahuluan ini tidak dikenakan sebarang faedah dan <strong>perlu dibayar balik sepenuhnya.</strong></li>
            </ul>
        </div>
    </div>
@stop