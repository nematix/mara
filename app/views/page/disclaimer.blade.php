@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Penafian</h3><hr>
            <p>
                MARA tidak akan bertanggungjawab terhadap sebarang kerosakkan atau kehilangan yang
                dialami disebabkan oleh penggunaan maklumat dalam laman web ini. Semua maklumat yang dipaparkan adalah benar dan
                betul pada masa tarikh dan masa diterbitkan dan tertakluk kepada pindaan dari masa ke semasa.
            </p>
        </div>
    </div>
@stop