@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Panduan Permohonan</h3><hr>

            <p>Membantu pelajar-pelajar Bumiputera dari keluarga berpendapatan sederhana rendah untuk membiayai perbelanjaan persediaan ke IPTT bagi mengatasi kesulitan kewangan semasa melapor diri ke Institusi Pendidikan MARA (IPMA)/ Institusi Pengajian Tinggi Swasta (IPTS) berdaftar dengan MARA.</p>
            <ul class="list-group">
                <li class="list-group-item">Jumlah pendahuluan adalah sebanyak <strong>RM2,000</strong> bagi seorang pelajar. Tertakluk kepada peruntukan dan syarat-syarat yang ditetapkan dari semasa ke semasa.</li>
                <li class="list-group-item">Pembayaran Pendahuluan ini diberi selewat-lewatnya 2 minggu selepas borang perjanjian yang lengkap dihantar.</li>
            </ul>

            <div class="panel">
                <!-- Step -->
                <div class="panel-section col-fixed">
                    <div class="col-fixed-30"><i class="fa fa-envelope fa-2x avatar circle red"></i></div>
                    <div class="content">
                        <div class="title"><a href="#">Keperluan Asas</a></div>
                        <p>
                            Pelajar hendaklah mempunyai akaun e-mail sebelum mengisi borang permohonan secara online. Sila pastikan semua maklumat yang dimasukkan adalah lengkap untuk diproses. Sila rujuk pada "Syarat Kelayakan" untuk keterangan lanjut.
                        </p>
                    </div>
                    <div class="col-fixed-30 time">
                        <span>1</span> Step
                    </div>
                </div>

                <!-- Step -->
                <div class="panel-section col-fixed">
                    <div class="col-fixed-30"><i class="fa fa-file fa-2x avatar circle orange"></i></div>
                    <div class="content">
                        <div class="title"><a href="#">Dokumen</a></div>

                            <p>Sila hantarkan secara pos atau terus ke Pejabat MARA Negeri (PMN) anda dokumen-dokumen sokongan berikut:</p>
                            <ul class="list-group">
                                <li class="list-group-item">Salinan <strong>kad pengenalan</strong> pelajar dan ibubapa.</li>
                                <li class="list-group-item">Salinan <strong>sijil kelahiran</strong> pelajar dan ibubapa.</li>
                                <li class="list-group-item">Salinan <strong>penyata gaji atau salinan borang BE</strong> terkini pelajar dan ibubapa / penjaga yang bekerja.</li>
                                <li class="list-group-item">Salinan <strong>borang BE</strong> bagi ibubapa / penjaga yang bekerja sendiri. Sekiranya ibubapa / penjaga bekerja sendiri dengan pendapatan kasar tidak dikenakan cukai, salinan <strong>surat pengesahan pendapatan</strong> dari Ketua Kampung, Ketua Mukim, Penghulu dan Imam Kariah yang dilantik oleh pihak berkuasa negeri yang mempunyai cop jawatan rasmi, Wakil Rakyat atau Jaksa Pendamai perlu dikemukakan.</li>
                                <li class="list-group-item">Salinan <strong>kad pengenalan</strong> dan salinan <strong>penyata gaji</strong> atau surat pengesahan pendapatan penjamin dari Ketua Kampung, Ketua Mukim, Penghulu dan Imam Kariah yang dilantik oleh pihak berkuasa negeri yang mempunyai cop jawatan rasmi, Wakil Rakyat atau Jaksa Pendamai bagi yang bekerja sendiri.</li>
                                <li class="list-group-item">Surat <strong>tawaran asal</strong> dan salinan <strong>kemasukan ke IPTT.</strong></li>
                            </ul>
                            <p>Semua dokumen-dokumen di atas hendaklah disahkan oleh samada oleh Ketua Kampung, Penghulu, Penggawa, Pegawai Daerah, Wakil Rakyat, Guru Besar, Pegawai Kerajaan / Badan Berkanun atau Pegawai MARA dari kumpulan Pengurusan dan Profesional, Pegawai Latihan Negeri (STO), Pegawai MARA Daerah (PMD) atau Penolong Pegawai MARA Daerah (PPMD).</p>
                    </div>
                    <div class="col-fixed-30 time">
                        <span>2</span> Step
                    </div>
                </div>

                <!-- Step -->
                <div class="panel-section col-fixed">
                    <div class="col-fixed-20"><i class="fa fa-warning fa-2x avatar circle yellow"></i></div>
                    <div class="content">
                        <div class="title"><a href="#">Status Permohonan</a></div>
                        Pihak MARA akan memaklumkan status permohonan melalui emel atau anda boleh menyemak sendiri di laman web ini.
                    </div>
                    <div class="col-fixed-20 time">
                        <span>3</span> Step
                    </div>
                </div>

                <!-- Step -->
                <div class="panel-section col-fixed">
                    <div class="col-fixed-20"><i class="fa fa-check fa-2x avatar circle green"></i></div>
                    <div class="content">
                        <div class="title"><a href="#">Kelulusan</a></div>
                        <p>
                            Sekiranya permohonan telah diluluskan, sila cetak Surat Tawaran dan dokumen Perjanjian. Pelajar dikehendaki menyerahkan lima (5) salinan dokumen Perjanjian. Dua (2) salinan dokumen Perjanjian hendaklah dimatikan setem hasil RM10.00 setiap salinan. Manakala tiga (3) salinan adalah difotokopi dari salah satu dokumen Perjanjian yang telah dimatikan setem tersebut. Sila serahkan semua dokumen termasuk semua dokumen sokongan di atas dalam tempoh dua (2) minggu dari tarikh Surat Tawaran dan memasukkan semua dokumen tersebut ke dalam fail yang boleh didapati di Pejabat MARA Negeri.
                        </p>
                    </div>
                    <div class="col-fixed-20 time">
                        <span>4</span> Step
                    </div>
                </div>

            </div>
        </div>
    </div>
@stop