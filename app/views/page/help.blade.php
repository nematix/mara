@section('page')
<div class="row">
    <div class="col-md-8">
        <h4><i class="fa fa-umbrella"></i> {{ trans('advance::advance.title.page_help') }}</h4>
        <p></p>
        <ul class="list-group">
            <li class="list-group-item"><i class="fa fa-link"></i> <a href="{{ URL::to('user/recovery') }}">{{ trans('advance::advance.title.user_password_forget') }}</a></li>
            <li class="list-group-item"><i class="fa fa-link"></i> <a href="{{ URL::to('user/activation') }}">{{ trans('advance::advance.title.user_resend_activation') }}</a></li>
            <li class="list-group-item hidden"><i class="fa fa-link"></i> <a href="{{ URL::to('user/activate') }}">{{ trans('advance::advance.title.user_manual_activation') }}</a></li>
            <li class="list-group-item "><i class="fa fa-link"></i> <a href="{{ URL::to('student/verify') }}">{{ trans('advance::advance.title.user_new_registration') }}</a></li>
        </ul>
    </div>
    <div class="col-md-4">
        <div class="box">
            <div class="box-header"><span class="title">{{ trans('advance::advance.title.page_helpdesk') }}</span></div>
            <div class="box-content padded">
                <ul class="list-unstyled">
                    <li><i class="fa fa-headphones"></i> 03-26132332</li>
                    <li><i class="fa fa-envelope"></i> webmaster@mara.gov.my</li>
                    <li><i class="fa fa-laptop"></i> http://aduan.mara.gov.my/</li>
                </ul>
            </div>
        </div>
    </div>
</div>
@stop