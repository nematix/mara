@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Terma - Terma</h3><hr>
            <p>Di bawah adalah syarat-syarat penggunaan laman portal serta hak dan kewajipan anda semasa mengakses dan/atau menggunakan perkhidmatan di laman web ini.<br> Sekiranya anda mengakses laman portal ini, ia merupakan pengakuan dan persetujuan bahawa anda terikat kepada syarat-syarat ini dan dengan ini, terbentuklah satu persetujuan di antara anda sebagai pelanggan, dan kami, bagi akses dan/atau penggunaan laman portal ini.</p>
            <p>Syarat-syarat ini akan menggantikan syarat-syarat terdahulu yang anda terima atau akses menerusi laman portal ini. Penggunaan dan/atau akses anda kepada perkhidmatan ini seterusnya akan dianggap sebagai penerimaan syarat-syarat ini.</p>

            <h5>Had Tanggungjawab</h5>
            <p>Anda dengan jelas memahami dan bersetuju bahawa kami tidak bertanggungjawab ke atas sebarang bentuk kerugian, secara langsung atau tidak langsung, yang berkaitan atau khusus, turutan atau exemplary, termasuk tetapi tidak terhad kepada kerugian berbentuk kehilangan keuntungan, kepercayaan atau kerugian tak ketara yang lain akibat daripada:</p>

            <ul class="list-group">
                <li class="list-group-item">penggunaan atau ketidakupayaan untuk menggunakan perkhidmatan ini; </li>
                <li class="list-group-item">kos perolehan barangan dan perkhidmatan gantian berikutan pembelian sebarang barangan, data, maklumat atau perkhidmatan atau mesej yang diterima atau transaksi yang dibuat menerusi atau daripada portal ini; </li>
                <li class="list-group-item">akses tanpa kebenaran atau perubahan dalam penghantaran atau data anda; </li>
                <li class="list-group-item">kenyataan atau tindakan pihak ketiga di laman portal ini; atau (5) perkara-perkara lain yang berkaitan dengan laman portal ini. </li>
            </ul>

            <h5>Pautan</h5>
            <p>Kami boleh menyediakan pautan kepada laman portal yang lain. Laman-laman web tersebut dimiliki dan dikendalikan oleh pihak ketiga dan oleh itu, kami tidak mempunyai kawalan ke atas laman dan sumber tersebut. Anda mengakui dan bersetuju bahawa kami tidak bertanggungjawab terhadap laman dan sumber luar tersebut dan tidak menyokong serta tidak bertanggungjawab ke atas sebarang kandungan, iklan, produk atau bahan-bahan lain yang disediakan di laman dan sumber tersebut. Anda seterusnya mengakui dan bersetuju bahawa kami tidak bertanggungjawab, secara langsung atau tidak langsung, terhadap sebarang kerosakan atau kerugian yang disebabkan oleh atau dipercayai sebagai sebabnya atau dikaitkan dengan penggunaan atau pergantungan ke atas kandungan, barangan atau perkhidmatan yang disediakan di atau menerusi laman atau sumber tersebut.</p>

            <h5>Penamatan</h5>
            <p>Kami boleh menamatkan akses anda ke mana-mana bahagian perkhidmatan atau seluruh perkhidmatan dan mana-mana perkhidmatan yang berkaitan pada bila-bila masa, dengan sebab atau tanpa sebab, dengan notis atau tanpa notis, dan berkuatkuasa serta-merta. Kami boleh juga menamatkan atau menggantung akaun anda yang tidak aktif, iaitu, didefinisikan sebagai kegagalan untuk menggunakan perkhidmatan laman web menerusi akaun tersebut bagi suatu tempoh tertentu. Anda bersetuju bahawa kami tidak bertanggungjawab kepada anda atau mana-mana pihak ketiga di atas penamatan akses kepada perkhidmatan tersebut.</p>

            <h5>Perubahan kepada Syarat-syarat Perkhidmatan</h5>
            <p>Kami mempunyai hak untuk membuat perubahan, pengubahsuaian, pembatalan atau penambahan kepada syarat-syarat ini pada bila-bila masa dengan memberi notis terlebih dahulu. Walaubagaimanapun, dalam keadaan kecemasan atau demi menjaga keselamatan laman portal atau dalam keadaan di luar kawalan, di mana kami mendapati perlunya untuk mengubah, mengubahsuai, membatal atau menambah syarat-syarat ini, ia akan berbuat demikian tanpa notis terdahulu kepada anda. Adalah dipersetujui bahawa anda akan mengakses dan meneliti syarat-syarat ini dari semasa ke semasa untuk mengetahui sebarang perubahan, pengubahsuaian, pembatalan atau penambahan yang terkini. Anda selanjutnya bersetuju dan menerima bahawa akses secara berterusan dan penggunaan syarat-syarat (yang diubah atau diubahsuai dari semasa ke semasa) akan dianggap sebagai penerimaan anda terhadap sebarang perubahan, pengubahsuaian, pembatalan atau penambahan kepada syarat-syarat ini.</p>

            <h5>Pengubahsuaian kepada Perkhidmatan</h5>
            <p>Kami mempunyai hak untuk mengubahsuai atau menamatkan perkhidmatan (atau sebahagian daripada perkhidmatan) sama ada secara sementara atau tetap pada bila-bila masa dengan notis atau tanpa notis. Anda bersetuju bahawa kami tidak bertanggungjawab kepada anda atau mana-mana pihak ketiga di atas sebarang pengubahsuaian, penggantungan atau penamatan perkhidmatannya.</p>

            <h5>Umum</h5>
            <p>Tajuk-tajuk yang digunakan di bawah syarat-syarat ini hanya untuk rujukan dan tidak boleh diambilkira dalam tafsiran syarat-syarat ini.</p>
            <p>Sekiranya mana-mana peruntukan syarat-syarat ini menyalahi undang-undang atau tidak sah di sisi undang-undang atau peraturan-peraturan sekarang atau yang akan datang, peruntukan tersebut akan diasingkan dan syarat-syarat ini akan ditafsirkan seolah-olah peruntukan yang tidak sah itu tidak wujud dan syarat-syarat ini bersama-sama peruntukan lain akan berkuatkuasa sepenuhnya tanpa dijejaskan oleh peruntukan yang menyalahi undang-undang atau yang tidak sah.</p>
        </div>
    </div>
@stop