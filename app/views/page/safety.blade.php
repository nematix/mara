@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Dasar Keselamatan</h3>

            <ul class="list-group">
                <li class="list-group-item">1. Sumbangan dan penyebaran maklumat yang dilarang dan bahan-bahan negatif yang bertentangan dengan dasar-dasar negara adalah dilarang.</li>
                <li class="list-group-item">2. Menyebar maklumat berbau politik, hasutan atau perkauman atau apa-apa yang menjejaskan reputasi jabatan adalah dilarang.</li>
                <li class="list-group-item">3. Pengunjung hendaklah memastikan fail yang dihantar melalui kepilan (attachment) bebas dari virus.</li>
                <li class="list-group-item">4. Pengunjung adalah bertanggungjawab  sepenuhnya ke atas maklumat yang dikunci masuk.</li>
                <li class="list-group-item">5. Semua maklumat yang hendak dimuatkan ke dalam laman portal MARA mestilah mendapat kelulusan Pengarah Bahagian Ehwal Korporat / Pengarah Bahagian Pengurusan Maklumat.</li>
                <li class="list-group-item">6. Laman web agensi atau syarikat yang memerlukan pautan ke laman portal MARA atau sebaliknya mestilah mendapat kebenaran Pengarah Bahagian Ehwal Korporat / Pengarah Bahagian Pengurusan Maklumat.</li>
                <li class="list-group-item">7. Pencerobohan atau percubaan untuk menggodam laman portal MARA adalah dilarang.</li>
                <li class="list-group-item">8. Semua storan elektronik dan penghantaran data peribadi akan dilindungi dan disimpan dengan menggunakan teknologi keselamatan yang sesuai.</li>
            </ul>
        </div>
    </div>
@stop