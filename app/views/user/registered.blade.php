@extends('admin::user.registered')

@section('stylesheet')
    @parent
    @include('partials.public-style')
@stop

@section('box-before')
    @include('partials.header-logo')
@show

@section('box-content')
    <div class="alert alert-success">
        {{ trans('mara::user.registered_text_success', array('email'=>$email)) }}
    </div>
    <a href="{{ url('user/login/staff') }}" class="btn btn-blue btn-block">{{ trans('admin::user.login_btn_login') }} <i class="fa fa-signin"></i></a>
@show