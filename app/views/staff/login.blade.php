@extends('admin::user.login')

@section('stylesheet')
    @parent
    @include('partials.public-style')
@stop

@section('box-before')
    @include('partials.header-logo')
@show

@section('box-header')
    <div class="title">{{ trans('advance::realm.staff.title.login') }}</div>
@show

@section('box-content')
    {{ Former::open()->class('separate-sections') }}
        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-user"></i></span>
            {{ Former::text('email')->placeholder(trans('advance::realm.staff.title.username_alt')) }}
        </div>

        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-key"></i></span>
            {{ Former::password('password')->placeholder(trans('admin::user.login_label_password')) }}
        </div>

        <div>
            <btn id="submit" class="btn btn-blue btn-block">{{ trans('admin::user.login_btn_login') }} <i class="fa fa-signin"></i></btn>
        </div>
    {{ Former::close() }}

    <div>
        <div class="pull-left">
            <i class="fa fa-umbrella"></i> {{ trans('admin::user.login_text_help', array('link'=>url('public/page/help'))) }}
        </div>
        <div class="pull-right">
            <a href="{{ url('staff/verify') }}"><i class="fa fa-certificate"></i> {{ trans('advance::realm.staff.title.header') }}</a>
        </div>
    </div>

    <div class="clearfix"></div>
@show