@extends('admin::user.register')

@section('stylesheet')
    @parent
    @include('partials.public-style')
@stop

@section('box-before')
    @include('partials.header-logo')
@show

@section('box-header')
    <div class="title">{{ trans('advance::realm.staff.title.header') }}</div>
@show

@section('box-content')
    {{ Former::open('staff/verify')->method('POST')->class('separate-sections') }}
    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-certificate"></i></span>
        {{ Former::text('salary_id')->class('validate[required]')->placeholder(trans('advance::realm.staff.title.salary_id')) }}
    </div>

    <div>
        <btn id="submit" class="btn btn-blue btn-block">{{ trans('general.title.verify') }} <i class="fa fa-signin"></i></btn>
    </div>
    {{ Former::close() }}
@show