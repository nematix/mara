@extends('themes/default::layouts.fluid')

@section('container')
    <div class="container" style="padding-top: 15px;">
        @include('core::partials.error')

        <div class="row">
            <div class="col-md-8">
                @include('partials.conversations')
            </div>

            <div class="col-md-4">
                <div class="panel panel-profile">
                    <div class="panel-heading bg-primary clearfix">
                        <a class="pull-right profile" href="">
                            <img class="img-circle img80_80" src="{{ Gravatar::src($user->email,80) }}" alt="">
                        </a>
                        <h3 class="ng-binding">{{ User::find($user->id)->first_name }}</h3>
                        <p>{{ User::find($user->id)->roles[0]->name }}</p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <span class="badge">{{ $conversations->count() }}</span>
                            <i class="fa fa-envelope-o"></i>
                            {{ trans('message::message.title.notification') }}
                        </li>
                    </ul>
                </div>

                <div class="panel">
                    <div class="panel-heading">
                        {{ $module['title'] }}
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item list-group-item-info">
                            <i class="fa fa-building-o "></i>
                            {{ trans('mara::mara.title.state_center') }} : {{ User::find($user->id)->profile->data->center_name }}
                        </li>
                        @foreach( $status_role as $status )
                        <li class="list-group-item">
                            <span class="badge badge-{{ $status['label'] }}">{{ $status['total'] }}</span>
                            <i class="fa fa-rss-square"></i>
                            {{ $status['title'] }}
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>

        </div>
    </div>
@stop

@section('javascript')
    @parent
    <script>
        $(document).ready(function(){
            $('#btn-application-advance').on('click',function(e){
                window.location.href = "{{ url('application/advance/new') }}";
            });
        });
    </script>
@stop