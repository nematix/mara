@extends('admin::user.register')

@section('stylesheet')
    @parent
    @include('partials.public-style')
@stop

@section('box-before')
    @include('partials.header-logo')
@show

@section('box-header')
    <div class="title">{{ trans('advance::realm.staff.title.header') }}</div>
@show

@section('box-content')
    {{ Former::open('user/register/staff')->class('separate-sections') }}
        @if( isset($staff) )
        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-envelope"></i></span>
            {{ Former::text('first_name')
                ->disabled()
                ->value( Str::title(trim($staff['name'])) ) }}
        </div>
        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-book"></i></span>
            {{ Former::text('idno_ic')
            ->disabled()
            ->class('validate[required,custom[idICNo]]')
            ->value( isset($staff) ? $staff['idno_ic'] : '' )
            ->placeholder(trans('user.register_staff_label_idnoic')) }}
        </div>
        {{ Former::hidden('first_name')->value($staff['name']) }}
        {{ Former::hidden('idno_ic')->value($staff['idno_ic']) }}
        {{ Former::hidden('salary_id')->value($staff['salary_id']) }}
        @endif

        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-envelope"></i></span>
            {{ Former::text('email')
                ->class('validate[required,custom[email]]')
                ->placeholder(trans('admin::user.label_email')) }}
        </div>

        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-key"></i></span>
            {{ Former::password('password')
                ->class('validate[required]')
                ->placeholder(trans('admin::user.label_password')) }}
        </div>

        <div class="input-group addon-left">
            <span class="input-group-addon" href="#"><i class="fa fa-key"></i></span>
            {{ Former::password('password_confirm')
                ->class('validate[required,equals[password]]')
                ->placeholder(trans('admin::user.label_password_confirm')) }}
        </div>

        <div>
            <btn id="submit" class="btn btn-blue btn-block">{{ trans('admin::user.btn_register') }} <i class="fa fa-signin"></i></btn>
            <a href="{{ url('user/login/staff') }}" class="btn btn-blue btn-block">{{ trans('general.title.back') }} <i class="fa fa-circle-arrow-left"></i></a>
        </div>
    {{ Former::close() }}
@show