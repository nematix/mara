@extends('admin::user.login')

@section('stylesheet')
    @parent
    @include('partials.public-style')
@stop

@section('box-before')
    @include('partials.header-logo')
@show

@section('box-header')
    <div class="title">{{ trans('advance::realm.student.title.login') }}</div>
@show