@extends('themes/default::layouts.fluid')

@section('container')
    <div class="container" style="padding-top: 15px;">
        @include('core::partials.error')

        <div class="row">
            <div class="col-md-8">
                @include('advance::partials.records')
                @include('partials.conversations')
            </div>

            <div class="col-md-4">
                <div class="panel panel-profile">
                    <div class="panel-heading bg-primary clearfix">
                        <a class="pull-right profile" href="">
                            <img class="img-circle img80_80" src="{{ Gravatar::src($user->email,80) }}" alt="">
                        </a>
                        <h3 class="ng-binding">{{ User::find($user->id)->full_name }}</h3>
                        <p>{{ User::find($user->id)->roles[0]->name }}</p>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            @if( $conversations->count() > 0 )
                            <span class="badge badge-success">
                                <a href="{{ url('message') }}" style="color:#fff;">{{ $conversations->count() }}</a>
                            </span>
                            @else
                                <span class="badge">{{ $conversations->count() }}</span>
                            @endif
                            <i class="fa fa-envelope-o"></i>
                            {{ trans('advance::advance.title.message_received') }}
                        </li>
                        <li class="list-group-item">
                            <span class="badge badge-warning">{{ $records->count() > 0 ? $records->first()->documents()->count() : 0 }}</span>
                            <i class="fa fa-comments-o"></i>
                            {{ trans('advance::advance.title.document_uploaded') }}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@stop