@extends('admin::user.register')

@section('stylesheet')
    @parent
    @include('partials.public-style')
@stop

@section('box-before')
    @include('partials.header-logo')
@show

@section('box-header')
    <div class="title">{{ trans('advance::realm.student.title.header') }}</div>
@show

@section('box-content')
    {{ Form::open(array('url'=>'user/register/student', 'class'=>'separate-sections')) }}

    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-envelope"></i></span>
        {{ Form::text('email',Input::old('email'),array('class'=>'validate[required,custom[email]]','placeholder'=>trans('advance::validation.attributes.email'))) }}
    </div>

    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-user"></i></span>
        {{ Form::text('first_name',Input::old('first_name'),array('placeholder'=>trans('advance::validation.attributes.first_name'))) }}
    </div>

    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-user"></i></span>
        {{ Form::text('last_name',Input::old('last_name'),array('placeholder'=>trans('advance::validation.attributes.last_name'))) }}
    </div>

    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-user"></i></span>
        {{ Form::hidden('idno_ic',Input::old('idno_ic')) }}
        {{ Form::text('idno_ic',Input::old('idno_ic'),array('placeholder'=>trans('advance::validation.attributes.idno_ic'),'disabled'=>'true')) }}
    </div>

    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-key"></i></span>
        {{ Form::password('password',array('class'=>'validate[required]', 'id'=>'password', 'placeholder'=>trans('advance::validation.attributes.password'))) }}
    </div>

    <div class="input-group addon-left">
        <span class="input-group-addon" href="#"><i class="fa fa-key"></i></span>
        <input id="password_confirm" placeholder="Pengesahan Katalaluan" type="password" class="validate[equals[password]]">
    </div>

    <div>
        <btn id="submit" class="btn btn-blue btn-block">{{ trans('advance::realm.student.title.register') }} <i class="fa fa-signin"></i></btn>
        <a href="{{ url('user/login/student') }}" class="btn btn-blue btn-block">{{ trans('general.title.back') }} <i class="fa fa-circle-arrow-left"></i></a>
    </div>

    {{ Form::close() }}
@show