<?php

#i: Default Route
Route::any('/', 'PublicController@showPage');
Route::get('public/page/{page?}', 'PublicController@showPage');

#i: Staff Public Controller
Route::group(['prefix'=>'staff'], function(){
    Route::get('verify','StaffController@getVerify');
    Route::post('verify','StaffController@postVerify');
    Route::get('register','StaffController@getRegister');
    Route::post('register','StaffController@postRegister');
});

#i: Student Public Controller
Route::group(['prefix'=>'student'], function(){
    Route::get('verify','StudentController@getVerify');
    Route::post('verify','StudentController@postVerify');
});

Route::group(['before'=>'auth.sentry'], function(){
    Route::controller('student','StudentController',[
        'getIndex'  => 'student',
        'getHome'   => 'student.home'
    ]);
    Route::controller('staff','StaffController',[
        'getIndex'  => 'staff',
        'getHome'   => 'staff.home'
    ]);
});