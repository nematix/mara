<?php


class PublicController extends Atlantis\Core\Controller\BaseController {

	public function showPage($page = 'welcome')
	{
        $this->layout->content = View::make('themes/default::layouts.page');
        $this->layout->content->page = View::make('page.' . $page);
	}

}