<?php

use Illuminate\Support\Facades\Validator;


class StudentController extends Atlantis\Core\Controller\BaseController {

    public function getIndex(){
        if( Sentry::check() ) {
            return Redirect::to('student/home');
        }else{
            return Redirect::to('student/login');
        }
    }


    public function getHome(){
        #i: Get profile(people) id
        $user = \User::find($this->user->id);

        #i: Assigning var to data
        $data['detail'] = Detail::find('advance');
        $data['records'] = Record::where('user_id','=',$user->id)->get();
        $data['broadcast'] = Conversation::forUser($this->superuser->id)->broadcast();
        $data['conversations'] = Conversation::forUser($this->user->id)->withMessageMeta('{"permission":{"reply":"staff"}}');

        $this->layout->content = View::make('student.home',$data);
    }


    public function getVerify(){
        #i: Loading view
        $this->layout->content = View::make('student.verify');
    }


    public function postVerify(){
        $post = Input::all();

        try{
            /** Input validation */
            $validator = Validator::make($post,array('idno_ic'=>'required|idno_ic'));
            if($validator->fails()) throw new Exception($validator->messages()->first());

            /** Verified blacklist */
            $blacklist = Blacklist::find($post['idno_ic']);

            /** If blacklist warning*/
            if($blacklist) {
                throw new Exception(trans('advance::advance.blacklist.modal-body',array('idno_ic'=>$post['idno_ic'])));
            }
            
            /** Check age base on IC */
            preg_match('/(\d{2})(\d{2})(\d{2})/', $post['idno_ic'], $matches);
            $age = Carbon\Carbon::createFromDate($matches[1]+1900, $matches[2], $matches[3])->age;

            if( $age > Config::get('advance::advance.application.maximum_age') ){
                throw new Exception(trans('advance::advance.text.registration_failed_age'));
            }

            /** Verified IC */
            $student = People::where('idno_ic',$post['idno_ic'])->get()->first();

            /** If found */
            if( $student ){
                /** Login help link */
                $href = URL::to('public/page/help');

                throw new Exception(trans('advance::realm.student.text.registered',array('href'=>$href)));

            }else{
                /** Queue cookie for next request (need second consideration) */
                Input::flash();

                /** Redirect to register */
                return Redirect::action('Atlantis\Admin\AuthController@getRegister',array('role'=>'student'));
            }

        }catch (\Exception $e){
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        /** Loading view */
        $this->layout->content = View::make('student.verify',$post);
    }
}
