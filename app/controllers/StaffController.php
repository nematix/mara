<?php

use Atlantis\Core\Controller\BaseController;
use Atlantis\Core\Module\Facades\Module;


class StaffController extends BaseController {

    /**
     * Index
     *
     */
    public function getIndex(){
        Redirect::route('staff.home',Input::all());
    }


    /**
     * Home
     *
     */
    public function getHome(){
        /** Get broadcast message */
        $data['broadcast'] = Conversation::forUser($this->superuser->id)->broadcast();
        $data['conversations'] = Conversation::forUser($this->user->id);

        /** Get modules object */
        $data['module'] = Module::get('advance');

        /** Get details object */
        $data['detail'] = Detail::find('advance');

        /** Get status capable of role */
        $statuses = $data['detail']->status['all'];
        $data['status_role'] = [];

        foreach($statuses as $status){
            if( User::find($this->user->id)->can('detail.status.'.$status['name'].'.update') ){
                $data['status_role'][$status['id']] = $status;
            }
        }

        /** Response */
        $this->layout->content = View::make('staff.home',$data);
    }


    /**
     * GET : Verify
     *
     */
    public function getVerify(){
        /** Loading view */
        $this->layout->content = View::make('staff.verify');
    }


    /**
     * POST : Verify
     * @return mixed
     */
    public function postVerify(){
        $post = Input::all();

        #i: Check for salary ID
        $salary_id = ( isset($post['salary_id']) ? $post['salary_id'] : '' );

        try{
            #i: Verified salary id
            $staff = Album::find($salary_id);

            #i: If found
            if( $staff ){
                #i: Check for registered staff
                $staff_registered = \People\Staff::where('salary_id','=',$staff->salary_id)->first();

                #i: Throw error if staff already registered
                if($staff_registered) throw new Exception(trans('advance::realm.staff.text.registered'));

                $pmn_eligible = Config::get('mara::mara.pmn_eligible');
                if( !in_array($staff->center_id,$pmn_eligible) ) throw new Exception('Pegawai dari PMN tersebut tidak dibenarkan!');

                #i: Add staff data
                $post['staff'] = $staff->toArray();

                #i: Queue cookie for next request (need second consideration)
                Cookie::queue('verify',$post,60);

                #i: Regirect to register
                return Redirect::action('StaffController@getRegister');

            }else{
                #i: Staff not exist
                throw new Exception(trans('advance::realm.staff.text.not_exist'));
            }

        }catch (\Exception $e){
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        #i: Loading view
        $this->layout->content = View::make('staff.verify',$post);
    }


    /**
     * GET : Register
     *
     */
    public function getRegister(){
        $get = Cookie::get('verify');

        try{
            if( isset($get['salary_id']) ) $get['staff'] = Album::find($get['salary_id']) or null;

        }catch (\Exception $e){
            $get['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        #i: Loading view
        $this->layout->content = View::make('staff.register',$get);
    }
}