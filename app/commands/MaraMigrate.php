<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Atlantis\Helpers\Facades\Helpers;


class MaraMigrate extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'mara:migrate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Mara database migration';


    public $fields = array(
        'email'             => 'emel',
        'first_name'        => 'nama',
        'password'          => 'katalaluan',
        'idno_ic'           => 'no_kp',
        'birth_date'        => 'tarikh_lahir',
        'account_bank_code' => 'kod_bank',
        'account_bank_no'   => 'no_akaun_bank',
        'gender'            => 'jantina',
        'race'              => 'keturunan',

    );

    public $people_guardian_fields = array(
        'idno_ic'               => 'no_kp_penjaga',
        'first_name'            => 'nama_penjaga',
        'address_street'        => 'alamat_penjaga_1',
        'address_district'      => 'daerah_penjaga',
        'address_city'          => 'bandar_penjaga',
        'address_state'         => 'negeri_penjaga',
        'address_postcode'      => 'poskod_penjaga',
        'contact_home'          => 'no_tel_rumah_penjaga',
        'contact_mobile'        => 'no_tel_bimbit_pejabat_penjaga',
    );

    public $people_guarantor_fields = array(
        'idno_ic'               => 'no_kp_penjamin',
        'first_name'            => 'nama_penjamin',
        'address_street'        => 'alamat_penjamin_1',
        'address_district'      => 'daerah_penjamin',
        'address_city'          => 'bandar_penjamin',
        'address_state'         => 'negeri_penjamin',
        'address_postcode'      => 'poskod_penjamin',
        'contact_home'          => 'no_tel_rumah_penjamin',
        'contact_mobile'        => 'no_tel_bimbit_pejabat_penjamin',
    );

    public $detail_advance_fields = array(
        'amount_total'          => 'jum_dipohon',
        'application_existing'  => 'pernah_memohon',
        'application_coursed'   => 'kursus',
        'application_location'  => 'tempat_memohon',
        'institution_name'      => 'institusi',
        'course_level'          => 'peringkat_pengajian',
        'course_start'          => 'tkh_mula_kursus',
        'course_end'            => 'tkh_tamat_kursus',
        'guardian_salary_gross' => 'pendapatan_kasar_penjaga',
        'guardian_family_no'    => 'bil_tanggungan_penjaga',
        'guardian_employment'   => 'pekerjaan_penjaga',
        'guarantor_salary_gross'=> 'pendapatan_kasar_penjamin',
        'guarantor_status'      => 'status_penjamin',
        'status'                => 'status'
    );

    public $map_state = array(
        '07' => '01', //'Johor'
        '12' => '02', //'Kedah'
        '16' => '03', //'Kelantan'
        '08' => '04', //'Melaka'
        '09' => '05', //'Negeri Sembilan'
        '13' => '06', //'Pahang'
        '14' => '07', //'Pulau Pinang'
        '11' => '08', //'Perak'
        '15' => '09', //'Perlis'
        '10' => '10', //'Selangor'
        '17' => '11', //'Terengganu'
        '18' => '12', //'Sabah'
        '19' => '13', //'Sarawak'
        '20' => '14', //'Wilayah Persekutuan Kuala Lumpur'
    );
    
	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        if($this->argument('type') == 'all' || $this->argument('type') == 'user') $this->migrateUser();
        if($this->argument('type') == 'all' || $this->argument('type') == 'profile') $this->migratePeople();
        if($this->argument('type') == 'all' || $this->argument('type') == 'record') $this->migrateDetail();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
			array('source', InputArgument::REQUIRED, 'Please input source connection name.'),
            array('destination', InputArgument::OPTIONAL, 'Please input destination connection name.', 'mysql'),
            array('type', InputArgument::OPTIONAL, 'Please input a migration type: all, user, profile & record', 'all')
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			array('queue', null, InputOption::VALUE_OPTIONAL, 'Using queue for data inserting task, eg: "--queue=true"', null),
		);
	}


    protected function migrateUser(){
        $this->info('@Migrating student information..');
        try{
            #i: Registering student
            $students = DB::connection($this->argument('source'))
                ->table('pemohon');

            $students->chunk(300, function($students){
                if( $students ){
                    foreach( $students as $student ){
                        #i: Register user through provider
                        if( $this->option('queue') ){
                            #i: Using queue
                            \Queue::push('MaraMigrate@userRegister',array('student'=>$student));
                            $this->info('User registration push to queue : ' . $student->{$this->fields['first_name']});

                        }else{
                            $this->userRegister(null,array('student'=>$student));
                        }
                    }
                }
            });

        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }


    protected function migratePeople(){
        $this->info('@Migrating people information..');
        try{
            #i: Migrating peoples
            $applications = DB::connection($this->argument('source'))
                ->table('permohonan');

            $applications->chunk(300, function($applications){
                if( $applications ){
                    $i = 0;
                    foreach( $applications as $application ){
                        #i: Migrating people through provider
                        if( $this->option('queue') ){
                            #i: Using queue
                            \Queue::push('MaraMigrate@peopleNew',array('application'=>$application,'type'=>'guardian'));
                            \Queue::push('MaraMigrate@peopleNew',array('application'=>$application,'type'=>'guarantor'));
                            $this->info('People information push to queue #'.(string)$i++);

                        }else{
                            #i: Direct migration
                            $this->peopleNew(null, array('application'=>$application,'type'=>'guardian'));
                            $this->peopleNew(null, array('application'=>$application,'type'=>'guarantor'));
                        }
                    }
                }
            });

        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }


    protected function migrateDetail(){
        $this->info('@Migrating record information..');
        try{
            #i: Migrating peoples
            $applications = DB::connection($this->argument('source'))
                ->table('permohonan');

            $applications->chunk(300, function($applications){
                if( $applications ){
                    $i = 0;
                    foreach( $applications as $application ){
                        #i: Migrating people through provider
                        $application_statuses = DB::connection($this->argument('source'))
                            ->table('status_permohonan')
                            ->where('permohonan_id','=',$application->permohonan_id)
                            ->get();

                        if( $this->option('queue') ){
                            #i: Using queue
                            \Queue::push('MaraMigrate@detailNew',array('application'=>$application,'application_statuses'=>$application_statuses));
                            $this->info('Application information push to queue #'.(string)$i++);

                        }else{
                            #i: Direct migration
                            $this->detailNew(null, array('application'=>$application,'application_statuses'=>$application_statuses));
                        }
                    }
                }
            });

        }catch (Exception $e){
            $this->error($e->getMessage());
        }
    }


    public function userRegister($job,$data){
        $student = (object)$data['student'];

        if( \User::where('email','=',$student->{$this->fields['email']})->first() ) return true;

        $people_name = Helpers::string()->people_name($student->{$this->fields['first_name']});

        $user = \Sentry::register( array(
            'email' => strtolower($student->{$this->fields['email']}),
            'password' => $student->{$this->fields['password']},
            'first_name' => Str::title( $people_name['first_name'] ),
            'last_name' => Str::title( $people_name['last_name'] ),
        ), true);

        if($user){
            #i: Value override
            if( $student->{$this->fields['gender']} == 'L' ) $student->{$this->fields['gender']} = 'male';
            else $student->{$this->fields['gender']} = 'female';

            #i: Add group
            $group = Sentry::findGroupByName('student');
            $user->addGroup($group);

            #i: Create user profile
            $profile = new \People();
            $profile->idno_ic = $student->{$this->fields['idno_ic']};
            $profile->birth_date = $student->{$this->fields['birth_date']};
            $profile->gender = $student->{$this->fields['gender']};
            $profile->race = $student->{$this->fields['race']};
            $profile->save();

            #i: Create Student data and add to profile
            $people_student = new People\Student();
            $people_student['account_bank_code'] = $student->{$this->fields['account_bank_code']};
            $people_student['account_bank_no'] = $student->{$this->fields['account_bank_no']};
            $people_student->save();

            #i: Attach Staff data to profile
            $people_student->people()->save($profile);

            #i: Attach profile to user
            $user = \User::find($user->id);
            $user->profile()->save($profile);

            #i: Get user role
            $student_role_name = \Config::get('admin::user.role_student');
            $role = Role::where('name','=',$student_role_name)->first();
            $user->roles()->attach($role->id);

            #i:
            $message = 'Successfully register user : ' . $user->first_name;
            if(!$job) $this->info($message);
            else print $message."\n";

        }else{
            $message = 'Unable to register user : ' . $student->{$this->fields['first_name']};
            if(!$job) $this->error($message);
            else print $message."\n";

            return false;
        }

        if($job) $job->delete();
        return $user;
    }


    public function peopleNew($job,$data){
        $application = (object)$data['application'];
        $type = $data['type'];

        #i: Array stub
        $field_array_name = 'people_' . $type  . '_fields';
        $people_fields = $this->{$field_array_name};

        #i: Check existing people
        if( \People::where('idno_ic','=',$application->{$people_fields['idno_ic']})->first() ){
            if($job) $job->delete();
            return true;
        }

        #i: Create a people
        try{
            $people_info = new \People();
            foreach( $people_fields as $field => $map ){
                $people_info->{$field} = Str::title( $application->{$map} );
            }

            #i: Name override
            $people_name = Helpers::string()->people_name($people_info->first_name);
            $people_info->first_name = Str::title( $people_name['first_name'] );
            $people_info->last_name = Str::title( $people_name['last_name'] );
            $people_info->gender = $people_name['gender'];
            $people_info->address_state = $this->map_state[$people_info->address_state];

            #i: Saving people
            $people_info->save();

            $message = 'Successfully add people info : '.$people_info->first_name;
            if(!$job) $this->info($message);
            else print $message."\n";

        }catch (Exception $e){
            if(!$job) $this->error($e->getMessage());
            else print $e->getMessage()."\n";

            return false;
        }

        if($job) $job->delete();
        return $people_info->id;
    }


    public function detailNew($job,$data){
        $application = (object)$data['application'];
        $application_statuses = (object)$data['application_statuses'];

        #i: Check existing detail
        if( \People::where('meta','LIKE', '%'.$application->permohonan_id.'%' )->first() ) {
            if($job) $job->delete();
            return true;
        }

        #i:
        try{
            $record = new \Record();
            foreach( $this->detail_advance_fields as $field => $map ){
                $record->{$field} = $application->{$map};
            }

            #i: Data massage
            $record->application_location = str_pad($record->application_location,4,'0',STR_PAD_LEFT);

            #i: Link with application
            $detail = Detail::where('name','=','advance')->first();
            $record->application_id = $detail->id;

            #i: Link with user
            $people = \People::where('idno_ic','=',$application->{$this->fields['idno_ic']})->first();
            $record->user_id = $people->user->id;

            #i: Link with guardian
            $guardian = \People::where('idno_ic','=',$application->{$this->people_guardian_fields['idno_ic']})->first();
            if( $guardian ){
                $record->guardian_id = $guardian->id;
            }else{
                $record->guardian_id = $this->peopleNew($application,'guardian');
            }

            #i: Link with guarantor
            $guarantor = \People::where('idno_ic','=',$application->{$this->people_guarantor_fields['idno_ic']})->first();
            if( $guarantor ){
                $record->guarantor_id = $guarantor->id;
            }else{
                $record->guarantor_id = $this->peopleNew($application,'guarantor');
            }

            #i: Saving details
            $record->save();

            if( $record->status['id'] > 0 ){
                #i: Get admin user
                $admin = Sentry::findAllUsersWithAccess('superuser')[0];

                #i: Create conversation
                $conversation = Conversation::create(array('subject' => $record->uuid));

                foreach($application_statuses as $status){
                    $status = (object)$status;

                    #i: Attach message
                    $message = Message::create([
                        'conversation_id' => $conversation->id,
                        'user_id' => $admin->id,
                        'body' => $status->catatan
                    ]);
                    $message->meta = array(
                        'detail' => array(
                            'status' => $status->kod_status
                        ),
                        'permission' => array(
                            'read' => 'staff',
                            'reply' => 'staff'
                        )
                    );
                    $message->save();

                    #i: Attach participant
                    Participant::create([
                        'conversation_id' => $conversation->id,
                        'user_id' => $record->user->id
                    ]);
                }
            }

            #i:
            $message = 'Successfully migrating application info : '.$record->uuid;
            if(!$job) $this->info($message);
            else print $message."\n";

        }catch (Exception $e){
            $message = $e->getMessage();
            if(!$job) $this->error($message);
            else print $message."\n";

            return false;
        }

        if($job) $job->delete();
        return $record;
    }

}