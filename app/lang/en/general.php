<?php return array(

    'title' => array(
        'yes'           => 'Yes',
        'no'            => 'No',
        'close'         => 'Close',
        'reset'         => 'Reset',
        'verify'        => 'Verify',
        'search'        => 'Search',
        'update'        => 'Update',
        'send'          => 'Send',
        'notify'        => 'Notify',
        'back'          => 'Back',
        'created_at'    => 'Created at',
        'updated_at'    => 'Last updated at',
        'print'         => 'Print'
    ),

    'text' => array(
        'copyright'     => 'Copyright &copy; ' . date('Y'),
        'browser'       => 'Optimized on Mozilla Firefox 11.0 &amp; Internet Explorer 10 above',
    )

);