<?php return array(

    'title' => array(
        'yes'           => 'Ya',
        'no'            => 'Tidak',
        'close'         => 'Tutup',
        'reset'         => 'Reset',
        'verify'        => 'Sahkan',
        'search'        => 'Carian',
        'update'        => 'Kemaskini',
        'send'          => 'Hantar',
        'notify'        => 'Makluman',
        'back'          => 'Kembali',
        'created_at'    => 'Cipta pada',
        'updated_at'    => 'Pindaan terakhir pada',
        'print'         => 'Cetak'
    ),

    'text' => array(
        'copyright'     => 'Hakcipta &copy; ' . date('Y'),
        'browser'       => 'Sesuai dipapar menggunakan Mozilla Firefox 11.0 &amp; Internet Explorer 9 ke atas',
    )

);