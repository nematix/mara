<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Penomboran Halaman Talian Bahasa
    |--------------------------------------------------------------------------
    |
    | Garis bahasa berikut digunakan oleh perpustakaan penomboran untuk membina
    | pautan penomboran yang mudah. Anda bebas untuk menukar mereka kepada apa sahaja
    | anda ingin menyesuaikan paparan anda agar lebih sesuai dengan applikasi.
    |
    */

    'previous' => '&laquo; Sebelum',

    'next'     => 'Selepas &raquo;',

);
