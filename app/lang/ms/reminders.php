<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Katalaluan mestilah dalam 6 aksara dan sepadan dengan ulangan.",

	"user"     => "Pengguna dengan alamat e-mail tidak wujud.",

	"token"    => "Token untuk penyahaktifan katalaluan adalah salah.",

);