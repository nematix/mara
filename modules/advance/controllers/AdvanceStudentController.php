<?php

namespace Modules\Advance;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Config;
use Atlantis\Core\Controller\BaseController;
use Detail, Record;
use Ramsey\Uuid\Console\Exception;


class AdvanceStudentController extends BaseController {

    protected $detail;


    public function __construct(){
        parent::__construct();

        $this->detail = Detail::find('advance');
    }


    /**
     * Detail New
     *
     * @param $name
     * @return mixed
     */
    public function getNew(){
        $data = array();

        try{
            if($this->detail){
                $data['detail'] = $this->detail;;

                $data['header'] = array(
                    'icon'  => 'fa fa-tasks',
                    'title' => trans('advance::advance.title.application_list'),
                    'sub'   => '',
                );

                #i: Detail configs
                $detail_detail_max = $this->detail->config->detail_max;

                #i: Get profile(people) id
                $user = \User::find($this->user->id);

                /** Check for user profile completion */
                if( !$user->profile->is_completed){
                    $user_profile = url("user/profile?return=".route('advance.student.new',['name'=>'advance']));
                    throw new Exception(trans('advance::advance.text.user_profile_not_complete',array('url'=>$user_profile)));
                }

                #i: Get application details for current user
                $records = $this->detail->records()->where('user_id','=',$user->id)->get();

                #i: Verified application details max
                if( count($records) >= $detail_detail_max ){
                    $data['records'] = $records;
                    $data['header']['sub'] =  trans('advance::advance.text.application_total', array('total'=>count($records)));
                    $data['_status'] = array(
                        'type' => 'warning',
                        'message' => Lang::get('advance::advance.text.application_max',array('total'=>$detail_detail_max))
                    );

                }else{
                    #i: Create application
                    $record = new Record;
                    $record->user_id = $user->id;
                    $record->amount_total = $this->detail->config->amount_total;

                    #i: Saving application detail
                    $this->detail->records()->save($record);

                    #i: Route to edit page
                    return Redirect::route('advance.student.edit',array($record->uuid));
                }
            }

        }catch(Exception $e){
            $data['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        $this->layout->content = View::make("{$this->detail->name}::student.manage", $data);
    }


    /**
     * Detail Manage
     *
     * @param $name Application name
     */
    public function getManage(){
        $data = array();

        try{
            #i: Get profile(people) id
            $user = \Sentry::getUser();
            $user = \User::find($user->id);

            #i: Get application details for current user
            $records = $this->detail->records()->where('user_id','=',$user->id)->get();

            $data['detail'] = $this->detail;
            $data['records'] = $records;
            $data['header'] = array(
                'icon'  => 'fa fa-tasks',
                'title' => trans('advance::advance.title.application_list'),
                'sub'   => trans('advance::advance.text.application_total', array('total'=>count($records))),
            );

        }catch(Exception $e){
            $data['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        $this->layout->content = View::make("{$this->detail->name}::student.manage", $data);
    }


    /**
     * Detail Update
     *
     * @params $name Application name
     * @params $uuid Detail UUID
     */
    public function getEdit($uuid){
        #i: Check for user role, if staff trigger manage
        if( $this->user_realm == 'staff' ) return Redirect::route('advance.staff.manage',$uuid);

        #i: Variables
        $data = array();
        $documents = array();

        try{
            if($this->detail){
                #i: Get application details for current user
                if( empty($uuid) ){
                    $record = $this->detail->records()->first();
                }else{
                    $record = $this->detail->records()->where('uuid','=',$uuid)->first();
                }

                foreach( $record->documents as $document ){
                    $documents = array_merge($documents, array(
                        $document->key => $document
                    ));
                }

                #i: Load guardian or guarantor if found
                if( $record->has('guardian')->count() > 0 ) $record->load('guardian');
                if( $record->has('guarantor')->count() > 0 ) $record->load('guarantor');

                #i: Add detail to view data
                if( $record ){
                    $data['record'] = $record;
                    $data['document_keys'] = Config::get('advance::advance.application.documents.input');
                    $data['documents'] = $documents;
                    $data['conditions_disable'] = Config::get('admin::admin.workflow.condition.disable');
                    $data['config'] = Config::get('advance::advance');
                }

                #i: Inline data populate
                \Former::populate($record);
            }

        }catch(Exception $e){
            $data['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        #i: Render
        $data['detail'] = $this->detail;
        $this->layout->content = View::make("{$this->detail->name}::student.edit", $data);
    }


    /**
     * Detail Update : POST
     *
     * @params $name Application name
     * @params $uuid Detail UUID
     */
    public function postUpdate($uuid){
        #i: Check for user role of Staff
        if( $this->user_realm == 'staff' ) return $this->postUpdateManage($uuid);

        $this->getStudentEdit($uuid);
    }
} 
