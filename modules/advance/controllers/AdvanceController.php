<?php

namespace Modules\Advance;

use Illuminate\Support\Facades\Redirect;
use Atlantis\Core\Controller\BaseController;


class AdvanceController extends BaseController {

    /**
     * Index
     *
     * @return mixed
     */
    public function getIndex(){
        $default = '';

        if( $this->user_realm == 'student' ) $default = 'manage';
        if( $this->user_realm == 'staff' ) $default = 'browse';

        return Redirect::route("advance.{$this->user_realm}.$default")->withInput();
    }

}