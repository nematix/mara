<?php

namespace Api\V1\Advance;

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Atlantis\Core\Controller\BaseController;
use User,People;


class StaffController extends BaseController{

    public function index(){
        $get = Input::all();

        if( isset($get['iDisplayLength']) ){
            #i: Pagination setup
            $current_page = ($get['iDisplayStart'] / $get['iDisplayLength']) + 1;
            $get['iTotalRecords'] = User::count();
            User::resolveConnection()->getPaginator()->setCurrentPage($current_page);

            #i: Filtering result
            if( isset($get['search']) ){
                $staffs = User::with('roles','profile','profile.data')->filtering($get['search']);

            }else{
                $staffs = User::with('roles','profile','profile.data');
            }

            $staffs->whereHas('groups', function($q){
                $q->where('name','staff');
            });

            #i: Filtered record count
            $get['iTotalDisplayRecords'] = $staffs->count();

            #i: Fetching result
            $staffs = $staffs->paginate($get['iDisplayLength']);

            #i: Collecting result
            $get['aaData'] = $staffs->toArray()['data'];

        }else{
            $staffs = User::all();
            $staffs->load('user','user.profile');
            $get = $staffs;
        }

        return $get;
    }

}