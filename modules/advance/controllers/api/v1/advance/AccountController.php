<?php

namespace Api\V1\Advance;

use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use \Illuminate\Support\Facades\Validator;
use Atlantis\Core\Controller\BaseController;
use Modules\Advance\Account;


class AccountController extends BaseController{

    /**
     * Index
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(){
        $get = Input::all();

        /** @var $accounts */
        if( isset($get['search']) ){
            $accounts = Account::filtering($get['search']);
        } else {
            $accounts = Account::all();
        }

        /** Check for Table format */
        if( isset($get['iDisplayLength']) ){
            /** Pagination setup */
            $current_page = ($get['iDisplayStart'] / $get['iDisplayLength']) + 1;
            $get['iTotalRecords'] = Account::count();
            Account::resolveConnection()->getPaginator()->setCurrentPage($current_page);

            /** Filtered record count */
            $get['iTotalDisplayRecords'] = $accounts->count();

            /** Fetching result */
            $accounts = $accounts->paginate($get['iDisplayLength']);

            /** Collecting result */
            $get['aaData'] = $accounts->toArray();

        }else{
            $get = $accounts;
        }

        return $get;
    }


    /**
     * Show account
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\Collection|null|static
     */
    public function show($id){
        /** Search detail */
        $account = Account::find($id);

        /** Return detail if found */
        if($account){
            return $account;
        }

        /** Return error if none */
        return Response::json(array('Error in query'),400);
    }


    /**
     * Update account
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($id){
        $put = Input::all();

        try{
            /** @var Validator $validation Inputs validation */
            $validation = Validator::make($put,['account_number' => "required|digits:6|account_series:$id"]);
            if($validation->fails()){
                throw new Exception( array_reduce( $validation->messages()->toArray(), function($result, $message){
                    $message = trans("advance::{$message[0]}");
                    $result .= "{$message} ";
                    return $result;
                }) );
            }

            /** Search detail */
            $account = Account::find($id);

            /** Update detail */
            if($account){
                /** Fill model */
                $account->fill($put);

                /** Saving */
                $account->save();
            }

            /** Response */
            $put['_status'] = array(
                'type' => 'success',
                'message' => 'Successfully update the budget!'
            );

        }catch (Exception $e){
            $put['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        return Response::json($put);
    }

}