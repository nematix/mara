<?php

namespace Api\V1\Advance;

use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Atlantis\Core\Controller\BaseController;
use Modules\Advance\Account;
use Modules\Advance\Transaction;


class AccountBudgetController extends BaseController{

    public function store($accountId){
        $post = Input::all();

        try {
            /** Search detail */
            $account = Account::find($accountId);

            /** Debit budget to account */
            $account->debit('advance:budget',$post['budget_amount']);

            /** Response */
            $post['_status'] = array(
                'type' => 'success',
                'message' => 'Successfully update the budget! for ' . $account->state_name
            );

        } catch (Exception $e){
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );

        }

        return Response::json($post);
    }

}