<?php

namespace Api\V1\Detail;

use Exception;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Atlantis\Core\Controller\BaseController;
use Atlantis\Message\Api\V1\MessageController;


class StatusController extends BaseController {

    /**
     * Store
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(){
        $post = Input::all();

        try{
            $messages = array(
                'body.required' => trans('validation.required',array('attribute'=>trans('advance::advance.title.comment'))),
            );

            $rules = array(
                'sender_id' => 'required',
                'receiver_id' => 'required',
                'subject' => 'required',
                'body' => 'required'
            );

            /** Message validation */
            $validation = Validator::make($post,$rules,$messages);
            if( $validation->fails() ) throw new Exception($validation->messages()->first());

            /** Default permissions */
            $post['meta'] = array(
                'permission' => array(
                    'read' => 'staff',
                    'reply' => 'staff'
                )
            );

            /** Assign status to message */
            if( isset($post['status']) ) $post['meta']['detail'] = array('status' => $post['status']);

            /** Notify user, set permission read by all group but no reply */
            if( isset($post['notify']) ) $post['meta']['permission'] = array('reply'=>'staff');

            /** Create message */
            $message = new MessageController();
            $message->store($post);

            $post['_status'] = array(
                'type' => 'success',
                'message' => 'Successfully updating detail status'
            );

        } catch(Exception $e){
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        /** Server response */
        return Response::json($post);
    }

}