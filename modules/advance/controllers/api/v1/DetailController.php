<?php

namespace Api\V1;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Atlantis\Core\Controller\BaseController;
use Atlantis\Message\Api\V1\MessageController;
use User,Record,People,Blacklist;
use Modules\Advance\Account;

class DetailController extends BaseController{

    /**
     * Index
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(){
        $get = Input::all();

        if( isset($get['iDisplayLength']) ){
            #i: Pagination setup
            $current_page = ($get['iDisplayStart'] / $get['iDisplayLength']) + 1;
            $get['iTotalRecords'] = Record::count();
            Record::resolveConnection()->getPaginator()->setCurrentPage($current_page);

            #i: Filtering result
            if( isset($get['search']) ){
                $records = Record::with('user','user.profile')->filtering($get['search']);

            }else{
                $records = Record::with('user','user.profile');
            }

            #i: Filtered record count
            $get['iTotalDisplayRecords'] = $records->count();

            #i: Fetching result
            $records = $records->paginate($get['iDisplayLength']);

            #i: Collecting result
            $get['aaData'] = $records->toArray()['data'];

        }else{
            $records = Record::all();
            $records->load('user','user.profile');
            $get = $records;
        }

        return $get;
    }


    /**
     * Show
     *
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Support\Collection|null|static
     */
    public function show($uuid){
        #i: Search detail
        $record = Record::find($uuid);

        #i: Return detail if found
        if($record){
            #i: Load model relations
            $record->load('user','user.profile','user.profile.data');

            #i: Load guardian or guarantor if found
            if( $record->has('guardian')->count() > 0 ) $record->load('guardian');
            if( $record->has('guarantor')->count() > 0 ) $record->load('guarantor');

            /** Load transaction record if exist */
            if( $record->has('transaction')->count() > 0 ) $record->load('transaction');

            return $record;
        }

        #i: Return error if none
        return Response::json(array('Error in query'),400);
    }


    /**
     * Update
     *
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function update($uuid){
        $put = Input::all();

        try{
            /** Search for record */
            $record = Record::with(array('user','user.profile','user.profile.data'))->find($uuid);

            if($record){
                /** Field validation, idno_ic must be unique */
                $validator = Validator::make($put, array('user.profile.idno_ic'=>'unique:peoples,idno_ic,'.$record->user_id.',user_id'));
                if($validator->fails()) throw new \Exception($validator->messages()->first());

                /** Check status override, blacklist */
                if( $put['status']['id'] == 1 ){
                    $blacklist = Blacklist::find(array_get($put,'user.profile.idno_ic'));

                    /** If found, status to not eligible(2) */
                    if($blacklist) {
                        $put['status'] = 2;

                        /** @var $info array Blacklist information */
                        $info = array(
                            'sender_id' => 1,
                            'receiver_id' => $record->user_id,
                            'subject' => $uuid,
                            'body' => trans(
                                'advance::advance.text.application_update_reason',
                                array(
                                    'status'=>trans('advance::advance.status.staff.not_eligible'),
                                    'reason'=>trans('advance::advance.blacklist.modal-body',array('idno_ic'=>array_get($put,'user.profile.idno_ic')))
                                )
                            ),
                            'meta' => array(
                                'permission' => array('reply'=>'staff')
                            )
                        );

                        /** Create message */
                        $message = new MessageController();
                        $message->store($info);
                    }
                }

                /** Fill model */
                $record->fill($put);

                /** Relations array */
                $detail_relations = array(
                    'guardian_id'   => 'guardian',
                    'guarantor_id'  => 'guarantor'
                );

                /** Load relations */
                foreach($detail_relations as $key => $relation ){
                    /** If guardian/guarantor id relation exist */
                    if( !empty($put[$key]) ){
                        /** Check if relation already bind */
                        if( $record->{$key} == $put[$key] ){
                            $record->{$relation}->fill($put[$relation]);

                        /** Seek for relation by id the bind with detail */
                        }else{
                            $people = People::find($put[$key]);
                            $record->{$relation}()->associate($people);
                        }

                    }else{
                        /** Relation not exist and new */
                        if( isset($put[$relation]) ){
                            $people = new People();
                            $people->fill($put[$relation]);
                            $record->{$relation}()->save($people);
                            $put[$key] = $record->{$key} = $people->id;
                        }
                    }
                }

                /** Saving */
                $record->user->fill($put['user']);
                $record->user->profile->fill($put['user']['profile']);
                $record->user->profile->data->fill($put['user']['profile']['data']);
                $record->push($put);
            }

            /** Response */
            $put['_status'] = array(
                'type' => 'success',
                'message' => 'Successfully update the record!'
            );

        }catch (Exception $e){
            $put['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        return Response::json($put);
    }


    /**
     * Destroy
     *
     * @param $uuid
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($uuid){
        $delete = Input::all();

        //[i] Search user
        $record = Record::find($uuid);

        if($record){
            $record->delete();
            $delete['_status'] = array(
                'type' => 'success',
                'message' => 'Successfully delete the record!'
            );

        }else{
            $delete['_status'] = array(
                'type' => 'error',
                'message' => 'Error deleteing record!'
            );
        }

        return Response::json($delete);
    }


    /**
     * Clear
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function clear(){
        $post = Input::all();

        try{
            //[i] Search for user and delete all detail
            $user = User::find($post['user_id']);
            $user->details()->delete();

            //[i] Notification
            $post['_status'] = array(
                'type' => 'success',
                'message' => 'Sucessfully delete all record'
            );

        } catch(Exception $e){
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        return Response::json($post);
    }


    /**
     * Missing method
     *
     * @param array $parameters
     * @return \Illuminate\Http\JsonResponse
     */
    public function missingMethod($parameters = array()){
        $post['_status'] = array(
            'type' => 'error',
            'message' => 'Operation not permitted!'
        );

        return Response::json($post);
    }

}