<?php

namespace Api\V1;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Atlantis\Content\Api\V1\DocumentController as BaseController;
use Document,Record;


class DocumentController extends BaseController{

    /**
     * Store document
     *
     * @return mixed
     */
    public function store(){
        $post = Input::all();

        try{
            $file = Input::file('qqfile');

            #i: Process document
            $document = new Document();
            $document->uuid = $post['qquuid'];
            $document->key = $post['document_key'];
            $document->image = $file;

            #i: Find detail
            $record = Record::find($post['record_uuid']);


            DB::beginTransaction();
            if($record){
                #i: If found save document into detail
                $record->documents()->save($document);

            }else{
                $document->save();
            }

            DB::commit();
            #i: Notification
            $post['success'] = true;

        } catch(\Exception $e){
            DB::rollback();
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        #i: Server response
        return Response::json($post);
    }


    /**
     * Documents clear
     *
     * @return mixed
     */
    public function clear(){
        $post = Input::all();

        try{
            #i: Search for detail & delete all documents
            $record = Record::find($post['detail_uuid']);
            $record->documents()->delete();

            #i: Notification
            $post['_status'] = array(
                'type' => 'success',
                'message' => 'Sucessfully delete all related documents'
            );

        } catch(\Exception $e){
            $post['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        #i: Server response
        return Response::json($post);
    }

}