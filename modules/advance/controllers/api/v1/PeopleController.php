<?php

namespace Api\V1;

use Atlantis\Core\Controller\BaseController;


class PeopleController extends BaseController{

    /**
     * Index
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function index(){
        return \People::all();
    }


    /**
     * Show People Data
     *
     * @param $id
     * @return \Illuminate\Support\Collection|null|static
     */
    public function show($id){
        $people = \People::find($id);

        /** Return people if found */
        if($people){
            return $people;
        }

        /** Return error if none */
        return \Response::json(array('Error in query'),400);
    }


    /**
     * Search People
     *
     * @param string $idno_ic
     * @return mixed
     */
    public function search($idno_ic=''){
        $get = \Input::all();
        if( empty($idno_ic) ) $idno_ic = $get['idno_ic'];

        /** Check for blacklist */
        $blacklist = \Blacklist::find($idno_ic);

        if($blacklist){
            $get['blacklist'] = 1;
        }

        /** Search people */
        $people = \People::where('idno_ic','=',$idno_ic)->first();

        /** Return people id if found */
        if($people){
            $get['id'] = $people->id;
            $get['first_name'] = $people->first_name;
            $get['last_name'] = $people->last_name;
        }

        return \Response::json($get);
    }

}