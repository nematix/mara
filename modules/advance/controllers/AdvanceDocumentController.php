<?php

namespace Modules\Advance;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Atlantis\Core\Controller\BaseController;
use Record;


class AdvanceDocumentController extends BaseController {

    public function getDownload(){
        $get = Input::all();

        $name = 'transform'.studly_case($get['name']);
        return $this->{$name}($get['uuid']);
    }


    /**
     * Approval document
     *
     * @param $uuid
     * @return mixed
     */
    protected function transformApproval($uuid){
        $record = Record::find($uuid);

        if($record){
            $converter = App::make('document.converter');
            $filename = Config::get('advance::advance.documents.offer_letter.file_name');

            /** Fetch and construct data from record */
            $data = array(
                'account_number' => $record->transaction->credit_account_id,
                'full_name' => strtoupper($record->user->full_name),
                'idno_ic' => $record->user->profile->idno_ic,
                'full_address' => strtoupper(View::make('advance::partials.user-address',array('user'=>$record->user))),
                'institution_name' => strtoupper($record->institution_name),
                'course_name' => strtoupper($record->application_coursed),
                'amount_text' => strtoupper(app('atlantis.helpers')->advance->spellNumeric($record->transaction->credit)),
                'amount_value' => $record->transaction->credit,
                'date' => $record->updated_at->format('d M Y'),
                'application_location' => strtoupper($record->application_location_name)
            );

            /** Convert view */
            $letter = $converter('pdf')->loadView('advance::documents.approval',$data);
            return $letter->download($filename);
        }
    }


    /**
     * Applicant info
     *
     * @param $uuid
     * @return mixed
     */
    protected function transformApplication($uuid){
        $record = Record::find($uuid);

        if($record){
            /** Converter instance */
            $converter = App::make('document.converter');
            $filename = Config::get('advance::advance.documents.applicant_info.file_name');

            /** Fetch and construct data from record */
            $data = array(
                'record' => $record,
                'account_number' => $record->transaction->credit_account_id
            );

            /** Convert view */
            $letter = $converter('pdf')->loadView('advance::documents.application',$data);
            return $letter->download($filename);
        }
    }


    /**
     *
     *
     * @param $uuid
     * @return mixed
     */
    protected function transformVoucher($uuid){
        $record = Record::find($uuid);

        if($record){
            /** Fetch and construct data from record */
            $data = array(
                'detail' => $record,
                'account_number' => $record->transaction->credit_account_id
            );

            /** Converter instance */
            $converter = App::make('document.converter');
            $filename = Config::get('advance::advance.documents.payment_voucher.file_name');

            /** Convert view */
            $letter = $converter('pdf')->loadView('advance::documents.voucher',$data);
            return $letter->download($filename);
        }
    }


    protected function transformAgreement($uuid){}

}