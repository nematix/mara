<?php

namespace Modules\Advance;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Config;
use Atlantis\Core\Controller\BaseController;
use Atlantis\Workflow\Resources\Status;
use Former\Facades\Former;
use User, Detail, Record;


class AdvanceStaffController extends BaseController {

    protected $detail;


    public function __construct(){
        parent::__construct();

        $this->detail = Detail::find('advance');
    }


    /*******************************************************************************************************************
     * Browse Record
     *
     * @params $name Application name
     ******************************************************************************************************************/
    public function getBrowse(){
        $data['detail'] = $this->detail;
        $data['header'] = array(
            'icon'  => 'fa fa-tasks',
            'title' => trans('advance::advance.title.application_browse'),
            'sub'   => trans('advance::advance.text.application_total', array('total' => $data['detail']->records->count()))
        );

        $data['status'] = array( 'all' => (new Status($this->realm))->all());

        $this->layout->content = View::make("{$this->detail->name}::staff.browse", $data);
    }


    /*******************************************************************************************************************
     * Statistic
     *
     * @params $name Application name
     ******************************************************************************************************************/
    public function getStatistic(){
        $data = Input::all();

        $data['detail'] = $this->detail;
        $data['header'] = array(
            'icon'  => 'fa fa-bar-chart-o',
            'title' => trans('advance::advance.title.application_statistic'),
            'sub'   => 'Pusat : ' . \User::find($this->user->id)->profile->data->center_name
        );

        $center_id = User::find($this->user->id)->profile->data->center_id;
        $statuses = $data['detail']->workflows('resource.status')->all();

        $advance_status_eligible = array(
            'title' => 'Permohonan berstatus layak',
            'value' => Record::center($center_id)->where('status',1)->count(),
            'class' => 'bg-'.$statuses[1]['label'],
            'status'=> $statuses[1]['title'],
            'status_label' => $statuses[1]['label'],
            'url'   => ''
        );
        $advance_status_accept = array(
            'title' => 'Permohonan yang berstatus terima',
            'value' => Record::center($center_id)->where('status',3)->count(),
            'class' => 'bg-'.$statuses[3]['label'],
            'status'=> $statuses[3]['title'],
            'status_label' => $statuses[3]['label']
        );

        $advance_status_support = array(
            'title' => 'Permohonan yang belum diluluskan',
            'value' => Record::center($center_id)->where('status',4)->count(),
            'class' => 'bg-'.$statuses[4]['label'],
            'status'=> $statuses[4]['title'],
            'status_label' => $statuses[4]['label']
        );

        $advance_status_consideration = array(
            'title' => 'Permohonan masih dalam pertimbangan',
            'value' => Record::center($center_id)->where('status',6)->count(),
            'class' => 'bg-'.$statuses[6]['label'],
            'status'=> $statuses[6]['title'],
            'status_label' => $statuses[6]['label']
        );

        $advance_status_pass = array(
            'title' => 'Permohonan masih belum disahkan status pembayaran',
            'value' => Record::center($center_id)->where('status',7)->count(),
            'class' => 'w2 bg-'.$statuses[7]['label'],
            'status'=> $statuses[7]['title'],
            'status_label' => $statuses[7]['label']
        );

        $data['statistics'] = array(
            $advance_status_eligible,
            $advance_status_accept,
            $advance_status_support,
            $advance_status_consideration,
            $advance_status_pass
        );

        $this->layout->content = View::make("{$this->detail->name}::staff.statistic", $data);
    }


    public function getEdit($uuid,$mode=''){
        if( $mode == 'approval' ){
            return $this->getEditReview($uuid);

        } elseif( $mode == 'payment' ){
            return $this->getEditPayment($uuid);

        } else {
            return $this->getEditReview($uuid);
        }
    }

    /*******************************************************************************************************************
     * Manage Detail Update
     *
     * @params $name Application name
     * @params $uuid Detail UUID
     ******************************************************************************************************************/
    public function getEditReview($uuid){
        /** Variables */
        $data = array();
        $documents = array();
        $document_keys = Config::get('advance::advance.application.documents.input');

        try{
            if($this->detail){
                /** Get application details for current user */
                $record = $this->detail->records()->where('uuid','=',$uuid)->first();

                /** Permission check */
                if( !User::find($this->user->id)->can('detail.status.'.$record->status['name'].'.update') ){
                    return Redirect::route('advance.staff.manage.review');
                }

                /** Get uploaded documents for record */
                foreach( $record->documents as $document ){
                    $documents = array_merge($documents, array(
                        $document->key => $document
                    ));
                }

                /** Get verified document in meta */
                if( !isset($record->meta->documents) ){
                    $_meta = (array) $record->meta;

                    foreach( $document_keys as $key ){
                        $_meta['documents'][$key] = array(
                            'verified' => ''
                        );
                    }

                    $record->meta = (object) $_meta;
                    $record->save();
                }

                /** Add detail to view data */
                if( $record ){
                    /** Load guardian or guarantor if found */
                    if( $record->has('guardian')->count() > 0 ) $record->load('guardian');
                    if( $record->has('guarantor')->count() > 0 ) $record->load('guarantor');

                    $data['record'] = $record;
                    $data['document_keys'] = $document_keys;
                    $data['documents'] = $documents;
                    $data['actions'] = Config::get('admin::admin.workflow.status.action')[$record->status['id']];
                    $data['actions_conditions'] = Config::get('admin::admin.workflow.condition.action');
                    $data['conditions_disable'] = Config::get('admin::admin.workflow.condition.disable');
                    $data['config'] = Config::get('advance::advance');

                    /** Fetch comments */
                    $conversation = \Conversation::forUser($record->user->id)->where('subject',$uuid)->first();
                    if( $conversation ) $data['comments'] = \Message::where('conversation_id',$conversation->id)->get();
                    $data['conversation'] = $conversation;

                    /** Populate form */
                    Former::populate($record);
                }
            }

        }catch(Exception $e){
            $data['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        /** Render */
        $data['detail'] = $this->detail;
        $this->layout->content = View::make("{$this->detail->name}::staff.detail", $data);
    }



    /**
     * Payment view
     *
     * @param $name
     */
    public function getEditPayment($uuid){
        $data = array();

        try{
            /** Throw exception */
            if(empty($this->detail)) throw new Exception('Fatal!, requested detail not found!');

            /** Find application record */
            $record = $this->detail->records()->where('uuid',$uuid)->first();

            /** Permission check */
            if( !User::find($this->user->id)->can('detail.status.'.$record->status['name'].'.update') ){
                return Redirect::route('advance.staff.manage.review');
            }

            if($record){
                $data['detail'] = $this->detail;
                $data['record'] = $record;
                $data['actions_conditions'] = array();
                $data['conditions_disable'] = array('form' => '1');
                $data['config'] = Config::get('advance::advance');
            }

        }catch(Exception $e){
            $data['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        $this->layout->content = View::make("{$this->detail->name}::staff.payment", $data);
    }


    /*******************************************************************************************************************
     *
     * @param $name
     * @param array $data_override
     ******************************************************************************************************************/
    public function  getManage($data_override = []){
        $data = \Input::all();

        try{
            /** Get current detail info */
            $data['detail'] = $this->detail;
            $data['header'] = array(
                'icon'  => 'fa fa-tasks',
                'title' => $data['detail']->description,
                'sub'   => trans('advance::advance.text.application_total', array('total' => $data['detail']->records->count())),
            );

            /** Status context permission filtering */
            $data['status_role'] = array_filter($data['detail']->status['context'], function($status){
                if( \User::find($this->user->id)->can("detail.status.{$status['name']}.update") ) return true;
            });

            /** Merge with data override if any */
            if( $data_override ) $data = array_merge($data,$data_override);

        }catch(Exception $e){
            $data['_status'] = array(
                'type' => 'error',
                'message' => $e->getMessage()
            );
        }

        $this->layout->content = View::make("{$this->detail->name}::staff.details", $data);
    }


    /**
     * Manage Review
     *
     * @param $name
     */
    public function getManageReview(){
        $data = array('mode' => 'review');

        $this->getManage($data);
    }


    /**
     * Manage Approve
     *
     * @param $name
     */
    public function getManageApprove(){
        $data = array('mode' => 'approve');

        $this->getManage($data);
    }


    /**
     * Manage Payment
     *
     * @param $name
     */
    public function getManagePayment(){
        $data = array('mode' => 'payment');

        $this->getManage($data);
    }


    /*******************************************************************************************************************
     * Manage Detail Update : POST
     *
     * @params $name Application name
     * @params $uuid Detail UUID
     ******************************************************************************************************************/
    public function postUpdateManage($uuid){
        /** POST vars */
        $post = Input::all();

        /** Get detail */
        $detail = Detail::find($uuid);

        /** Fill all attribute from POST */
        $detail->fill($post);

        /** Saving to model */
        $detail->push($post);

        /** Redirect to edit paga */
        $this->getUpdateManage($uuid);
    }

}