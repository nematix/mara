<?php

namespace Modules\Advance;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;
use Atlantis\Core\Controller\BaseController;
use Atlantis\User\Models\User;

class AdvanceManageController extends BaseController {

    /**
     * Staff Management
     *
     */
    public function getStaff(){
        /** @var array $data {
         *      @var string users name
         *      @var object User Get all staff user
         * }
         */
        $data['users'] = User::whereHas('groups', function($q){
            $q->where('name','staff');
        })->get();

        /** Configuring view header */
        $data['header'] = array(
            'icon'  => 'fa fa-user',
            'title' => trans('advance::'.app('router')->currentRouteName()),
            'sub'   => trans('advance::advance.title.staff_manage_total', array('total' => $data['users']->count()))
        );

        $this->layout->content = View::make("advance::admin.staff.browse", $data);
    }


    /**
     * Budget Management
     */
    public function getBudget(){
        $data['accounts'] = Account::all();

        /** Configuring view header */
        $budget_total = $data['accounts']->count();
        $data['header'] = array(
            'icon'  => 'fa fa-dollar',
            'title' => trans('advance::'.app('router')->currentRouteName()),
            'sub'   => trans('advance::advance.title.budget_manage_total', array('total' => $budget_total))
        );

        $data['budget_years'] = Transaction::select(DB::raw('YEAR(created_at) as year'))->lists('year', 'year');

        $this->layout->content = View::make("advance::admin.budget.manage",$data);
    }

}