<?php namespace Modules\Advance;

use NumberFormatter;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Config;

class AdvanceServiceHelpers {

    /**
     * Advance application ID generator
     *
     * @param $record
     * @return string
     */
    public function applicationIdGenerator($record){
        $pmn_id = $record->application_location;
        $payment_account = Account::find($pmn_id);
        $account_number = $payment_account->account_number + 1;

        try {
            DB::beginTransaction();

            /** Initials data */
            $prefix = Config::get('advance::advance.application.prefix');
            $center = array_slice(str_split($pmn_id), -3);
            $series = str_split($account_number, 1);

            /** Check digit formulae */
            $check = array_sum($prefix) + array_sum($center) + array_sum($series);
            $check = 10 - ($check % 10);

            /** Combine parts */
            $offer_id = implode($prefix) . implode($center) . implode($series) . $check;

            /** Save last account number and commit DB transaction */
            $payment_account->account_number = $account_number;
            $payment_account->save();
            DB::commit();

            /** Return offer id */
            return $offer_id;

        } catch(\Exception $e){
            DB::rollback();
        }

        return null;
    }


    /**
     * Spell currency based on ICU
     *
     * @param $number
     * @return string
     */
    public function spellCurrency($number){
        $nFormat = new NumberFormatter(Config::get('advance::advance.format.locale_icu'), NumberFormatter::SPELLOUT);

        return $nFormat->formatCurrency($number, Config::get('advance::advance.format.currency_prefix'));
    }


    /**
     * Spell numeric based on ICU
     *
     * @param $number
     * @return string
     */
    public function spellNumeric($number){
        $nFormat = new NumberFormatter(Config::get('advance::advance.format.locale_icu'), NumberFormatter::SPELLOUT);

        return $nFormat->format($number);
    }

} 