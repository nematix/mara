<?php return array(

    //
    // Specify routing for Rest & Rpc
    //

    'router' => array(
        'routes' => array(

            'advance_rpc_record_approve' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => 'advance/record',
                    'defaults' => array(
                        'controller' => 'Modules\\Advance\\Api\\V1\\Rpc\\Record\\RecordController',
                        'action' => 'approve',
                    ),
                ),
            ),

            'advance_rpc_record_payment' => array(
                'type' => 'Segment',
                'options' => array(
                    'route' => 'advance/record',
                    'defaults' => array(
                        'controller' => 'Modules\\Advance\\Api\\V1\\Rpc\\Record\\RecordController',
                        'action' => 'payment',
                    ),
                ),
            ),
        ),
    ),


    //
    // RPC mount point
    //

    'rpc' => array(

        /**
         * Record
         */
        'Modules\\Advance\\Api\\V1\\Rpc\\Record\\RecordController@approve' => array(
            'route_name' => 'advance_rpc_record_approve',
            'service_name' => 'Advance\Rpc\Record\Approve'
        ),

        'Modules\\Advance\\Api\\V1\\Rpc\\Record\\RecordController@payment' => array(
            'route_name' => 'advance_rpc_record_payment',
            'service_name' => 'Advance\Rpc\Record\Payment'
        ),
    ),


    //
    // REST mount point collection
    //

    'rest' => array(),
);
