<?php return [
    /*
    |--------------------------------------------------------------------------
    | Module Configurations
    |--------------------------------------------------------------------------
    |
    |
    */

    'application' => [
        'prefix'                    => [3,5],
        'amount_maximum'                => 2000,
        'amount_minimum'                => 1500,

        'salary_guardian_min'       => 0,
        'salary_guarantor_min'      => 500,

        'documents' => [
            'input'  => [
                'ic_student',
                'ic_guardian',
                'ic_guarantor',
                'cert_birth',
                'cert_birth_guardian',
                'statement_salary_guardian',
                'statement_salary_guarantor',
            ],
            'output' => [
                'offer_letter',
                'agreement_letter',
                'applicant_info',
                'payment_voucher'
            ]
        ],

        'maximum_age'   => 40
    ],

    'documents' => [
        'offer_letter'  => [
            'file_name'     => 'surat-tawaran-:account_number'
        ],
        'payment_voucher' => [
            'file_name'     => 'baucer-bayaran-:account_number'
        ],
        'applicant_info' => [
            'file_name'     => 'maklumat-pemohon-:account_number'
        ]
    ],

    'integration' => [
        'smbm' => [
            'host'          => '10.1.100.81',
            'port'          => '3000'
        ]
    ],


    'format' => [
        'locale_icu'                => 'ms',
        'currency_prefix'           => 'MYR'
    ],

    'filters' => [
        'category_bank' => [
            '559340',
            '559405',
            '559998',
            '559781',
            '559195',
            '559114',
            '559995',
            '559536',
            '559460',
            '559970',
            '559680'
        ]
    ]
];
