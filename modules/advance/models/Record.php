<?php

use Illuminate\Support\Facades\Config;
use Jenssegers\Date\Date;
use Atlantis\Detail\Model\Record as BaseRecord;


class Record extends BaseRecord {

    /** @var array Guard list */
    protected $guarded = array('uuid','message','user','guardian','guarantor','detail','url_update','transaction','application_location_name','created_when','updated_when');

    /** @var array Attribute append */
    protected $appends = array('url_update','application_location_name','created_when','updated_when');


    protected static function boot()
    {
        parent::boot();

        static::deleting(function($record){
            $record->conversation()->delete();
        });
    }


    public function user(){
        return $this->belongsTo('User');
    }


    public function guardian(){
        return $this->hasOne('People','id','guardian_id');
    }


    public function guarantor(){
        return $this->hasOne('People','id','guarantor_id');
    }


    public function transaction(){
        return $this->hasOne('Modules\Advance\Transaction','id','payment_id');
    }


    public function conversation(){
        return $this->hasMany('Conversation','subject');
    }
    /**
     * Status Attribute - Get
     *
     * @param $value
     * @return mixed
     */
    public function getStatusAttribute($value){
        $status = $this->detail->workflows('resource.status')[$value];

        #i: Save original value
        $status['raw'] = $value;

        return $status;
    }


    /**
     * Status Attribute - Set
     *
     * @param $value
     */
    public function setStatusAttribute($value){
        if( is_array($value) ){
            $this->attributes['status'] = $value['id'];
        }else{
            $this->attributes['status'] = $value;
        }
    }


    /**
     * Application Location Name Attribute - Get
     *
     * @return mixed
     */
    public function getApplicationLocationNameAttribute(){
        $center_id = $this->attributes['application_location'];

        $center = Code::category('pmn')->where('name',$center_id)->first();

        if($center){
            return $center->value;

        }else {
            return $center_id;
        }
    }


    /**
     * Url Update - Get
     *
     * @return string
     */
    public function getUrlUpdateAttribute(){
        $realm = app('Atlantis\View\Interfaces\Realm')->current();

        /** Return the update url */
        return route("advance.{$realm->name}.edit",array($this->uuid));
    }


    /**
     *
     * @return string
     */
    public function getPaymentProcessingStatusAttribute(){
        /** Default processing status : unavailable */
        $status = 'unavailable';

        /** Get transaction count */
        $transactions = $this->transaction()->where('transaction_type','advance:payment');

        if( $transactions->count() > 0 ){
            /** Get transaction */
            $transaction = $transactions->first();

            if( !empty($transaction->credit_account_id) ){
                /** Account number is generated : pending */
                $status = 'pending';

                if( isset($transaction->meta) ){
                    /** Account is exist : processing */
                    if( isset($transaction->meta->voucher_number) ) $status = 'processing';

                    /** Payment in sync, processing complete : complete */
                    if( isset($transaction->meta->sync) ){
                        if( $transaction->meta->sync == 1 ) $status = 'complete';
                    }
                }
            }
        }

        return $status;
    }


    /**
     * Override
     *
     * @return string
     */
    public function getCreatedWhenAttribute(){
        return Date::createFromTimeStamp(strtotime($this->created_at))->diffForHumans();
    }


    /**
     * Override
     *
     * @return string
     */
    public function getUpdatedWhenAttribute(){
        return Date::createFromTimeStamp(strtotime($this->updated_at))->diffForHumans();
    }


    /**
     * Scope Center
     *
     * @param $query
     * @param $center
     */
    public function scopeCenter($query,$center){
        $query->where('application_location',$center);
    }

}