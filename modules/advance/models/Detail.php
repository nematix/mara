<?php

use Atlantis\Detail\Model\Detail as BaseDetail;


class Detail extends BaseDetail {

    public function getCardsAttribute($value){
        if( empty($value) ) $value = '{}';
        return json_decode($value);
    }

    //@todo Should handled by modules
    public function getMenusAttribute(){
        return Config::get('admin::admin.sidebar.applications.'.$this->name);
    }

}
