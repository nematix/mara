<?php


class Album extends \Eloquent {
    protected $table = 'Albmas';
    protected $connection = 'mysql_album';
    protected $primaryKey = 'NOMBOR_GAJI';


    public function getSalaryIdAttribute(){
        return $this->attributes['NOMBOR_GAJI'];
    }
    public function setSalaryIdAttribute($value){
        $this->attributes['NOMBOR_GAJI'] = $value;
    }


    public function getNameAttribute(){
        return $this->attributes['NAMA'];
    }
    public function setNameAttribute($value){
        $this->attributes['NAMA'] = $value;
    }


    public function getSalaryGradeAttribute(){
        return $this->attributes['GRED_GAJI'];
    }
    public function setSalaryGradeAttribute($value){
        $this->attributes['GRED_GAJI'] = $value;
    }


    public function getIdnoIcAttribute(){
        return $this->attributes['NO_K/P'];
    }
    public function setIdnoIcAttribute($value){
        $this->attributes['NO_K/P'] = $value;
    }


    public function getCenterIdAttribute(){
        return $this->attributes['KOD_PUSAT'];
    }
    public function setCenterIdAttribute($value){
        $this->attributes['KOD_PUSAT'] = $value;
    }
}