<?php

namespace Modules\Advance;

use Illuminate\Support\Facades\DB;
use Atlantis\Core\Model\BaseModel;


class Transaction extends BaseModel{

    protected $table = 'advance_transactions';
    protected $fillable = ['account_id','transaction_type','debit','credit'];


    public function account(){
        return $this->hasOne('Modules\Advance\Transaction','id','account_id');
    }


    public function scopeState($query, $state){
        return $query->where('state_id',$state);
    }


    public function scopeAccountId($query, $account_id){
        return $query->where('account_id',$account_id);
    }


    public function scopeYear($query, $year){
        return $query
            ->whereRaw("YEAR(created_at) = $year");
    }


    public function scopeTransactionType($query,$type){
        return $query
            ->where('transaction_type',$type);
    }

}