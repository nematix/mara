<?php

namespace Modules\Advance;

use Atlantis\Core\Model\BaseModel;
use Code;

class Account extends BaseModel{

    protected $table = 'advance_accounts';
    protected $primaryKey = 'owner_id';
    protected $appends = ['state_name','year_budget_amount'];
    protected $fillable = ['state_id','category','account_type','account_number'];


    /**
     * Relation : transaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transaction(){
        return $this->hasMany('Modules\Advance\Transaction','account_id','id');
    }


    /**
     * State Name attribute
     *
     * @return mixed
     */
    public function getStateNameAttribute(){
        $state = Code::category('pmn')->where('name',$this->owner_id)->first();

        return $state->value;
    }


    /**
     * Current year budget amount
     *
     * @return mixed
     */
    public function getYearBudgetAmountAttribute(){
        $debit = $this->transaction()->year(date('Y'))->transactionType('advance:budget')->sum('debit');
        $credit = $this->transaction()->year(date('Y'))->transactionType('advance:payment')->sum('credit');
        return $debit - $credit;
    }


    /**
     * @param $transaction_type
     * @param $amount
     * @param null $from_account
     */
    public function debit($transaction_type, $amount, $from_account=null){
        $transaction = new Transaction();
        $transaction->transaction_type = $transaction_type;
        $transaction->debit = $amount;
        $transaction->debit_account_id = $from_account;

        $this->transaction()->save($transaction);
    }


    /**
     * @param $transaction_type
     * @param $amount
     * @param null $to_account
     */
    public function credit($transaction_type, $amount, $to_account=null){
        $transaction = new Transaction();
        $transaction->transaction_type = $transaction_type;
        $transaction->credit = $amount;
        $transaction->credit_account_id = $to_account;

        $this->transaction()->save($transaction);
    }
}