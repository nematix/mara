<?php return array(
    'title' => array(
        'info'          => 'Maklumat Peribadi',
        "profile"       => "Profil Peribadi",
        'application'   => 'Maklumat Permohonan',
        'course'        => 'Maklumat Pengajian',
        'guardian'      => 'Maklumat Penjaga',
        'guarantor'     => 'Maklumat Penjamin',
        'document'      => 'Dokumen',
        'final'         => 'Pengesahan',
    ),

    'section' => array(
        'personal' => array(
            'first_name'            => 'Nama',
            'last_name'             => 'Nama Bapa',
            'idno_ic'               => 'Nombor Kad Pengenalan',
            'gender-male'           => 'Lelaki',
            'gender-female'         => 'Perempuan',
            'race'                  => 'Keturunan',
            'birth_date'            => 'Tarikh Lahir'
        ),
        'application' => array(
            'amount_total'          => 'Jumlah Dipohon (RM)',
            'account_no'            => 'No. Akaun Bank',
            'bank_code'             => 'Nama Bank',
            'existing'              => 'Pernah Mendapat Pendahuluan Pinjaman MARA ?',
        ),
        'course' => array(
            'application_coursed'   => 'Kursus Yang Di Tawarkan',
            'institution_name'      => 'Nama Institusi',
            'institution_state'     => 'Negeri Institusi',
            'course_level'          => 'Peringkat Pengajian',
            'course_start'          => 'Tarikh Mula Kursus',
            'course_end'            => 'Tarikh Tamat Kursus'
        ),
        'guardian' => array(
            'idno_ic'               => 'No. K/P Penjaga',
            'find_people'           => 'Sila masukkan No. K/P penjaga untuk memeriksa maklumat sedia-ada',
            'first_name'            => 'Nama Penjaga',
            'last_name'             => 'Nama Bapa Penjaga',
            'address_street'        => 'No. Rumah / Jalan',
            'address_district'      => 'Daerah',
            'address_city'          => 'Bandar',
            'address_state'         => 'Negeri',
            'address_postcode'      => 'Poskod',
            'contact_home'          => 'No. Telefon Rumah',
            'contact_mobile'        => 'No. Telefon Bimbit',
            'guardian_salary_gross' => 'Pendapatan Kasar Bulanan',
            'guardian_family_no'    => 'Bilangan Tanggungan',
            'guardian_employment'   => 'Pekerjaan Penjaga'

        ),
        'guarantor' => array(
            'idno_ic'                       => 'No. K/P Penjamin',
            'find_people'                   => 'Sila masukkan No. K/P penjamin untuk memeriksa maklumat sedia-ada',
            'first_name'                    => 'Nama Penjamin',
            'last_name'                     => 'Nama Bapa Penjamin',
            'address_street'                => 'No. Rumah / Jalan',
            'address_district'              => 'Daerah',
            'address_city'                  => 'Bandar',
            'address_state'                 => 'Negeri',
            'address_postcode'              => 'Poskod',
            'contact_home'                  => 'No. Telefon Rumah',
            'contact_mobile'                => 'No. Telefon Bimbit',
            'guarantor_salary_gross'        => 'Pendapatan Kasar Bulanan',
            'guarantor_status_parent'       => 'Ibu\Bapa',
            'guarantor_status_not_parent'   => 'Bukan Ibu\Bapa'

        ),
        'document' => array(
            'description'                   => 'Dokumen yang diperlukan',
            'button_clear'                  => 'Padam semua dokumen',
            'preview'                       => 'Paparan Dokumen',
            'allowed_file_types'            => 'Berikut adalah jenis fail yang di benarkan untuk di muat-naik: <strong>png, jpg & tiff</strong>'
        ),
        'final' => array(
            'terms_title'                   => 'Syarat & Terma',
            'terms_body'                    => "Saya mengaku bahawa semua keterangan yang diberikan di atas adalah benar dan pihak MARA berhak menolak permohonan sekiranya didapati borang ini tidak lengkap atau mana-mana keterangan di atas tidak benar atau diragui.\n".
                                                'Makluman : Permohonan anda masih boleh diubah selagi pihak Pegawai MARA Negeri belum mengesahkan penerimaan dokumen sokongan daripada pihak anda. Setelah dokumen diterima, permohonan ini tidak boleh diubah dan dianggap lengkap.',
            'checkbox_final'                => 'Saya menerima Syarat & Terma yang di tetapkan.'
        )
    )
);