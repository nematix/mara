<?php return array(

    'manage' => array(
        'staff'                 => 'Pengurusan Pegawai',
        'budget'                => 'Pengurusan Bajet',
    ),

    'student' => array(
        'new'                   => 'Permohonan Baru',
        'manage'                => 'Senarai Permohonan',
    ),

    'staff' => array(
        'browse'                => 'Carian Permohonan',
        'statistic'             => 'Statistik Permohonan',
        'manage' => array(
            'review'            => 'Semakan Permohonan',
            'approve'           => 'Kelulusan Permohonan',
            'payment'           => 'Pembayaran Permohonan',
        )
    ),

    'title' => array(
        'application_new'       => 'Permohonan Baru',
        'application_list'      => 'Senarai Permohonan',
        'application_browse'    => 'Carian Senarai Permohonan',
        'application_clear'     => 'Padam Semua Permohonan',
        'application_date'      => 'Tarikh Memohon',
        'application_approval'  => 'Kelulusan',
        'application_approval_amount'   => 'Jumlah di Luluskan',
        'application_statistic' => 'Statistik Permohonan',
        'application_status'    => 'Status',
        'application_location'  => 'Tempat Memohon',
        'application_payment'   => 'Pembayaran',
        'payment_voucher'       => 'Baucer Bayaran',

        'detail_form'           => 'Borang Permohonan',
        'student_name'          => 'Nama Pelajar',
        'student_manage'        => 'Semakan Pelajar',
        'staff_manage_total'    => 'Jumlah Pegawai Keseluruhan : :total',
        'staff_manage_roles'    => 'Peranan Pegawai',
        'staff_salary_id'       => 'No Gaji',
        'staff_center_id'       => 'No Pusat',

        'budget_manage_total'   => 'Jumlah Keseluruhan Bajet : :total',
        'budget_manage_amount'  => 'Jumlah Peruntukan (RM)',
        'budget_manage_accountno'   => 'Nombor Akaun (Bersiri)',
        'budget_manage_overall' => 'Bajet Keseluruhan',

        'page_help'             => 'Bantuan Akaun Pengguna',
        'page_helpdesk'         => 'Maklumat Meja Bantuan',

        'user_password_forget'  => 'Lupa katalaluan',
        'user_resend_activation'=> 'Hantar kembali email pengaktifan akaun',
        'user_manual_activation'=> 'Pengaktifan akaun secara manual',
        'user_new_registration' => 'Pendaftaran baru akaun pelajar',

        'information'           => 'Maklumat',
        'total'                 => 'Jumlah',
        'action'                => 'Tindakan',

        'message_received'      => 'Makluman dari pentadbiran',
        'document_uploaded'     => 'Dokumen yang di upload',
        'document_approved'     => 'Dokumen Tawaran (Lulus)',

        'comment'               => "Komen"
    ),

    'text' => array(
        'application_total'         => 'Terdapat :total permohonan pendahuluan',
        'application_max'           => 'Permohonan telah wujud! Hanya :total permohonan dibenarkan bagi setiap pengguna',
        'application_clear_prompt'  => 'Padam semua permohonan?',
        'application_eligible'      => 'Permohonan di buat pada :date dan disahkan layak oleh sistem.',
        'user_profile_not_complete' => 'Profil pemohon tidak lengkap, sila lengkapkan pada <a href=":url">pautan</a> ini.',
        'application_update_reason' => 'Permohonan anda :status, Maklumat: :reason',
        'registration_failed_age'   => 'Permohonan anda ditolak kerana melepasi had kelayakan umur.'
    ),

    'status' => array(
        'staff' => array(
            'new'           => 'Baru',
            'eligible'      => 'Layak',
            'not_eligible'  => 'Tidak Layak',
            'accept'        => 'Terima',
            'support'       => 'Sokong',
            'not_support'   => 'Tidak Sokong',
            'consideration' => 'Dalam Pertimbangan',
            'pass'          => 'Lulus',
            'not_pass'      => 'Tidak Lulus',
            'payment'       => 'Bayar',
            'complete'      => 'Lengkap'
        ),
        'student' => array(
            'in_process'    => 'Sedang di Proses',
            'pass'          => 'Lulus',
            'not_pass'      => 'Tidak Lulus'
        ),
        'payment' => array(
            'unavailable'   => 'Tiada maklumat pembayaran',
            'pending'       => 'Pembayaran belum di proses',
            'processing'    => 'Pembayaran sedang di proses',
            'ready'         => 'Pembayaran telah di proses',
            'complete'      => 'Pembayaran lengkap'
        )
    ),

    'documents' => array(
        'offer_letter'                  => 'Surat Tawaran',
        'agreement_letter'              => 'Surat Perjanjian',
        'applicant_info'                => 'Maklumat Pemohon',
        'ic_student'                    => 'Kad Pengenalan Pemohon',
        'ic_guardian'                   => 'Kad Pengenalan Penjaga',
        'ic_guarantor'                  => 'Kad Pengenalan Penjamin',
        'cert_birth'                    => 'Sijil Kelahiran Pemohon',
        'cert_birth_guardian'           => 'Sijil Kelahiran Penjaga',
        'statement_salary_guardian'     => 'Penyata Gaji / Borang B1 atau Surat Pengesahan Pendapatan Penjaga',
        'statement_salary_guarantor'    => 'Penyata Gaji / Borang B1 atau Surat Pengesahan Pendapatan Penjamin',
    ),

    'blacklist' => array(
        'modal-title'   => 'Senarai Hitam',
        'modal-body'    => 'Pemilik kad pengenalan :idno_ic telah disenarai hitam, sila hubungi pihak Mara untuk keterangan lanjut'
    ),

);
