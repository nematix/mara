<?php return array(
    'account_series'    => 'Nombor siri akaun tidak boleh kurang dan sama dengan nombor semasa.',

    'attributes' => array(
        'name'                  => 'Nama',
        'email'                 => 'Email',
        'full_name'             => 'Nama Penuh',
        'first_name'            => 'Nama',
        'last_name'             => 'Nama Bapa',
        'password'              => 'Katalaluan',

        'idno_ic'               => 'No. K/P (Baru)',
        'race'                  => 'Keturunan',
        'gender'                => 'Jantina',
        'gender_male'           => 'Lelaki',
        'gender_female'         => 'Perempuan',
        'birth'                 => 'Tarikh & Tempat Lahir',
        'birth_date'            => 'Tarikh Lahir',

        'address_street'        => 'No. Rumah / Jalan',
        'address_area'          => "Taman/Kawasan",
        'address_district'      => 'Daerah',
        'address_city'          => 'Bandar',
        'address_state'         => 'Negeri',
        'address_postcode'      => 'Poskod',
        'address_citystate'     => 'Bandar & Negeri',
        'contact_home'          => 'No. Telefon Rumah',
        'contact_mobile'        => 'No. Telefon Bimbit',

        'application_location'  => 'Tempat Memohon',
        'amount_total'          => 'Jumlah Dipohon (RM)',
        'account_no'            => 'No. Akaun Bank',
        'bank_code'             => 'Nama Bank',
        'existing'              => 'Pernah Mendapat Pendahuluan Pinjaman MARA',

        'application_coursed'   => 'Kursus Yang Di Tawarkan',
        'institution_name'      => 'Nama Institusi',
        'institution_state'     => 'Negeri Institusi',
        'course_level'          => 'Peringkat Pengajian',
        'course_start'          => 'Tarikh Mula Kursus',
        'course_end'            => 'Tarikh Tamat Kursus',

        'guardian_name'         => 'Nama Penjaga',
        'guardian_id'           => 'No. K/P Penjaga',

        'guarantor_id'          => 'No. K/P Penjamin',
        'guarantor_name'        => 'Nama Penjamin',
        'guarantor_status'      => 'Status Penjamin',
        'guarantor_salary'      => 'Pendapatan Penjamin',
        'status'                => 'Status',

        'voucher_number'        => 'Nombor Baucer',
        'cheque_number'         => "Nombor Cheque",
        'cheque_date'           => "Tarikh Cheque"
    )

);