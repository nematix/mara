<?php return array(
    'staff' => array(
        'title' => array(
            'login'             => 'Log Masuk Pegawai',
            'header'            => 'Daftar Penguna Pegawai',
            'salary_id'         => 'Nombor Gaji',
            'username_alt'      => 'Email \ Nombor Gaji',
        ),

        'text' => array(
            'registered'        => 'Pegawai telah didaftarkan',
            'not_exist'         => 'Nombor gaji tidak dapat disahkan',
            'salary_id_verify'  => 'Sila masukkan nombor gaji untuk mengesahkan akaun anda',
        )
    ),

    'student' => array(
        'title' => array(
            'login'             => 'Log Masuk Pelajar',
            'header'            => 'Pendaftaran Pelajar',
            'register'          => 'Daftar Pelajar',
        ),

        'text' => array(
            'registered'        => 'Pelajar telah di daftarkan, sila klik link <a href=":href">disini</a> untuk bantuan akaun pengguna',
        )
    )
);