<?php return array(

    'manage' => array(
        'staff'                 => 'Staff Management',
        'budget'                => 'Budget Management',
    ),

    'student' => array(
        'new'                   => 'New Application',
        'manage'                => 'Application List',
    ),

    'staff' => array(
        'browse'                => 'Browse Application',
        'statistic'             => 'Application Statistic',
        'manage' => array(
            'review'            => 'Application Review',
            'approve'           => 'Application Approval',
            'payment'           => 'Application Payment',
        )
    ),

    'title' => array(
        'application_new'       => 'New Application',
        'application_list'      => 'List of Application',
        'application_browse'    => 'Application Browse',
        'application_clear'     => 'Delete All Application',
        'application_date'      => 'Application Date',
        'application_approval'  => 'Approval',
        'application_approval_amount'   => 'Approved Amount',
        'application_statistic' => 'Application Statistic',
        'application_status'    => 'Status',
        'application_location'  => 'Location to Apply',
        'application_payment'   => 'Payment',
        'payment_voucher'       => 'Payment Voucher',

        'detail_form'           => 'Application Form',
        'student_manage'        => 'Student\'s Review',
        'student_name'          => 'Student\'s Name',
        'staff_manage_total'    => 'Total Staff',
        'staff_manage_roles'    => 'Staff\'s role',
        'staff_salary_id'       => 'Salary No.',
        'staff_center_id'       => 'Center No.',
        'budget_manage_total'   => 'Total Budget : :total',
        'budget_manage_amount'  => 'Total Allocation (RM)',
        'budget_manage_accountno'   => 'Account Number (Serial)',
        'budget_manage_overall' => 'Overall Budget',

        'page_help'             => 'User Account Support',
        'page_helpdesk'         => 'Helpdesk Information',

        'user_password_forget'  => 'Forget password',
        'user_resend_activation'=> 'Resend account activation email',
        'user_manual_activation'=> 'Manual account activation',
        'user_new_registration' => 'New student account registration',

        'information'           => 'Information',
        'total'                 => 'Total',
        'action'                => 'Action',

        'message_received'      => 'Notification from Administrator',
        'document_uploaded'     => 'Documents uploaded',
        'document_approved'     => 'Approval Documents',

        'comment'               => "Comment"
    ),

    'text' => array(
        'application_total'         => 'There are :total advance application',
        'application_max'           => 'Application already exists! Only :total application is allowed per user',
        'application_clear_prompt'  => 'Delete all application?',
        'application_eligible'      => 'Application was made on :date and verified qualify by the system.',
        'user_profile_not_complete' => 'The applicant\'s profile does not complete, please complete this section <a href=":url">link</a>.',
        'application_update_reason' => 'You are :status, Info: :reason',
        'registration_failed_age'   => 'Registration failed because you age exceed the age eligility'
    ),

    'status' => array(
        'staff' => array(
            'new'           => 'New',
            'eligible'      => 'Qualify',
            'not_eligible'  => 'Not Qualify',
            'accept'        => 'Accepted',
            'support'       => 'Supported',
            'not_support'   => 'Not Supported',
            'consideration' => 'In Consideration',
            'pass'          => 'Approved',
            'not_pass'      => 'Not Approved',
            'payment'       => 'Pay',
            'complete'      => 'Complete'
        ),
        'student' => array(
            'in_process'    => 'Application is being processed',
            'pass'          => 'Approved',
            'not_pass'      => 'Not Approved'
        ),
        'payment' => array(
            'unavailable'   => 'No payment information',
            'pending'       => 'Payment pending proces',
            'processing'    => 'Payment in process',
            'ready'         => 'Payment process ready',
            'complete'      => 'Payment complete'
        )
    ),

    'documents' => array(
        'offer_letter'                  => 'Offer Letter',
        'agreement_letter'              => 'Agreement Letter',
        'applicant_info'                => 'Applicant Info',
        'ic_student'                    => 'Applicant\'s I/C No.',
        'ic_guardian'                   => 'Guardian\'s I/C No.',
        'ic_guarantor'                  => 'Guarantor\'s I/C No.',
        'cert_birth'                    => 'Applicant\'s Birth Certification',
        'cert_birth_guardian'           => 'Guardian\'s Birth Certification',
        'statement_salary_guardian'     => 'Guardian\'s Payslip / Form B1 or Income Verification Letter ',
        'statement_salary_guarantor'    => 'Guarantor\'s Payslip / Form B1 or Income Verification Letter',
    ),

    'blacklist' => array(
        'modal-title'   => 'Blacklist',
        'modal-body'    => 'I/C holder :idno_ic has been blacklisted, please contact Mara for more information'
    ),

);
