<?php return array(
    'account_series'    => 'Account series number cannot be smaller than current value.',

    'attributes' => array(
        'name'                  => "Name",
        'email'                 => "Email",
        'full_name'             => "Full Name",
        'first_name'            => "First Name",
        'last_name'             => "Last Name",
        'password'              => "Password",

        'idno_ic'               => "I/C No. (New)",
        'race'                  => "Race",
        'gender'                => "Gender",
        'gender_male'           => "Male",
        'gender_female'         => "Female",
        'birth'                 => "Birth Place",
        'birth_date'            => "Birth Date",

        'address_street'        => "House No. / Street",
        'address_area'          => "Area",
        'address_district'      => "District",
        'address_city'          => "City",
        'address_state'         => "State",
        'address_postcode'      => "Postcode",
        'address_citystate'     => "City & State",
        'contact_home'          => "Home Phone No.",
        'contact_mobile'        => "Mobile Phone No.",

        'application_location'  => "Location of Application",
        'amount_total'          => "Amount Applied (RM)",
        'account_no'            => "Account Bank No.",
        'bank_code'             => "Bank Name",
        'existing'              => "Had received MARA Loan Advance",

        'application_coursed'   => "Course",
        'institution_name'      => "Institution Name",
        'institution_state'     => "Institution's State",
        'course_level'          => "Education Level Level",
        'course_start'          => "Course Start Date",
        'course_end'            => "Course End Date",

        'guardian_name'         => "Guardian's Name",
        'guardian_id'           => "Guardian I/C No.",

        'guarantor_id'          => "Guarantor I/C No.",
        'guarantor_name'        => "Guarantor's Name",
        'guarantor_status'      => "Guarantor Status",
        'guarantor_salary'      => "Guarantor's Salary",
        'status'                => "Status",

        'voucher_number'        => 'Voucher Number',
        'cheque_number'         => "Cheque Number",
        'cheque_date'           => "Cheque Date"
    )

);
