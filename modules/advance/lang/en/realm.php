<?php return array(
    'staff' => array(
        'title' => array(
            'login'             => 'Staff Login',
            'header'            => 'Staff Register',
            'salary_id'         => 'Salary No.',
            'username_alt'      => 'Email \ Salary No.',
        ),

        'text' => array(
            'registered'        => 'Staff had been registered',
            'not_exist'         => 'Salary No. could not be verified',
            'salary_id_verify'  => 'Please enter Salary No. to verify your account',
        )
    ),

    'student' => array(
        'title' => array(
            'login'             => 'Student Login',
            'header'            => 'Student Registration',
            'register'          => 'Student Register',
        ),

        'text' => array(
            'registered'        => 'Student has already registered, please click the link <a href=":href">here</a> to aid user account',
        )
    )
);