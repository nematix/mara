<?php return array(
    "title" => array(
        "info"          => "Personal Information",
        "profile"       => "Personal Profile",
        "application"   => "Application Information",
        "course"        => "Course",
        "guardian"      => "Guardian Information",
        "guarantor"     => "Guarantor Information",
        "document"      => "Document",
        "final"         => "Confirmation",
    ),

    "section" => array(
        "personal" => array(
            "first_name"            => "Name",
            "last_name"             => "Father's Name",
            "idno_ic"               => "I/C No.",
            "gender-male"           => "Male",
            "gender-female"         => "Female",
            "race"                  => "Race",
            "birth_date"            => "Birth Date"
        ),
        "application" => array(
            "amount_total"          => "Amount Applied (RM)",
            "account_no"            => "Account No.",
            "bank_code"             => "Bank's Name",
            "existing"              => "Have you ever received MARA Loan before?",
        ),
        "course" => array(
            "application_coursed"   => "Course",
            "institution_name"      => "Institution",
            "institution_state"     => "Institution's State",
            "course_level"          => "Level",
            "course_start"          => "Course Start Date",
            "course_end"            => "Course End Date"
        ),
        "guardian" => array(
            "idno_ic"               => "Guardian I/C No.",
            "find_people"           => "Please enter Guardian's I/C No. to examine the pre-existing information",
            "first_name"            => "Guardian's Name",
            "last_name"             => "Name of the Father's Guardian",
            "address_street"        => "House No. / Street",
            "address_district"      => "District",
            "address_city"          => "City",
            "address_state"         => "State",
            "address_postcode"      => "Postcode",
            "contact_home"          => "Home Phone No.",
            "contact_mobile"        => "Mobile Phone No.",
            "guardian_salary_gross" => "Monthly Gross Income",
            "guardian_family_no"    => "Total Liabilities",
            "guardian_employment"   => "Guardian's Occupation"

        ),
        "guarantor" => array(
            "idno_ic"                       => "Guarantor's I/C No.",
            "find_people"                   => "Please enter guardian's I/C No. K/P to examine the pre-existing information",
            "first_name"                    => "First Name",
            "last_name"                     => "Last Name",
            "address_street"                => "House No. / Street",
            "address_district"              => "District",
            "address_city"                  => "City",
            "address_state"                 => "State",
            "address_postcode"              => "Postcode",
            "contact_home"                  => "Home Phone No.",
            "contact_mobile"                => "Mobile Phone No.",
            "guarantor_salary_gross"        => "Monthly Gross Income",
            "guarantor_status_parent"       => "Mother\\Father",
            "guarantor_status_not_parent"   => "Not Mother\\Father"

        ),
        "document" => array(
            "description"                   => "Required Documents",
            "button_clear"                  => "Delete All Document",
            'preview'                       => 'Documents Preview',
            'allowed_file_types'            => 'Following is allowed file type for upload: <strong>png, jpg & tiff</strong>'
        ),
        "final" => array(
            "terms_title"                   => "Term & Condition",
            "terms_body"                    => "I declare that all information given above is true and Mara reserve the right to reject the application if the form is not complete or any information above is incorrect or dubious.\n".
                                                "Note: Your application can still be changed as long as the MARA Official State has not confirmed receiving your supporting documents. Once the documents are received, the application can not be changed and is considered as complete.",
            "checkbox_final"                => "I accept the Terms & Conditions."
        )
    )
);
