<?php

namespace Modules\Advance\Api\V1\Rpc\Record;

use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Response;
use Atlantis\Api\Controller\BaseController;
use Record, Modules\Advance\Account, Modules\Advance\Transaction;
use GuzzleHttp\Client;


class RecordController extends BaseController{


    /**
     * Approval
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function methodApprove(){
        $post = Input::all();
        $params = $post['params'];
        $this->result['id'] = $post['id'];

        try{
            DB::beginTransaction();
            $record = Record::find($params['uuid']);

            /** Check for support status */
            if( $record->status['id'] != 4 ) throw new Exception('This application is not yet supported!');

            /** Generate account number, check for duplicate */
            do {
                $account_number = app('atlantis.helpers')->advance->applicationIdGenerator($record);

                /** Check account string length */
                if( strlen($account_number) > 12 ){
                    throw new Exception('Fatal! Account size generated exceeded maximum allowed!');
                }

            } while( Transaction::where('credit_account_id',$account_number)->count() > 0 );

            /** Payment transaction */
            $payment_account = Account::find($record->application_location);
            $payment_account->credit('advance:payment',$params['amount_approved'],$account_number);

            /** Status, link payment to record */
            $record->status = 7;
            $record->payment_id = Transaction::where('credit_account_id',$account_number)->first()->id;
            $record->save();

            DB::commit();

            $this->result['result'] = 'Application approval success!';

        } catch(Exception $e){
            /** Cancel db transaction */
            DB::rollback();

            /** Response error */
            $this->result['error'] = $e->getMessage();
        }

        return Response::json($this->result);
    }


    /**
     * Payment Processing
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function methodPayment(){
        $post = Input::all();
        $params = $post['params'];
        $this->result['id'] = $post['id'];

        try{
            /** Prepare meta data of transaction */
            $params['sync'] = 0;

            /** Save payment information */
            $record = Record::find($params['uuid']);
            $payment_transaction = $record->transaction()->first();
            $payment_transaction->meta = $params;
            $payment_transaction->save();
            $record->push($payment_transaction);

            /** Send payment processing to smbm */
            Queue::push(function($job) use($params){
                try {
                    $record = Record::find($params['uuid']);
                    $payment_transaction = $record->transaction()->first();
                    $payment_meta = (Array) $payment_transaction->meta;

                    /** SMBM API Server config */
                    $host = Config::get('advance::advance.integration.smbm.host');
                    $port = Config::get('advance::advance.integration.smbm.port');
                    $uri = "http://{$host}:{$port}/api/advance/payments";

                    /** @var Client $client Server client instance */
                    $client = new Client();

                    /** Data preparation */
                    $data = [
                        'account_number' => $payment_transaction->credit_account_id,
                        'student_name' => $record->user->full_name,
                        'course_level' => $record->course_level,
                        'course_start_date' => $record->course_start,
                        'idno_ic' => $record->user->profile->idno_ic,
                        'birth_date' => $record->user->profile->birth_date,
                        'gender' => $record->user->profile->gender,
                        'race' => $record->user->profile->race
                    ];

                    /** Start batch transaction */
                    DB::beginTransaction();

                    /** Check for payment transaction exist */
                    $response = $client->get("{$uri}/{$payment_transaction->credit_account_id}");
                    $payment = $response->json();

                    if( !empty($payment) ){
                        //$json = $response->json();
                        //$payment_meta['voucher_number'] = $json[''];
                        //$payment_meta['voucher_cheque'] = $json[''];
                        $payment_meta['sync'] = 1;
                        $payment_meta['attempt'] = 'Payment information exist';

                    } else {
                        /** Push payment to SMBMAPI */
                        $response = $client->post($uri,array('body' => $data));

                        /** Check for status */
                        if($response->getStatusCode() != 201){
                            throw new Exception('Payment processing to SMBM failed!');

                        } else {
                            $payment_meta['sync'] = 1;
                        }
                    }

                    /** Save new meta info */
                    $payment_transaction->meta = $payment_meta;
                    $payment_transaction->save();
                    $record->push($payment_transaction);

                    /** Commit DB transaction and delete queue job */
                    DB::commit();

                    /** Delete queue job */
                    $job->delete();


                } catch(Exception $e){
                    /** Rollback any transaction */
                    DB::rollback();

                    /** Log error */
                    Log::error("[SMBM Push error] ".$e->getTraceAsString());
                }
            });

            /** Response */
            $this->result['result'] = 'Application is successfully send for processing';

        }catch (Exception $e){
            $this->result['error'] = $e->getMessage();
        }

        return Response::json($this->result);
    }
} 
