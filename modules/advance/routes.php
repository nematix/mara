<?php

use Illuminate\Support\Facades\Route;


Route::group(['before'=>'auth.sentry'], function(){
    Route::controller('advance/manage','\Modules\Advance\AdvanceManageController',array(
        'getStaff'    => 'advance.manage.staff',
        'getBudget'   => 'advance.manage.budget'
    ));

    Route::controller('advance/student','\Modules\Advance\AdvanceStudentController',array(
        'getManage' => 'advance.student.manage',
        'getNew'    => 'advance.student.new',
        'getEdit'   => 'advance.student.edit'
    ));

    Route::controller('advance/staff','\Modules\Advance\AdvanceStaffController',array(
        'getBrowse'         => 'advance.staff.browse',
        'getManage'         => 'advance.staff.manage',
        'getManageReview'   => 'advance.staff.manage.review',
        'getManageApprove'  => 'advance.staff.manage.approve',
        'getManagePayment'  => 'advance.staff.manage.payment',
        'getEdit'           => 'advance.staff.edit',
        'getPayment'        => 'advance.staff.payment',
        'getStatistic'      => 'advance.staff.statistic'
    ));

    Route::controller('advance','\Modules\Advance\AdvanceController',array(
        'getIndex'  => 'advance'
    ));

    Route::group(array('before'=>'document.auth'), function(){
        Route::controller('documents','\Modules\Advance\AdvanceDocumentController');
    });
});


/**
 * API
 */
Route::group(array('prefix'=>'api/v1'), function(){
    /** Advance API */
    Route::resource('advance/account','\\Api\\V1\\Advance\\AccountController');
    Route::resource('advance/accounts.budgets','\\Api\\V1\\Advance\\AccountBudgetController');
    Route::resource('advance/staff','\\Api\\V1\Advance\\StaffController');

    /** Details API */
    Route::post('details/clear','\\Api\\V1\\DetailController@clear');
    Route::resource('details','\\Api\\V1\\DetailController');
    Route::resource('details/status','\\Api\\V1\\Detail\\StatusController');

    /** */
    Route::post('documents/clear','\\Api\\V1\\DocumentController@clear');
    Route::resource('documents','\\Api\\V1\\DocumentController');

    /** Peoples API */
    Route::get('peoples/search/{term?}', '\Api\V1\PeopleController@search');
    Route::resource('peoples','\Api\V1\PeopleController');
});