<!DOCTYPE html>

<html lang="en-US">
<head>
    <meta charset="utf-8">
    <style>
        body {
            font-size:9pt;
            font-family:Calibri,sans-serif;
        }

        h1 {
            font-size:10pt;
            margin:3pt 0 .0001pt 7pt;
        }

        p{
            margin:0 0 .0001pt;
        }

        .section {
            size:595pt 842pt;
            margin:12pt 20pt;
        }

        table {
            width:100.0%;
            border-collapse:collapse;
        }

        tr.header td {
            background: #D9D9D9;
            width: 100.0%;
            height: 23px;
            font-weight: bold;
        }

        td {
            padding:0 5px;
            height: 22px;
        }

        td.left {
            width:26%;
        }

        td.right {
            width:74%;
        }
    </style>

    <title></title>
</head>
<body>

    <div class="section">
        <p style='text-align: right'>BIL. MARA : {{$record->transaction->credit_account_id}}</p>
        <h3>MAJLIS AMANAH RAKYAT (MARA)</h3>
        <h3>MAKLUMAT PEMOHON PENDAHULUAN PINJAMAN PELAJARAN</h3>
    </div>

    <div class="section">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr class="header">
                <td colspan="4">Maklumat Pemohon</td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.name') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper($record->user->full_name) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.idno_ic') }}</p></td>
                <td colspan="3" class="right"><p>{{ $record->user->profile->idno_ic }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.gender') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper(trans('advance::cards.section.personal.gender-'.$record->user->profile->gender)) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.email') }}</p></td>
                <td colspan="3" class="right"><p>{{ $record->user->email }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.race') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper(Code::category('native')->where('name',$record->user->profile->race)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.birth_date') }}</p></td>
                <td colspan="3" class="right"><p>{{ Carbon\Carbon::parse($record->user->profile->birth_date)->format('d M Y') }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.course_start') }}</p></td>
                <td style='width: auto;'>
                    <p>{{ Carbon\Carbon::parse($record->course_start)->format('d M Y')  }}</p></td>
                <td class="left">
                    <p>{{ trans('advance::validation.attributes.course_end') }}</p></td>
                <td style='width: auto;'>
                    <p>{{ Carbon\Carbon::parse($record->course_end)->format('d M Y')  }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.amount_total') }}</p></td>
                <td colspan="3" class="right"><p>{{ $record->amount_total }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.application_location') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper(Code::category('pmn')->where('name',$record->application_location)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.account_no') }}</p></td>
                <td colspan="3" class="right"><p>{{ $record->user->profile->data->account_bank_no }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.bank_code') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper(Code::category('bank')->where('name',$record->user->profile->data->account_bank_code)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.application_coursed') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper($record->application_coursed) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.institution_name') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper($record->institution_name) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.course_level') }}</p></td>
                <td colspan="3" class="right"><p>{{ strtoupper(Code::category('course-level')->where('name',$record->course_level)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.existing') }}</p></td>
                <td colspan="3" class="right"><p>{{ $record->application_existing == 0 ? 'TIDAK' : 'PERNAH' }}</p></td>
            </tr>
        </table>
    </div>

    <div class="section">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr class="header">
                <td colspan="2">Maklumat Penjaga</td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.guardian_name') }}</p></td>
                <td class="right"><p>{{ strtoupper($record->guardian->full_name) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.idno_ic') }}</p></td>
                <td class="right"><p>{{ $record->guardian->idno_ic }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.address_street') }}</p></td>
                <td class="right"><p>{{ strtoupper($record->guardian->address_street) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.address_postcode') }}</p></td>
                <td class="right"><p>{{ $record->guardian->address_postcode }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.address_state') }}</p></td>
                <td class="right"><p>{{ strtoupper(Code::category('state')->where('name',$record->guardian->address_state)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.contact_home') }}</p></td>
                <td class="right"><p>{{ $record->guardian->contact_home }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.contact_mobile') }}</p></td>
                <td class="right"><p>{{ $record->guardian->contact_mobile }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>Pendapatan Bapa</p></td>
                <td class="right"><p>{{ $record->guardian_salary_gross }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>Pekerjaan</p></td>
                <td class="right"><p>{{ strtoupper(Code::category('class')->where('name',$record->guardian_employment)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>Bil. Tanggungan</p></td>
                <td class="right"><p>{{ $record->guardian_family_no }}</p></td>
            </tr>
        </table>
    </div>

    <div class="section">
        <table border="0" cellpadding="0" cellspacing="0">
            <tr class="header">
                <td colspan="2">Maklumat Penjamin</td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.guarantor_name') }}</p></td>
                <td class="right"><p>{{ strtoupper($record->guarantor->full_name) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.idno_ic') }}</p></td>
                <td class="right"><p>{{ $record->guarantor->idno_ic }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.address_street') }}</p></td>
                <td class="right"><p>{{ strtoupper($record->guarantor->address_street) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.address_postcode') }}</p></td>
                <td class="right"><p>{{ $record->guarantor->address_postcode }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.address_state') }}</p></td>
                <td class="right"><p>{{ strtoupper(Code::category('state')->where('name',$record->guarantor->address_state)->first()->value) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.contact_home') }}</p></td>
                <td class="right"><p>{{ $record->guarantor->contact_home }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.contact_mobile') }}</p></td>
                <td class="right"><p>{{ $record->guarantor->contact_mobile }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.guarantor_status') }}</p></td>
                <td class="right"><p>{{ strtoupper($record->guarantor_status == 1 ? trans('advance::cards.section.guarantor.guarantor_status_parent') : trans('advance::cards.section.guarantor.guarantor_status_not_parent')) }}</p></td>
            </tr>

            <tr>
                <td class="left"><p>{{ trans('advance::validation.attributes.guarantor_salary') }}</p></td>
                <td class="right"><p>{{ $record->guarantor_salary_gross }}</p></td>
            </tr>
        </table>
    </div>
</body>
</html>