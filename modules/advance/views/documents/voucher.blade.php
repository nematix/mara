<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Baucar Bayaran</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
        body { font-family:Arial, Helvetica, sans-serif; font-size:10pt; }
        h1 { page-break-before: always; }
        table { border-collapse: collapse; }
    </style>

</head>
<body>

<div align="center">
    <table width="600px" border="0">
        <tr>
            <td width="450px">
                <strong>MAJLIS AMANAH RAKYAT (MARA)</strong><br />
                {{ Code::category('pmn')->where('name',$detail->application_location)->first()->value }}
            </td>
            <td align="center">
                <h3>BAUCAR BAYARAN</h3>
            </td>
        </tr>
    </table>
    &nbsp;
    <table width="600px" border="0">
        <tr>
            <td width="50px">Penerima</td>
            <td width="200px"><strong>{{ strtoupper($detail->user->full_name) }}</strong></td>
            <td width="60px">No. Baucar</td>
            <td width="5px">:</td>
            <td width="80px">{{ $detail->transaction->meta->voucher_number }}</td>
        </tr>
        <tr>
            <td valign="top">Alamat</td>
            <td>
                <strong>
                    {{ strtoupper($detail->guardian->address_street.' '.$detail->guardian->address_area) }}
                </strong>
            </td>
            <td valign="top">No. Cek</td>
            <td valign="top">:</td>
            <td valign="top">{{ $detail->transaction->meta->cheque_number }}</td>
        </tr>
        <tr>
            <td></td>
            <td>
                <strong>
                {{ strtoupper($detail->user->profile->address_city.' '.$detail->user->profile->address_postcode) }}<br />
                {{ strtoupper($detail->user->profile->address_district.' '.Code::category('state')->where('name',$detail->user->profile->address_state)->first()->value) }}<br />
                </strong>
            </td>
            <td valign="top">Tarikh Cek</td>
            <td valign="top">:</td>
            <td valign="top">{{ isset($detail->transaction->meta->cheque_date) ? $detail->transaction->meta->cheque_date : '' }}</td>
        </tr>
    </table>
    &nbsp;
    <table width="600px" border="1">
        <tr>
            <td align="center">No. Rujukan</td>
            <td align="center">No. Inbois</td>
            <td colspan="2" align="center">Keterangan</td>
            <td align="center">Amaun</td>
        </tr>
        <tr>
            <td align="center">{{ $detail->user->profile->idno_ic }}</td>
            <td></td>
            <td colspan="2" align="center">Bayaran Pendahuluan Pinjaman Pelajar</td>
            <td align="right">RM {{ $detail->transaction->credit }}</td>
        </tr>
        <tr>
            <td colspan="4">RINGGIT MALAYSIA {{  strtoupper(app('atlantis.helpers')->advance->spellNumeric($detail->transaction->credit)) }} SAHAJA</td>
            <td align="right">RM {{ $detail->transaction->credit }}</td>
        </tr>
        <tr>
            <td colspan="4">Kod Akaun</td>
            <td align="right"></td>
        </tr>
        <tr>
            <td align="center" rowspan="2" colspan="2">Nominal</td>
            <td align="center" colspan="2">Perseorangan</td>
            <td align="center" rowspan="2">Amaun</td>
        </tr>
        <tr>
            <td align="center">Transaksi</td>
            <td align="center">Nombor Akaun</td>
        </tr>
        <tr>
            <td align="center" colspan="2"></td>
            <td align="center">DP</td>
            <td align="center">{{ $detail->user->profile->data->account_no }}</td>
            <td align="right">RM {{ $detail->transaction->credit }}</td>
        </tr>
        <tr>
            <td align="right" colspan="4">Jumlah</td>
            <td align="right">RM {{ $detail->transaction->credit }}</td>
        </tr>
        <tr>
            <td align="center" colspan="4" valign="bottom" height="150px" align="center">
                ............................................................<br />
                Pegawai Berkuasa
            </td>
            <td align="center" valign="bottom">
                ..............................<br />
                Penerima
            </td>
        </tr>
        <tr>
            <td align="center"></td>
            <td align="center" colspan="2">Disediakan</td>
            <td align="center">Disemak</td>
            <td align="center">Diluluskan</td>
        </tr>
        <tr>
            <td>
                Tandatangan
                <p>&nbsp;</p>
                <p>Nama</p>
                <p>Tarikh</p>
            </td>
            <td align="center" colspan="2"></td>
            <td align="center"></td>
            <td align="center"></td>
        </tr>
    </table>
</div>

</body>
</html>