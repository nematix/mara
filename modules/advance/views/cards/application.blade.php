<div class="wizard-input-section row">
    <div class="col-lg-6">
        {{ Former::text('amount_total')
            ->disabled()
            ->class('validate[required,custom[number],max['. $config['application']['amount_maximum'] .']]')
            ->ng_model('detail.amount_total')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_validation()
        }}
    </div>
    <div class="col-lg-6">
        {{ Former::text('account_no')
            ->class('validate[required,custom[number]]')
            ->ng_model('detail.user.profile.data.account_bank_no')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_validation()
        }}
    </div>
</div>
<div class="wizard-input-section">
    {{ Former::select('application_location')
        ->class('validate[required]')
        ->as_ui_validation()
        ->fromQuery( Code::category('pmn')->get(), function($model){return $model->value;}, 'name' )
        ->ng_model('detail.application_location')
        ->ng_disabled($conditions_disable['form'])
        ->ui_select2()
    }}
</div>
<div class="wizard-input-section">
    {{ Former::select('bank_code')
        ->class('validate[required]')
        ->as_ui_validation()
        ->fromQuery( Code::category('bank')->whereIn('name', $config['filters']['category_bank'] )->get(), function($model){return $model->value;}, 'name' )
        ->ng_model('detail.user.profile.data.account_bank_code')
        ->ng_disabled($conditions_disable['form'])
        ->ui_select2()
    }}
</div>
<div class="wizard-input-section row">
    <div class="col-lg-6">
        {{ Form::label(trans('advance::cards.section.application.existing')) }}
    </div>
    <div class="col-lg-3">
        <input type="radio" name="application_existing" value="1" ng-model="detail.application_existing" as-ui-icheck ng-disabled='{{$conditions_disable["form"]}}'>
        {{ Form::label('application_existing',trans('general.title.yes')) }}
        <input name="application_existing" type="hidden" class="validate[required]" ng-model="detail.application_existing" as_ui_validation>
    </div>
    <div class="col-lg-3">
        <input type="radio" name="application_existing" value="0" ng-model="detail.application_existing" as-ui-icheck ng-disabled='{{$conditions_disable["form"]}}'>
        {{ Form::label('application_existing',trans('general.title.no')) }}
    </div>
</div>