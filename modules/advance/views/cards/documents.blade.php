<div class="panel">
    <div class="panel-heading">
        {{ trans('advance::advance.title.document_approved') }}
    </div>
    <ul class="list-group">
        @if( $user_realm->name == 'staff' )
        <li class="list-group-item">
            <div class="form-group">
                <label> No Akaun Pelajar :</label>
                <div class="well"><h3 style="margin: 0; text-align: center;">{{ $record->transaction->credit_account_id or 'Tiada' }}</h3></div>
            </div>
            <div class="form-group">
                <label> Status Pembayaran :</label>
                <div class="alert alert-{{ $record->payment_processing_status == 'complete' ? 'success' : 'warning' }}">{{ trans('advance::advance.status.payment.'.$record->payment_processing_status) }}</div>
            </div>
        </li>
        @endif
        <a class="list-group-item list-group-item-success" href="{{ url('documents/download'.'?name=approval&uuid='.$record->uuid) }}">
            {{ trans('advance::advance.documents.offer_letter') }}
            <div class="pull-right action-buttons">
                <i class="icon icon-cloud-download text-success"></i>
            </div>
        </a>
        <a class="list-group-item list-group-item-success" href="{{ url('assets/documents/agreement.pdf') }}">
            {{ trans('advance::advance.documents.agreement_letter') }}
            <div class="pull-right action-buttons">
                <i class="icon icon-cloud-download text-success"></i>
            </div>
        </a>
        <a class="list-group-item list-group-item-success" href="{{ url('documents/download'.'?name=application&uuid='.$record->uuid) }}">
            {{ trans('advance::advance.documents.applicant_info') }}
            <div class="pull-right action-buttons">
                <i class="icon icon-cloud-download text-success"></i>
            </div>
        </a>
    </ul>
</div>