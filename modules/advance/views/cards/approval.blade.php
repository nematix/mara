<div class="panel" ng-controller="ApplicationApproval">
    <div class="panel-heading">
        {{ trans('advance::advance.title.application_approval') }}
    </div>
    <ul class="list-group" as-ui-card="approval">
        <li class="list-group-item">
            <div class="form-group">
                {{ Former::text('amount_total','amount_total')
                    ->class('form-control')
                    ->ng_disabled($conditions_disable['form'])
                    ->ng_model('detail.amount_total')
                    }}
            </div>
        </li>
        <li class="list-group-item">
            <div class="form-group">
                {{ Former::text('detail_amount_approved',trans('advance::advance.title.application_approval_amount'))
                    ->class('form-control validate[required,custom[number],min['. $config['application']['amount_minimum'] .'],max['. $config['application']['amount_maximum'] .']]')
                    ->ng_model('detail.amount_approved')
                    ->ng_disabled('detail.status.raw >= 7')
                    ->as_ui_validation()
                }}
            </div>
        </li>
        <li class="list-group-item">
            <btn id="btn-action-7"
                    class="action-status btn btn-{{ $detail->status['all'][7]['label'] }} ladda-button"
                    ng-click="approve()"
                    ng-show="{{ (isset($actions_conditions[$record->status['id']][7]) ? $actions_conditions[$record->status['id']][7] : true) }}"
                    ng-disabled="!validation.$error.cards.approval.$valid"
                    as-ui-button
                    as-ui-progress="ladda">
                {{ $detail->status['all'][7]['title'] }}
            </btn>
        </li>
    </ul>
</div>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIAdvanceRecord = appBase + 'api/v1/advance/records/';

        asng.controller('ApplicationApproval',['$scope', 'Rpc', 'Validation', 'Model', function($scope, Rpc, Validation){
            /** Re-enable validation */
            Validation.config.validate = true;
            $scope.validation = Validation;

            /** Advance/Record RPC */
            $scope.record = Rpc.create('Advance/Record');

            $scope.approve = function(){
                $scope.record.execute('approve',{
                    uuid: $scope.detail.uuid,
                    amount_approved: $scope.detail.amount_approved
                }).success(function(response){
                    _a.alert.rpc(response).done(function(){
                        location.reload(true);
                    });
                });
            }
        }]);
    </script>
@stop