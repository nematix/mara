<div ng-controller="AdvanceCourse">
    <div class="wizard-input-section">
        @if( $user_realm->name == 'staff' )
        <div class="input-group">
        @endif
            {{ Former::text('application_coursed')
                ->class('validate[required]')
                ->ng_model('detail.application_coursed')
                ->ng_disabled($conditions_disable['form'])
                ->as_ui_validation()
            }}
        @if( $user_realm->name == 'staff' )
            <span class="input-group-btn">
                <button class="btn btn-primary btn-sm" ng-click="getCourse()"><i class="fa fa-search"> </i> Carian</button>
            </span>
        </div>
        @endif
    </div>
    <div class="wizard-input-section row">
        <div class="col-lg-9">
            @if( $user_realm->name == 'staff' )
            <div class="input-group">
            @endif
                {{ Former::text('institution_name')
                    ->class('validate[required]')
                    ->ng_model('detail.institution_name')
                    ->ng_disabled($conditions_disable['form'])
                    ->as_ui_validation()
                }}

            @if( $user_realm->name == 'staff' )
                <span class="input-group-btn">
                    <button class="btn btn-primary btn-sm" ng-click="getInstitutions()"><i class="fa fa-search"> </i> Carian</button>
                </span>
            </div>
            @endif
        </div>
        <div class="col-lg-3">
            {{ Former::select('institution_state')
                ->class('validate[required]')
                ->fromQuery( Code::category('state')->get(), function($model){return $model->value;}, 'name' )
                ->ng_model('detail.institution_state')
                ->ng_disabled($conditions_disable['form'])
                ->ui_select2()
            }}
        </div>
    </div>
    <div class="wizard-input-section">
        {{ Former::select('course_level')
            ->class('validate[required]')
            ->as_ui_validation()
            ->fromQuery( Code::category('course-level')->get(), function($model){return $model->value;}, 'name' )
            ->ng_model('detail.course_level')
            ->ng_disabled($conditions_disable['form'])
            ->ui_select2()
        }}
    </div>
    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('data_course_start',trans('advance::validation.attributes.course_start'))
                ->ng_model('data.course_start')
                ->ng_disabled($conditions_disable['form'])
                ->as_ui_datepicker('dd-mm-yy')
                ->as_ui_datepicker_options('{altField:"#course_start",altFormat:"yy-mm-dd",yearRange: "c:c+10"}')
            }}
            {{
                Former::text('course_start',false)
                ->class('hidden validate[required,custom[dateUS],future[NOW]]')
                ->ng_model('detail.course_start')
                ->as_ui_validation();
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('data_course_end',trans('advance::validation.attributes.course_end'))
                ->ng_model('data.course_end')
                ->ng_disabled($conditions_disable['form'])
                ->as_ui_datepicker('dd-mm-yy')
                ->as_ui_datepicker_options('{altField:"#course_end",altFormat:"yy-mm-dd",yearRange: "c:c+10"}')
            }}
            {{
                Former::text('course_end',false)
                ->class('hidden validate[required,custom[dateUS],future[#data_course_end_on]]')
                ->ng_model('detail.course_end')
                ->as_ui_validation();
            }}
            <input type="hidden" id="data_course_end_on" name="data_course_end_on" ng-bind="data.courseEndOn()">
        </div>
    </div>
    <div class="wizard-input-section">
        <button class="btn btn-primary" onclick="$('[href=#documents]').tab('show');">Upload Document</button>
    </div>
    @include('advance::partials.code-lookup')
</div>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var url = appBase + 'api/v1/codes/institution-name?format=list';

        /**
         * Temporary date validation alert workaround
         */
        $.validationEngineLanguage.allRules.dateUS.alertText = $.validationEngineLanguage.allRules.date.alertText

        asng.controller('AdvanceCourse', ['$scope','$http','$modal', function($scope,$http,$modal){
            /** Transactional data */
            $scope.data = {
                courseEndOn: function(){
                    var dateEnd = moment($scope.detail.course_start,'YYYY-MM-DD').add(18,'month').format('YYYY-MM-DD');
                    $('#data_course_end_on').val(dateEnd);
                    return dateEnd;
                }
            };

            /** Course name lookup dialog */
            $scope.getCourse = function(){
                $scope.params = {
                    title: 'Carian Kursus',
                    search_key_label: 'Nama Kursus',
                    code_category: 'course-name'
                };

                var codeLookupModal = $modal.open({
                    templateUrl: 'CodeLookuplModal.html',
                    controller: 'CodeLookuplModal',
                    size: '',
                    resolve: {
                        params: function(){
                            return $scope.params;
                        }
                    }
                });

                codeLookupModal.result.then(function (code) {
                    $scope.detail.application_coursed = code.value;
                });
            };

            /** Institution lookup dialog */
            $scope.getInstitutions = function(){
                $scope.params = {
                    title: 'Carian Institusi',
                    search_key_label: 'Nama Institusi',
                    code_category: 'institution-name'
                };

                var codeLookupModal = $modal.open({
                    templateUrl: 'CodeLookuplModal.html',
                    controller: 'CodeLookuplModal',
                    size: '',
                    resolve: {
                        params: function(){
                            return $scope.params;
                        }
                    }
                });

                codeLookupModal.result.then(function (code) {
                    $scope.detail.institution_name = code.value;
                });
            };

        }]);
    </script>
@stop