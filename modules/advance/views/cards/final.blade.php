{{ Former::open() }}
<div class="wizard-input-section">
    <p>{{ trans('advance::cards.section.final.terms_title') }}</p>
    <textarea rows="6" placeholder="{{ trans('advance::cards.section.final.terms_title') }}">{{ trans('advance::cards.section.final.terms_body') }}</textarea>
</div>
<div class="wizard-input-section">
    {{ Former::input('final')
        ->type('checkbox')
        ->as_ui_icheck()
        ->ng_model('final')
        ->ng_disabled($conditions_disable['form'])
    }}
    {{ Former::label(trans('advance::cards.section.final.checkbox_final')) }}
</div>
{{ Former::close() }}