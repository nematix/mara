<div ng-controller="AdvanceGuardian">
    <div class="wizard-input-section">
        {{ Former::text('guardian_id')
            ->class('validate[required,custom[idICNo]]')
            ->ng_model('detail.guardian.idno_ic')
            ->ng_model_options('{debounce:500}')
            ->ng_keyup('checkGuardian($event)')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_validation()
        }}
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('guardian[first_name]',trans('advance::cards.section.guardian.first_name'))
                ->ng_model('detail.guardian.first_name')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('guardian[last_name]',trans('advance::cards.section.guardian.last_name'))
                ->ng_model('detail.guardian.last_name')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section">
        {{ Former::text('guardian[address_street]',trans('advance::cards.section.guardian.address_street'))
            ->ng_model('detail.guardian.address_street')
            ->ng_disabled($conditions_disable['form'])
            ->class('validate[required]')
            ->as_ui_validation()
        }}
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('guardian[address_district]',trans('advance::cards.section.guardian.address_district'))
                ->ng_model('detail.guardian.address_district')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('guardian[address_city]',trans('advance::cards.section.guardian.address_city'))
                ->ng_model('detail.guardian.address_city')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-8">
            {{ Former::select('guardian[address_state]',trans('advance::cards.section.guardian.address_state'))
                ->class('validate[required]')
                ->as_ui_validation()
                ->ng_model('detail.guardian.address_state')
                ->ng_disabled($conditions_disable['form'])
                ->fromQuery( Code::category('state')->get(), function($model){return $model->value;}, 'name' )
                ->ui_select2()
            }}
        </div>
        <div class="col-lg-4">
            {{ Former::text('guardian[address_postcode]',trans('advance::cards.section.guardian.address_postcode'))
                ->ng_model('detail.guardian.address_postcode')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[postcode]]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('guardian[contact_home]',trans('advance::cards.section.guardian.contact_home'))
                ->ng_model('detail.guardian.contact_home')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[custom[number]]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('guardian[contact_mobile]',trans('advance::cards.section.guardian.contact_mobile'))
                ->ng_model('detail.guardian.contact_mobile')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[number]]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-3">
            {{ Former::text('guardian_salary_gross',trans('advance::cards.section.guardian.guardian_salary_gross'))
                ->ng_model('detail.guardian_salary_gross')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[number],min['. $config['application']['salary_guardian_min'] .']]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-3">
            {{ Former::text('guardian_family_no',trans('advance::cards.section.guardian.guardian_family_no'))
                ->ng_model('detail.guardian_family_no')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[number]]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::select('guardian_employment',trans('advance::cards.section.guardian.guardian_employment'))
                ->ng_model('detail.guardian_employment')
                ->ng_disabled($conditions_disable['form'])
                ->fromQuery( Code::category('class')->get(), function($model){return $model->value;}, 'name' )
                ->ui_select2()
            }}
        </div>
    </div>
</div>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">

        asng.controller('AdvanceGuardian', ['$scope','Rest',function($scope, Rest){
            /** Globals */
            var curScope = $scope;

            /** Check guardian for blacklist */
            $scope.checkGuardian = function(elem){
                var elementInput = $(elem.target);

                if( !elementInput.validationEngine('validate') ){
                    var idNoIc = elementInput.val();

                    $.getJSON(urlAPIPeople + 'search',{idno_ic:idNoIc}, function(data){
                        if( data.blacklist === 1 ){
                            var modal_body = '{{ trans("advance::advance.blacklist.modal-body") }}';
                            modal_body = modal_body.replace(':idno_ic',idNoIc);

                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_WARNING,
                                message: modal_body,
                                onhide: function(dialogRef){
                                    elementInput.val('');
                                }
                            });

                        }else if( data.id ){
                            curScope.getGuardian(data.id);

                        }else{
                            curScope.detail.guardian_id = '';
                        }
                    });
                }
            };

            /** Attach guardian detail */
            $scope.getGuardian = function(guardianId){
                $scope.detail.guardian_id = guardianId;
                $scope.detail.guardian = Rest.get({model:'peoples', id:guardianId});
            };

        }]);

    </script>
@stop