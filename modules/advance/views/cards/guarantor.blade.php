<div ng-controller="AdvanceGuarantor">
    <div class="wizard-input-section row">
        <div class="col-lg-8">
            {{ Former::text('guarantor_id')
                ->class('validate[required,custom[idICNo]]')
                ->ng_model('detail.guarantor.idno_ic')
                ->ng_model_options('{debounce:500}')
                ->ng_keyup('checkGuarantor($event)')
                ->ng_disabled($conditions_disable['form'])
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-4" style="padding-top: 23px;">
            <input type="checkbox" name="is_guardian" value="1" as-ui-icheck ng-model="is_guardian" ng-change="isGuardian()" ng-checked="validation.cards.guardian[0].$valid && validation.cards.guarantor[0].$valid">
            {{ Form::label('is_guardian','Penjamin adalah penjaga') }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('guarantor[first_name]',trans('advance::cards.section.guarantor.first_name'))
                ->ng_model('detail.guarantor.first_name')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('guarantor[last_name]',trans('advance::cards.section.guarantor.last_name'))
                ->ng_model('detail.guarantor.last_name')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section">
        {{ Former::text('guarantor[address_street]',trans('advance::cards.section.guarantor.address_street'))
            ->ng_model('detail.guarantor.address_street')
            ->ng_disabled($conditions_disable['form'])
            ->class('validate[required]')
            ->as_ui_validation()
        }}
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('guarantor[address_district]',trans('advance::cards.section.guarantor.address_district'))
                ->ng_model('detail.guarantor.address_district')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('guarantor[address_city]',trans('advance::cards.section.guarantor.address_city'))
                ->ng_model('detail.guarantor.address_city')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-8">
            {{ Former::select('guarantor[address_state]',trans('advance::cards.section.guarantor.address_state'))
                ->class('validate[required]')
                ->as_ui_validation()
                ->ng_model('detail.guarantor.address_state')
                ->ng_disabled($conditions_disable['form'])
                ->fromQuery( Code::category('state')->get(), function($model){return $model->value;}, 'name' )
                ->ui_select2()
            }}
        </div>
        <div class="col-lg-4">
            {{ Former::text('guarantor[address_postcode]',trans('advance::cards.section.guarantor.address_postcode'))
                ->ng_model('detail.guarantor.address_postcode')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[postcode]]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-6">
            {{ Former::text('guarantor[contact_home]',trans('advance::cards.section.guarantor.contact_home'))
                ->ng_model('detail.guarantor.contact_home')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[custom[number]]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-6">
            {{ Former::text('guarantor[contact_mobile]',trans('advance::cards.section.guarantor.contact_mobile'))
                ->ng_model('detail.guarantor.contact_mobile')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[number]]')
                ->as_ui_validation()
            }}
        </div>
    </div>

    <div class="wizard-input-section row">
        <div class="col-lg-8">
            {{ Former::text('guarantor_salary_gross',trans('advance::cards.section.guarantor.guarantor_salary_gross'))
                ->ng_model('detail.guarantor_salary_gross')
                ->ng_disabled($conditions_disable['form'])
                ->class('validate[required,custom[number],min['. $config['application']['salary_guarantor_min'] .']]')
                ->as_ui_validation()
            }}
        </div>
        <div class="col-lg-4">
            {{ Former::select('guarantor_status')
                ->ng_model('detail.guarantor_status')
                ->ng_disabled($conditions_disable['form'])
                ->options(array(
                    1 => trans('advance::cards.section.guarantor.guarantor_status_parent'),
                    2 => trans('advance::cards.section.guarantor.guarantor_status_not_parent')
                ))
                ->ui_select2()
            }}
        </div>
    </div>
</div>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">

        asng.controller('AdvanceGuarantor', ['$scope','Rest',function($scope, Rest){
            /** Globals */
            var curScope = $scope;

            /** Check for blacklist */
            $scope.checkGuarantor = function(elem){
                var elementInput = $(elem.target);

                if( !elementInput.validationEngine('validate') ){
                    var idNoIc = elementInput.val();

                    $.getJSON(urlAPIPeople + 'search',{idno_ic:idNoIc}, function(data){
                        if( data.blacklist === 1 ){
                            var modal_body = '{{ trans("advance::advance.blacklist.modal-body") }}';
                            modal_body = modal_body.replace(':idno_ic',idNoIc);

                            BootstrapDialog.show({
                                type: BootstrapDialog.TYPE_WARNING,
                                message: modal_body,
                                onhide: function(dialogRef){
                                    elementInput.val('');
                                }
                            });

                        }else if( data.id ){
                            curScope.getGuarantor(data.id);

                        }else{
                            curScope.detail.guarantor_id = '';
                        }
                    });
                }
            };

            /** Attach guarantor detail */
            $scope.getGuarantor = function(guarantorId){
                $scope.detail.guarantor_id = guarantorId;
                $scope.detail.guarantor = Rest.get({model:'peoples', id:guarantorId});
            };


            $scope.isGuardian = function(){
                if( $scope.is_guardian ){
                    $scope.updateDetail(function(){
                        $scope.getGuarantor($scope.detail.guardian_id);
                        $scope.detail.guarantor_salary_gross = $scope.detail.guardian_salary_gross;
                    });
                }
            }
        }]);

    </script>
@stop