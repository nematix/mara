<div class="panel" ng-controller="PaymentVoucher">
    <div class="panel-heading">
        {{ trans('advance::advance.title.application_payment') }}
    </div>
    <ul class="list-group" as-ui-card="voucher">
        <li class="list-group-item">
            <div class="form-group">
                <label> No Kelulusan :</label>
                <div class="well"><h3 style="margin: 0; text-align: center;">{{ $record->transaction->credit_account_id or 'Tiada' }}</h3></div>
            </div>
            <div class="form-group">
                <label> Status Pembayaran :</label>
                <div class="alert alert-{{ $record->payment_processing_status == 'complete' ? 'success' : 'warning' }}">{{ trans('advance::advance.status.payment.'.$record->payment_processing_status) }}</div>
            </div>
        </li>
        @if($record->transaction->credit_account_id)
        <li class="list-group-item">
            <div class="form-group">
                {{ Former::text('voucher_number')
                    ->class('form-control validate[required]')
                    ->ng_disabled($record->payment_processing_status == 'complete' ? 1 : 0)
                    ->ng_model('detail.transaction.meta.voucher_number')
                    ->as_ui_validation() }}

            </div>
            <div class="form-group">
                {{ Former::text('cheque_number')
                    ->class('form-control validate[required]')
                    ->ng_disabled($record->payment_processing_status == 'complete' ? 1 : 0)
                    ->ng_model('detail.transaction.meta.cheque_number')
                    ->as_ui_validation() }}
            </div>
            <div class="form-group">
                {{ Former::text('cheque_date')
                    ->class('form-control validate[required]')
                    ->ng_disabled($record->payment_processing_status == 'complete' ? 1 : 0)
                    ->ng_model('detail.transaction.meta.cheque_date')
                    ->as_ui_datepicker('dd-mm-yy') }}
            </div>
        </li>
        <li class="list-group-item">
            <button class="btn btn-success" ng-click="payment()" ng-disabled="!validation.$error.cards.voucher.$valid || {{ $record->payment_processing_status == 'complete' ? 1 : 0 }}">{{ trans('general.title.update') }}</button>
            <a class="btn btn-primary" ng-disabled="{{ $record->payment_processing_status == 'complete' || $record->payment_processing_status == 'processing' ? true : false }}" href="{{ url('documents/download'.'?name=voucher&uuid='.$record->uuid) }}">{{ trans('general.title.print') }}</a>
        </li>
        @endif
    </ul>
</div>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIAdvanceRecord = appBase + 'api/v1/documents/';

        asng.controller('PaymentVoucher',['$scope', 'Rpc', 'Validation', function($scope, Rpc, Validation){
            /** Re-enable validation */
            Validation.config.validate = true;
            $scope.validation = Validation;

            /** Advance/Record RPC */
            $scope.record = Rpc.create('Advance/Record');

            $scope.payment = function(){
                $scope.record.execute('payment',{
                    uuid: $scope.detail.uuid,
                    voucher_number: $scope.detail.transaction.meta.voucher_number,
                    cheque_number: $scope.detail.transaction.meta.cheque_number,
                    cheque_date: $scope.detail.transaction.meta.cheque_date
                }).success(function(response){
                    _a.alert.rpc(response).done(function(){
                        location.reload(true);
                    });
                });
            }
        }]);
    </script>
@stop