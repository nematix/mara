<div class="wizard-input-section row">
    <div class="col-lg-6">
        {{ Former::text('first_name')
            ->class('validate[required,custom[onlyLetterSpEx]]')
            ->ng_model('detail.user.first_name')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_validation()
        }}
    </div>
    <div class="col-lg-6">
        {{ Former::text('last_name')
            ->class('validate[required,custom[onlyLetterSpEx]]')
            ->ng_model('detail.user.last_name')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_validation()
        }}
    </div>
</div>

<div class="wizard-input-section">
    {{ Former::text('idno_ic')
        ->ng_model('detail.user.profile.idno_ic')
        ->disabled()
    }}
</div>
<div class="wizard-input-section">
    {{ Former::text('email')
        ->disabled()
        ->ng_model('detail.user.email')
    }}
</div>

<div class="wizard-input-section row">
    <div class="col-lg-3">
        <input type="radio" name="profile.gender" value="male" ng-model="detail.user.profile.gender" as-ui-icheck ng-disabled='{{$conditions_disable["form"]}}')>
        {{ Form::label(trans('advance::cards.section.personal.gender-male')) }}
        <input type="hidden" name="profile.gender" class="validate[required]" ng-model="detail.user.profile.gender" as-ui-validation>
    </div>
    <div class="col-lg-3">
        <input type="radio" name="profile.gender" value="female" ng-model="detail.user.profile.gender" as-ui-icheck ng-disabled='{{$conditions_disable["form"]}}')>
        {{ Form::label(trans('advance::cards.section.personal.gender-female')) }}
    </div>
    <div class="col-lg-6">
        {{ Former::text('birth_date')
            ->class('validate[required,custom[date],past[now]]')
            ->ng_model('detail.user.profile.birth_date')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_datepicker('dd-mm-yy')
            ->as_ui_datepicker_options('{yearRange: "c-50:c"}')
            ->as_ui_validation()
        }}
    </div>
</div>

<div class="wizard-input-section">
    {{ Former::select('race')
        ->class('validate[required]')
        ->fromQuery( Code::parentName('native'), function($model){return $model->value;}, 'name' )
        ->ng_model('detail.user.profile.race')
        ->ng_disabled($conditions_disable['form'])
        ->ui_select2()
    }}
</div>

<div class="wizard-input-section">
    {{ Former::text('address_street')
        ->ng_model('detail.user.profile.address_street')
        ->ng_disabled($conditions_disable['form'])
        ->class('validate[required]')
        ->as_ui_validation()
    }}
</div>

<div class="wizard-input-section row">
    <div class="col-lg-6">
        {{ Former::text('address_area')
            ->ng_model('detail.user.profile.address_area')
            ->ng_disabled($conditions_disable['form'])
            ->as_ui_validation()
        }}
    </div>
    <div class="col-lg-6">
        {{ Former::text('address_city')
            ->ng_model('detail.user.profile.address_city')
            ->ng_disabled($conditions_disable['form'])
            ->class('validate[required]')
            ->as_ui_validation()
        }}
    </div>
</div>

<div class="wizard-input-section row">
    <div class="col-lg-8">
        {{ Former::select('address_state')
            ->class('validate[required]')
            ->as_ui_validation()
            ->ng_model('detail.user.profile.address_state')
            ->ng_disabled($conditions_disable['form'])
            ->fromQuery( Code::category('state')->get(), function($model){return $model->value;}, 'name' )
            ->ui_select2()
        }}
    </div>
    <div class="col-lg-4">
        {{ Former::text('address_postcode')
            ->ng_model('detail.user.profile.address_postcode')
            ->ng_disabled($conditions_disable['form'])
            ->class('validate[required,custom[postcode]]')
            ->as_ui_validation()
        }}
    </div>
</div>

<div class="wizard-input-section row">
    <div class="col-lg-6">
        {{ Former::text('contact_home')
            ->ng_model('detail.user.profile.contact_home')
            ->ng_disabled($conditions_disable['form'])
            ->class('validate[custom[number]]')
            ->as_ui_validation()
        }}
    </div>
    <div class="col-lg-6">
        {{ Former::text('contact_mobile')
            ->ng_model('detail.user.profile.contact_mobile')
            ->ng_disabled($conditions_disable['form'])
            ->class('validate[required,custom[number]]')
            ->as_ui_validation()
        }}
    </div>
</div>