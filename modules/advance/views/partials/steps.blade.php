@section('stylesheet')
    @parent
    <style>
        .step-title {
            min-height: 20px;
            float:left;
        }
        .step-title {
            padding-left: 0;
            padding-right: 0;
            width: 99.5% !important;
            min-height: 28px;
            display: block;
        }
        .step-title .step-order {
            float: left;
            margin-left: 12%;
        }
        .step-title .step-name {
            float: left;
            margin-left: 10px;
        }
        .step-title i[class^="icon-"] {
            float: right;
            margin-right: 10px;
        }

        .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
            pointer-events: inherit;
        }
    </style>
@stop

<div id="advance-steps" as-ui-wizard-step as-ui-callback-validation="stepCallback($event,element)">
    <div class="col-lg-3">
        @if( $record->status['raw'] == 7 )
            @include('advance::cards.documents')
        @endif
        <div class="row padded hidden-sm hidden-xs" style="padding-top:0">
            <div class="step-title"><span class="step-order">1</span><span class="step-name">{{ trans('advance::cards.title.info') }}</span></div>
            <div class="step-title"><span class="step-order">2</span><span class="step-name">{{ trans('advance::cards.title.application') }}</span></div>
            <div class="step-title"><span class="step-order">3</span><span class="step-name">{{ trans('advance::cards.title.course') }}</span></div>
            <div class="step-title"><span class="step-order">4</span><span class="step-name">{{ trans('advance::cards.title.guardian') }}</span></div>
            <div class="step-title"><span class="step-order">5</span><span class="step-name">{{ trans('advance::cards.title.guarantor') }}</span></div>
            <div class="step-title"><span class="step-order">6</span><span class="step-name">{{ trans('advance::cards.title.final') }}</span></div>
        </div>

        <div class="box padded">
            <!--
            <button class="btn back-button">{{ trans('admin::user.btn_back') }}</button>
            <button class="btn next-button">{{ trans('admin::user.btn_next') }}</button>
            -->
            <button id="btn-update" class="btn btn-default" ng-click="detailUpdate()" ng-hide="{{ $conditions_disable['form'] }}">{{ trans('general.title.update') }}</button>
            <button id="btn-submit" class="btn btn-primary hidden" ng-click="detailSubmit()">{{ trans('admin::user.btn_submit') }}</button>
        </div>
    </div>

    <div class="col-lg-9">
        <form name="information" class="form-horizontal fill-up" novalidate>
            <div class="paper">
                <div class="paper-header">
                    <span class="title">{{ $detail->config->detail_title }}</span>
                </div>
                <div class="paper-content padded">
                    <!-- -------------------------------------
                     Personal Information
                     ---------------------------------------->
                    <div id="pstep1" class="step-content" as-ui-card="info">
                        @include('advance::cards.info')
                    </div>

                    <!-- -------------------------------------
                     Application Info
                     ---------------------------------------->
                    <div id="pstep2" class="step-content" as-ui-card="application">
                        @include('advance::cards.application')
                    </div>

                    <!-- -------------------------------------
                     Course Info
                     ---------------------------------------->
                    <div id="pstep3" class="step-content" as-ui-card="course">
                        @include('advance::cards.course')
                    </div>

                    <!-- -------------------------------------
                     Guardian Info
                     ---------------------------------------->
                    <div id="pstep4" class="step-content" as-ui-card="guardian">
                        @include('advance::cards.guardian')
                    </div>

                    <!-- -------------------------------------
                     Guarantor Info
                     ---------------------------------------->
                    <div id="pstep5" class="step-content" as-ui-card="guarantor">
                        @include('advance::cards.guarantor')
                    </div>
                    <!-- -------------------------------------
                     Final
                     ---------------------------------------->
                    <div id="pstep6" class="step-content" as-ui-card="final">
                        @include('advance::cards.final')
                    </div>
                </div>

            </div>
        </form>
    </div>
</div>