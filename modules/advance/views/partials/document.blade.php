<div class="AdvanceDocument" ng-controller="AdvanceDocument">
    <div class="col-md-9">
        <div class="box">
            <div id="upload-core" class="box-content">
                <table class="table table-normal">
                    <thead>
                        <tr>
                            <td colspan="3">{{ trans('advance::cards.section.document.description') }}</td>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach ($document_keys as $key)
                        <tr id="upload-{{ $key }}" qquuid="{{ isset($documents[$key]) ? $documents[$key]->uuid : '' }}" class="{{ isset($documents[$key]) ? 'status-success' : 'status-pending' }}">
                            <td class="icon"><i class="fa fa-cloud-upload"></i></td>
                            <td>{{ trans("advance::advance.documents.{$key}") }}
                                <span class="file-name">
                                    @if( isset($documents[$key]) )
                                        ( {{ $documents[$key]->image->originalFilename() }} )
                                    @endif
                                </span>
                                <div class="progress progress-striped active hide" style="margin: 0;">
                                    <div class="progress-bar progress-blue" data-percent="0" style="width: 0%"></div>
                                </div>
                            </td>
                            <td class="task">
                                <btn id="{{ $key }}" class="upload btn btn-xs btn-default"><i class="fa fa-arrow-up"></i></btn>
                                <btn id="action-delete-{{ $key }}" class="delete btn btn-xs btn-default {{ !isset($documents[$key]) ? 'disabled' : '' }}" ng-click="{{ isset($documents[$key]) ? 'deleteDocument(\''.$documents[$key]->uuid.'\')' : '' }}"><i class="fa fa-trash-o"></i></btn>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="panel">
            <div class="panel-heading">
                {{ trans('admin::user.title_task') }}
            </div>
            <ul class="list-group">
                <li class="list-group-item list-group-item-info">
                    <i class="fa fa-exclamation"></i>
                    {{ trans('advance::cards.section.document.allowed_file_types') }}
                </li>
                <li class="list-group-item">
                    <a href="#" ng-click="clearDocument()"><i class="fa fa-ban"></i> {{ trans('advance::cards.section.document.button_clear') }}</a>
                </li>
            </ul>
        </div>
    </div>
</div>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIDocument = appBase + 'api/v1/documents/';

        asng.controller('AdvanceDocument', ['$scope','$http','$compile', function($scope, $http, $compile){
            /** Clear documents */
            $scope.clearDocument = function(){
                BootstrapDialog.confirm('{{ trans('content::content.text.document_clear') }}',function(result){
                    if( result ){
                        $http.post(urlAPIDocument + 'clear', {detail_uuid:'{{ $record->uuid }}'}).success(function(res){
                            res = res || {};
                            _a.alert.server(res);
                            if( res._status['type'] == 'success' ){
                                $("#upload-core .status-success").each(function(){
                                    $(this).removeClass('status-success').addClass('status-pending');
                                    $(this).find('.file-name').html('');
                                })
                            }
                        })
                    }
                })
            };

            /** Preparing delete button */
            $scope.prepareDeleteButton = function(elem,uuid){
                var element_button = $(elem);
                element_button.attr('ng-click',"deleteDocument('" + uuid + "')")
                $compile($('.AdvanceDocument'))($scope);
            };

            /** Delete document */
            $scope.deleteDocument = function(uuid){
                var fileElement = $("[qquuid*='"+uuid+"']");

                /** Delete document */
                $http.delete(urlAPIDocument + uuid).success(function(res){
                    res = res || {};
                    _a.alert.server(res);
                    if( res._status['type'] == 'success' ){
                        fileElement.removeClass('status-success').addClass('status-pending').attr('qquuid','');
                        fileElement.find('.file-name').html('');
                        fileElement.find('.delete').addClass('disabled');
                    }
                })
            };
        }]);

        $(document).ready(function(){
            //[i] Document Uploader
            $("#upload-core").fineUploader({
                uploaderType: 'basic',
                request: {
                    endpoint: appBase + 'api/v1/documents'
                },
                validation: {
                    allowedExtensions: ["png","jpeg","jpg","tiff"],
                    sizeLimit: 5000000
                },
                extraButtons: [
                    {element: $("#ic_student")},
                    {element: $("#ic_guardian")},
                    {element: $("#ic_guarantor")},
                    {element: $("#cert_birth")},
                    {element: $("#cert_birth_guardian")},
                    {element: $("#statement_salary_guardian")},
                    {element: $("#statement_salary_guarantor")}
                ]

            }).on("error", function(event,id,name,errorReason,xhr){
                var button = $(this).fineUploader("getButton", id);
                var uploadId = $(button).attr('id');

                //[i] Alert error on server response
                if(_a) {
                    if( typeof errorReason == 'object' ){
                        _a.alert.server(errorReason);
                        $('#upload-' + uploadId + ' .file-name').html('');
                        $('#upload-' + uploadId).removeClass('status-info status-success').addClass('status-pending');

                    } else {
                        _a.alert.warning(errorReason);
                    }
                }

            }).on("upload", function(event, id, name) {
                var button = $(this).fineUploader("getButton", id);
                var uploadId = $(button).attr('id');

                //[i] Add file to upload queue
                $(this).fineUploader("setParams", {user_id:'{{ $user->id }}', record_uuid:'{{ $record->uuid }}', document_key:uploadId}, id);

            }).on("submit", function(event, id, name) {
                var button = $(this).fineUploader("getButton", id);
                var uploadId = $(button).attr('id');

                $('#upload-'+uploadId).removeClass('status-pending').addClass('status-info');
                $('#upload-'+uploadId+' .file-name').html('( '+name+' )');

            }).on('progress', function(event,id,name,uploadedBytes,totalBytes){
                var button = $(this).fineUploader("getButton", id);
                var uploadId = $(button).attr('id');
                var percent = Math.round( ( uploadedBytes / totalBytes ) * 100 );
                var progress = $('#upload-'+uploadId+' .progress');

                if( percent < 100 ){
                    progress.removeClass('hide').find('.progress-bar').css('width',percent+'%');
                }else{
                    progress.find('.progress-bar').css('width',percent+'%').removeClass('progress-blue').addClass('progress-green');
                    setTimeout(function(){
                        progress.fadeOut().addClass('hide');
                    },2000);
                }

            }).on("complete", function(event, id, name, responseJSON) {
                var button = $(this).fineUploader("getButton", id);
                var uploadId = $(button).attr('id');

                //i: Change visual indicator
                $('#upload-'+uploadId).removeClass('status-info').addClass('status-success').attr('qquuid',responseJSON.qquuid);
                $('#action-delete-'+uploadId).removeClass('disabled');

                var scope = angular.element('.AdvanceDocument').scope();
                scope.prepareDeleteButton('#action-delete-'+uploadId,responseJSON.qquuid);
            });
        })
    </script>
@stop