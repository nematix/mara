<div class="box">
    <div class="box-header">
        <span class="title">{{ trans('message::message.title.conversations') }}</span>
    </div>
</div>
<ul class="chat-box timeline">
    @if( isset($comments) )
    @foreach( $comments as $comment )
    <li class="arrow-box-left gray">
        <div class="avatar"><img src="{{ Gravatar::src( $comment->user->email ) }}" class="avatar-small"></div>
        <div class="info">
                    <span class="name">
                        @if( !empty($comment->meta->detail->status) )
                        <span class="label label-{{ $detail->status['all'][$comment->meta->detail->status]['label'] }}">{{ $detail->status['all'][$comment->meta->detail->status]['title'] }}</span> <strong class="indent">{{ $comment->user->full_name }}</strong>
                        @elseif( !empty($comment->meta->permission->read) && !empty($comment->meta->permission->reply) )
                        <span class="label label-info">{{ trans('general.title.update') }}</span> <strong class="indent">{{ $comment->user->full_name }}</strong>
                        @else
                        <span class="label label-success">{{ trans('general.title.notify') }}</span> <strong class="indent">{{ $comment->user->full_name }}</strong>
                        @endif
                    </span>
            <span class="time"><i class="icon-time" title="{{ $comment->created_at->toDayDateTimeString() }}"></i> {{ $comment->created_when }}</span>
        </div>
        <div class="content">
            {{ $comment->body }}
        </div>
    </li>
    @endforeach
    @endif
</ul>