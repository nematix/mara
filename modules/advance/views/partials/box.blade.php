@foreach( $detail->cards as $card )
    @if( $user_realm->name == 'staff' && $card == 'info' )
        <!-- {{ $card = 'profile' }} -->
    @endif

    @if( $card != 'final' )
    <form name="{{ $card }}" class="form-horizontal fill-up" novalidate>
        <fieldset as-ui-card="{{ $card }}">
            <div class="box">
                <div class="box-header">
                    <span class="title">
                        <i class="fa fa-plus-square"></i>
                        <a href="#" ng-click="boxes['{{ $card }}'].$valid = !boxes['{{ $card }}'].$valid">{{ trans('advance::cards.title.'.$card) }}</a>
                        <i class="fa fa-lock" ng-show="{{ $conditions_disable['form'] }}"></i>
                    </span>
                    <ul class="box-toolbar">
                        <li ng-show="validation.$error.cards['{{ $card }}'].$valid" ng-cloak><span class="label label-green">{{ trans('advance::advance.status.staff.complete') }}</span></li>
                        <li ng-hide="validation.$error.cards['{{ $card }}'].$valid" ng-cloak><span class="label label-red">[[ validation.$error.cards['{{ $card }}'].$count ]]</span></li>
                    </ul>
                </div>
                <div class="box-content padded" collapse="boxes['{{ $card }}'].$valid">
                    @include('advance::cards.'.$card)
                </div>
            </div>
        </fieldset>
    </form>
    @endif
@endforeach