<div class="controller box closable-chat-box open" ng-controller="boxController">
    <div class="box-content padded">
        <div class="fields">
            <div class="avatar"><img class="avatar-small" src="{{ Gravatar::src($user->email) }}" /></div>
            <ul>
                <li><b><a href="#">{{ $user->first_name }}</a></b></li>
                <li class="note">{{ $user_realm->description }}</li>
            </ul>
        </div>
        <div class="chat-message-box">
            <textarea as-ui-editor-wysihtml name="comment" ng-model="message.params.body" rows="3"></textarea>
        </div>
        <div class="clearfix actions">
            <span class="note"></span>
            <div class="pull-left faded-toolbar">
                @if( User::find($user->id)->can('message.send') )
                    <btn id="btn-action-send" class="action-message btn btn-default ladda-button" ng-click="sendMessage()" as-ui-button as-ui-progress="ladda">{{ trans('general.title.send') }}</btn>
                @endif
            </div>
            <div class="pull-right faded-toolbar">
                <a id="btn-action-update" class="btn btn-blue ladda-button" ng-click="detailUpdate()" as-ui-button as-ui-progress="ladda"><span class="ladda-label">{{ trans('general.title.update') }}</span></a>
                @if( isset($actions) )
                    @foreach( $actions as $action )
                    <btn
                        id="btn-action-{{ $action }}"
                        class="action-status btn btn-{{ $detail->status['all'][$action]['label'] }} ladda-button"
                        ng-click="statusUpdate({{ $action }})"
                        ng-show="{{ (isset($actions_conditions[$record->status['id']][$action]) ? $actions_conditions[$record->status['id']][$action] : true) }}"
                        as-ui-button as-ui-progress="ladda">
                        {{ $detail->status['all'][$action]['title'] }}
                    </btn>
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>


@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIMessage = appBase + 'api/v1/messages';
        var urlAPIDetailStatus = appBase + 'api/v1/details/status';

        function boxController($scope, Resource){
            $scope.message = Resource.create(urlAPIDetailStatus,{
                sender_id: '{{ $user->id }}',
                receiver_id: '{{ $record->user->id }}',
                conversation_id: '{{ isset($conversation) ? $conversation->id : -1 }}',
                subject: '{{ isset($conversation) ? $conversation->subject : $record->uuid }}'
            });

            $scope.sendMessage = function(){
                $scope.message.params.notify = true;
                $scope.message.store(function(response){
                    if( response._status.type = 'success' ) location.reload(true);
                });
            };

            $scope.detailUpdate = function(){
                $scope.message.store(function(response){
                    if( response._status.type == 'success' ) {
                        $scope.$parent.detail.$update($scope.$parent.detail_params).then(function(response){
                            if( response.data._status.type == 'success' ) location.reload(true);
                        });
                    }
                });
            };

            $scope.statusUpdate = function(action){
                $scope.$parent.detail.status.id = action;

                $scope.message.params.status = action;
                $scope.message.store(function(response){
                    if( response._status.type == 'success' ){
                        $scope.$parent.detail.$update($scope.$parent.detail_params).then(function(response){
                            if( response.data._status.type == 'success' ) location.reload(true);
                        });
                    }
                });
            };
        }
    </script>
@stop