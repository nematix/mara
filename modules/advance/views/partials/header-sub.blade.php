<div class="area-top clearfix">
    <div class="pull-left header">
        <h3 class="title">
            <i class="{{ $header['icon'] }}"></i>
            {{ $header ? $header['title'] : '' }}
        </h3>
        <h5>
            <span>
            {{ $header ? $header['sub'] : '' }}
            </span>
        </h5>
    </div>
    @if( $user_realm->name == 'staff' && isset($status_role) )
    <ul class="list-inline pull-right sparkline-box">
        @foreach( $status_role as $status )
        <li class="sparkline-row">
            <h4 class="{{ $status['color'] }}"><span>{{ $status['title'] }}</span> {{ $status['total'] }}</h4>
        </li>
        @endforeach
    </ul>
    @endif
</div>