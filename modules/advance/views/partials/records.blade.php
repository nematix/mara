<div class="box">
    <div class="box-header">
        <div class="title">{{ $detail->description }}</div>
    </div>
    <div class="box-content">
        @if( $records->count() > 0 )
            @foreach( $records as $index => $record )
            <div class="box-section col-fixed">
                <div class="col-fixed-30 no-padding">
                    <img src="data:image/png;base64,{{ Barcode::getBarcodePNG($detail->name.'://'.$record->uuid,'QRCODE',2,2) }}">
                </div>
                <div class="content">
                    <span class="title"><a href="{{ $record->url_update }}" class="text-{{ $record->status['label'] }}">Permohonan #{{ $index+1 }} {{ $record->status['id'] >= 4 ? "[{$record->application_location_name}]" : '' }}</a></span>
                    <div><i class="fa fa-clock-o"></i> Created {{ $record->created_when }} at {{ $record->created_at->toDateTimeString() }}</div>
                    <div><i class="fa fa-clock-o"></i> Last update {{ $record->updated_when }}</div>
                </div>
                <div class="col-fixed-30">
                    <span class="label label-{{ $record->status['label'] }}">{{ $record->status['title'] }}</span>
                </div>
            </div>
            @endforeach
        @else
            <div class="box-section col-fixed">
                <a class="btn btn-primary" href="{{ route('advance.student.new',['name'=>'advance']) }}">{{ trans('advance::advance.student.new') }}</a>
            </div>
        @endif
    </div>
</div>