<script type="text/ng-template" id="CodeLookuplModal.html">
    <div class="modal-header">
        <h3 class="modal-title">[[ params.title ]]</h3>
    </div>
    <div class="modal-body">
        <form class="form-horizontal">
            <div class="form-group">
                <label class="col-sm-3 control-label" for="search_key">[[ params.search_key_label ]]</label>
                <div class="col-sm-9">
                    <input type="text" id="search_key" class="form-control" ng-model="search_key" ng-model-option="{debounce:100}">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    <select class="form-control" size="5" ng-model="selected_code" ng-options="c.value for c in codes | filter: {value:search_key}"></select>
                </div>
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-click="ok()" ng-disabled="!validation.$error.controls.$valid">Pilih</button>
        <button class="btn btn-warning" ng-click="cancel()">Batal</button>
    </div>
</script>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        asng.controller('CodeLookuplModal', function($scope, $modalInstance, params, Resource, Validation){
            $scope.validation = Validation;
            $scope.params = params;

            var apiUrl = appBase + 'api/v1/codes',
                codes = Resource.create(apiUrl);

            /** Get codes */
            codes.show($scope.params.code_category,function(server){
                $scope.codes = server;
            });

            $scope.ok = function(){
                $modalInstance.close($scope.selected_code);
            };

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            };
        });
    </script>
@stop
