@extends('themes/default::layouts.fluid')

@section('base')
<div class="main-content">
    @include('advance::partials.header-sub')
    <div class="container" ng-controller="BudgetManage">
        @include('core::partials.error')
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                </div>
                <div class="box">
                    <div class="box-header border-bottom-off">
                        <span class="title">{{ trans('advance::'.app('router')->currentRouteName()) }}</span>
                    </div>
                    <div class="box-content">

                        <table id="manage-budget" class="responsive dataTable">
                            <thead>
                                <tr>
                                    <th><div>{{ trans('advance::validation.attributes.address_state') }}</div></th>
                                    <th style="width:200px"><div>{{ trans('advance::advance.title.budget_manage_amount') }}</div></th>
                                    <th style="width:200px"><div>{{ trans('advance::advance.title.budget_manage_accountno') }}</div></th>
                                    <th style="width:90px"><div></div></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr ng-repeat="budget in budgets">
                                    <td>[[ budget.state_name ]]</td>
                                    <td>[[ (budget.year_budget_amount || 0) | currency:"" ]]</td>
                                    <td><span editable-text="budget.account_number" e-name="account_number" e-form="rowform" >[[ budget.account_number ]]</span></td>
                                    <td align="middle">
                                        <form editable-form name="rowform" ng-show="rowform.$visible" onbeforesave="budgetUpdate(budget.owner_id,$data)">
                                            <button type="submit" ng-disabled="rowform.$waiting"><i class="fa fa-check"></i></button>
                                            <button type="button" ng-disabled="rowform.$waiting" ng-click="rowform.$cancel()"><i class="fa fa-times"></i></button>
                                        </form>
                                        <div ng-show="!rowform.$visible">
                                            <a ng-click="rowform.$show()"><i class="fa fa-cog"></i></a>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="panel">
                    <div class="panel-heading">
                        Info
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div ng-controller="BudgetDetail">
                                <div class="form-group">
                                    {{ Former::select('budget_year')
                                        ->fromQuery( $budget_years )
                                        ->select('2015')
                                        ->ng_model('budget_year')
                                        ->ui_select2()
                                    }}
                                </div>
                                <div>
                                    <button class="btn btn-primary" ng-click="open()">Add Budget</button>
                                </div>
                            </div>
                        </li>
                        <li class="list-group-item list-group-item-info">
                            <i class="fa fa-dollar"></i>
                            {{ trans('advance::advance.title.budget_manage_overall') }} : [[ budgets | sumByKey:'year_budget_amount' | currency:'RM ' ]]
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@include('advance::admin.budget.partials.detail')
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        asng.controller('BudgetManage', function($scope,Resource){
            var currentYear = (function(){ return new Date()})().getFullYear();
            var budgets = Resource.create(appBase + 'api/v1/advance/account', { year: currentYear });

            budgets.index(function(server){
                $scope.budgets = server;
            });

            $scope.budgetValidateAmount = function(amount){
                if( isNaN( Number(amount) ) ){
                    return 'Amount should be number!';
                }
            };

            $scope.budgetUpdate = function(id,data){
                budgets.params = data;
                budgets.update(id,function(server){
                    // Updated
                });
            };

        });

        asng.controller('BudgetDetail', function($scope,$modal){

            $scope.open = function(){
                var budgetDetailModel = $modal.open({
                    templateUrl: 'BudgetDetailModal.html',
                    controller: 'BudgetDetailModal',
                    size: '',
                    resolve: {
                        scope: function(){
                            return $scope;
                        }
                    }
                });

                budgetDetailModel.result.then(function () {
                    location.reload(true)

                }, function () {
                    //console.log('dismissed');
                });
            }

        });
    </script>
@stop