<script type="text/ng-template" id="BudgetDetailModal.html">
    <div class="modal-header">
        <h3 class="modal-title">Add new budget</h3>
    </div>
    <div class="modal-body">
        <form class="">
            <div class="form-group">
                {{ Former::select('account_owner')
                    ->class('validate[required]')
                    ->fromQuery( Code::category('pmn')->get(), function($model){ return $model->value; }, 'name' )
                    ->ng_model('account_owner')
                    ->ui_select2()
                    ->as_ui_validation()
                }}
            </div>
            <div class="form-group">
                {{ Former::text('budget_amount')
                    ->class('form-control validate[required,custom[number]]')
                    ->ng_model('budget_amount')
                    ->as_ui_validation()
                }}
            </div>
        </form>
    </div>
    <div class="modal-footer">
        <button class="btn btn-primary" ng-click="ok()" ng-disabled="!validation.$error.controls.$valid">OK</button>
        <button class="btn btn-warning" ng-click="cancel()">Cancel</button>
    </div>
</script>

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        asng.controller('BudgetDetailModal', function($scope, $modalInstance, Resource, Validation){
            $scope.validation = Validation;

            $scope.ok = function(){
                var apiUrl = appBase + 'api/v1/advance/accounts/' + $scope.account_owner + '/budgets',
                    budgets = Resource.create(apiUrl);

                budgets.params = {budget_amount: $scope.budget_amount};
                budgets.store(function(server){
                    if(server._status.type == 'success' )
                        $modalInstance.close();
                });
            }

            $scope.cancel = function(){
                $modalInstance.dismiss('cancel');
            }
        });
    </script>
@stop
