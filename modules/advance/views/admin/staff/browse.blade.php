@extends('themes/default::layouts.fluid')

@section('base')
<div class="main-content">
    @include('advance::partials.header-sub')
    <div class="container" ng-controller="StaffManage">
        @include('core::partials.error')
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                </div>
                <div class="box">
                    <div class="box-header border-bottom-off">
                        <span class="title">{{ trans('advance::'.app('router')->currentRouteName()) }}</span>
                    </div>
                    <div class="box-content">
                        <table id="manage-staff" as-ui-table as-ui-table-options="table_options" ao-column-defs="table_columns" class="responsive">
                            <thead>
                            <tr>
                                <th><div>{{ trans('advance::advance.title.staff_center_id') }}</div></th>
                                <th style="width:100px"><div>{{ trans('advance::advance.title.staff_salary_id') }}</div></th>
                                <th style="width:120px"><div>{{ trans('admin::user.label_first_name') }}</div></th>
                                <th style="width:120px"><div>{{ trans('admin::user.label_last_name') }}</div></th>
                                <th style="width:100px"><div>{{ trans('admin::user.label_status') }}</div></th>
                                <th style="width:200px"><div>{{ trans('advance::advance.title.staff_manage_roles') }}</div></th>
                                <th style="width:50px"><div></div></th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="box">
                    <div class="box-header">
                        <span class="title">{{ trans('general.title.search') }}</span>
                    </div>
                    <div class="box-content padded">
                        <form class="fill-up">
                            <!--<div class="form-group">
                                {{ Former::select('user.profile.data.center_id')
                                    ->fromQuery( Code::category('pmn')->get(), 'value', 'name' )
                                    ->ng_model('profile.data.center_id')
                                    ->ui_select2()
                                    ->as_ui_table_filters()
                                }}
                            </div>-->
                            <div class="form-group">
                                {{ Former::text('first_name')
                                    ->class('form-control')
                                    ->ng_model('first_name')
                                    ->as_ui_table_filters()
                                }}
                            </div>
                            <!--<div class="form-group">
                                {{ Former::text('user.profile.data.salary_id')
                                    ->class('form-control')
                                    ->ng_model('profile.idno_ic')
                                    ->as_ui_table_filters()
                                }}
                            </div>-->
                            @if( isset($roles['all']) )
                            <div class="form-group">
                                {{ Former::select('roles')
                                    ->fromQuery( $roles['all'], 'title', 'id' )
                                    ->ng_model('roles')
                                    ->ui_select2()
                                    ->as_ui_table_filters()
                                }}
                            </div>
                            @endif
                        </form>
                        <btn class="btn btn-xs btn-default" ng-click="resetFilter()">{{ trans('general.title.reset') }}</btn>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">

        asng.controller('StaffManage', function($scope){
            $scope.filterDefault = function(){
                $scope.$eval("user.profile.data.center_id = ''");
                $scope.$eval("user.first_name = ''");
                $scope.$eval("user.profile.data.salary_id = ''");
                $scope.status = '';
            };

            $scope.table_options = {
                "sPaginationType": "full_numbers",
                "sDom": '<"">tr<"table-footer"ip>',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": appBase + 'api/v1/advance/staff',
                "aDefaultFilters": []
            };

            $scope.table_columns = [
                {"aTargets":[0],'mData':'profile.data.center_name'},
                {"aTargets":[1],'mData':'profile.data.salary_id'},
                {"aTargets":[2],'mData':'first_name'},
                {"aTargets":[3],'mData':'last_name'},
                {"aTargets":[4],'mData':'status'},
                {"aTargets":[5],'mData':'roles','mRender':function(data, type, full){
                    var roles_text = _.reduce(data, function(mem,role){ return mem + ', ' + role.name },'')
                    roles_text = roles_text.substr(2);

                    return roles_text;
                }},
                {"aTargets":[6],'mData':'url_update','mRender':function(data, type, full){
                    return '<a href="' + data + '"><i class="fa fa-cog"></i></a>';
                }}
            ];

            $scope.resetFilter = function(){
                $scope.filterDefault();
                $scope.asTableReset('manage-staff');
            }
        });

    </script>
@stop