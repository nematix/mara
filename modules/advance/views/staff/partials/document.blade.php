<div class="">
    <div class="box">
        <div id="upload-core" class="box-content">
            <form name="documents" novalidate>
                <table class="table table-normal">
                    <thead>
                    <tr>
                        <td class="icon"></td>
                        <td>{{ trans('advance::cards.section.document.description') }}</td>
                        <td style="width: 70px"></td>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($document_keys as $key)
                            <tr class="{{ isset($documents[$key]) ? 'status-success' : 'status-pending' }}">
                                <td><i class="fa fa-cloud-upload"></i></td>
                                <td>
                                    {{ trans("advance::advance.documents.{$key}") }}
                                    @if( isset($documents[$key]) )
                                    <span class="file-name">
                                        ( {{ $documents[$key]->image->originalFilename() }} )
                                    </span>
                                    @endif
                                    <span class="label label-success" ng-show="detail.meta.documents.{{ $key }}.verified">Verified</span>
                                </td>
                                <td>
                                    <input type="hidden" name="{{ $key }}" ng-model="detail.meta.documents.{{ $key }}.verified" ng-required="true">
                                    <btn class="delete btn btn-xs btn-default" ng-click="detail.meta.documents.{{ $key }}.verified = true" ng-disabled="{{$conditions_disable['form']}}"><i class="fa fa-check"></i></btn>
                                    <btn class="delete btn btn-xs btn-default" ng-click="detail.meta.documents.{{ $key }}.verified = ''" ng-disabled="{{$conditions_disable['form']}}"><i class="fa fa-times"></i></btn>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </form>
        </div>
    </div>
</div>

@if( !empty($documents) )
    <div class="box">
        <div class="box-header">
            <span class="title">{{ trans('advance::cards.section.document.preview') }}</span>
        </div>
        <div class="box-content padded">
            <div id="thumbs">
                @foreach ($documents as $document)
                    @if( $document->image->contentType() )
                    <a href="{{ $document->image->url() }}" style="background-image:url('{{ $document->image->url('thumb') }}');" title="{{ trans("advance::advance.documents.{$document->key}") }}"></a>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endif

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        $(document).ready(function(){
            $('#thumbs a').touchTouch();
        })
    </script>
@stop