@extends('themes/default::layouts.fluid')

@section('stylesheet')
    @parent
    <style>
        .packery {
            /*background: #FDD;
            background: hsla(45, 100%, 40%, 0.2);*/
        }

        /* clearfix */
        .packery:after {
            content: ' ';
            display: block;
            clear: both;
        }

        .item,
        .grid-sizer {
            width: 30%;
        }

        .item {
            height: 110px;
            /*float: left;
            background: #C09;
            border: 4px solid #333;
            border-color: hsla(0, 0%, 0%, 0.3);*/
        }

        .item.w2 { width:  62%; }
        .item.h2 { height: 220px; }
        .item.h4 { height: 440px; }
        .item.w4 { width: 120%; }

        .item.bg-success { background: #b6dd69; color: #779148; }
        .item.bg-info { background: #dee8f2; color: #3c6a95; }
        .item.bg-warning { background: #f8e9bb; color: #a37f11; }
        .item.bg-error { background: #ecb5ac; color: #c75d5d; }

        .item.bg-info .stats-label { color: #3c6a95; }
        .item.bg-warning .stats-label { color: #a37f11; }
        .item.bg-success .stats-label { color: #779148; }
        .item.bg-error .stats-label { color: #c75d5d; }
    </style>
@stop

@section('base')
<div class="main-content">
    @include('advance::partials.header-sub')
    <div class="container " ng-controller="controllerStatistic">
        @include('core::partials.error')
        <div class="row">

            <div class="col-md-6">
                <div class="box">
                    <div class="box-content">
                        <table class="table table-normal">
                            <thead>
                                <tr>
                                    <td>{{ trans('advance::advance.title.application_status') }}</td>
                                    <td>{{ trans('advance::advance.title.information') }}</td>
                                    <td>{{ trans('advance::advance.title.total') }}</td>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach( $statistics as $statistic )
                                <tr>
                                    <td style="width: 80px;"><span class="label label-{{ $statistic['status_label'] }}">{{ $statistic['status'] }}</span></td>
                                    <td>{{ $statistic['title'] }}</td>
                                    <td>{{ $statistic['value'] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="packery js-packery" data-packery-options='{ "columnWidth": ".grid-sizer", "itemSelector": ".item", "gutter": 10 }'>
                    <div class="grid-sizer"></div>
                    @foreach( $statistics as $statistic )
                    <div class="box item {{ $statistic['class'] }}" style="margin-bottom: 0;">
                        <div class="box-content padded">
                            <div class="dashboard-stats">
                                <ul class="list-inline">
                                    <li class="glyph"><i class="fa fa-2x fa-user"></i></li>
                                    <li class="count">{{ $statistic['value'] }}</li>
                                </ul>
                                <div class="stats-label">{{ $statistic['title'] }}</div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIPeople = appBase + 'api/v1/peoples/';
        var urlAPIDocument = appBase + 'api/v1/documents/';

        function controllerStatistic($scope,Rest,Validation){

        }
    </script>
@stop