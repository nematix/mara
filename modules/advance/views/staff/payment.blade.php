@extends('themes/default::layouts.fluid')

@section('stylesheet')
    @parent
    <style>
        form.plain textarea,
        form.plain input[type="text"],
        form.plain input[type="password"],
        form.plain input[type="datetime"],
        form.plain input[type="datetime-local"],
        form.plain input[type="date"],
        form.plain input[type="month"],
        form.plain input[type="time"],
        form.plain input[type="week"],
        form.plain input[type="number"],
        form.plain input[type="email"],
        form.plain input[type="url"],
        form.plain input[type="search"],
        form.plain input[type="tel"],
        form.plain input[type="color"],
        form.plain select,
        form.plain .uneditable-input,
        form.plain .select2-container .select2-choice,
        form.plain .select2-container.select2-container-disabled .select2-choice {
            background: none;
            border-width: 0 0 1px 0;
            border-style: dotted;
        }

        form.plain .select2-container.select2-container-disabled .select2-choice div {
            display: none;
        }
    </style>
@stop

@section('base')
<div class="main-content">
    <div class="container" ng-controller="controllerDetail">
        @include('core::partials.error')
        <div class="row">
            <div class="area-top clearfix">
                <div class="pull-left header">
                    <h3 class="title">
                        <i class="fa fa-tasks"></i> {{ $detail->description }}
                    </h3>
                    <h5>
                        <span class="visible-lg">
                            <span class="label label-{{ $record->status['label'] }}">{{ $record->status['title'] }}</span>
                            <span>{{ trans('general.title.created_at').' '.$record->created_when }}, </span>
                            <span>{{ trans('general.title.updated_at').' '.$record->updated_at->formatLocalized('%A, %d %B %Y') }}</span>
                        </span>
                    </h5>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading">
                        {{ trans('advance::cards.title.application') }}
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <form name="info" class="plain form-horizontal fill-up" novalidate>
                            @include('advance::cards.profile')
                            </form>
                        </li>
                        <li class="list-group-item">
                            <form name="application" class="plain form-horizontal fill-up" novalidate>
                                @include('advance::cards.application')
                            </form>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                @include('advance::cards.voucher')
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIPeople = appBase + 'api/v1/peoples/';
        var urlAPIDocument = appBase + 'api/v1/documents/';

        function controllerDetail($scope,Rest,Validation){
            /** Disable validation for current view */
            Validation.config.validate = false;

            /** Record params */
            $scope.detail_params = {model:'details', id:'{{ (isset($record) ? $record->uuid : "") }}'};

            /** Getting detail model */
            $scope.detail = Rest.get($scope.detail_params);
        }
    </script>
@stop