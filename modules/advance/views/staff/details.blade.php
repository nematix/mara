@extends('themes/default::layouts.fluid')

@section('base')
<div class="main-content">
    @include('advance::partials.header-sub')
    <div class="container" ng-controller="controllerDetails">
        @include('core::partials.error')
        <div class="row">
            <div class="col-lg-9">
                <div class="row">
                </div>
                <div class="box">
                    <div class="box-header border-bottom-off">
                        <span class="title">{{ trans('advance::advance.title.student_manage') }}</span>
                    </div>
                    <div class="box-content">
                        <table id="details-manage" as-ui-table as-ui-table-options="table_options" ao-column-defs="table_columns" class="responsive">
                            <thead>
                            <tr>
                                <th style="width:150px"><div>{{ trans('advance::advance.title.application_date') }}</div></th>
                                <th style="width:150px"><div>{{ trans('advance::validation.attributes.idno_ic') }}</div></th>
                                <th><div>{{ trans('advance::advance.title.student_name') }}</div></th>
                                <th style="width:100px"><div>{{ trans('advance::advance.title.application_status') }}</div></th>
                                <th style="width:40px">{{ trans('advance::advance.title.action') }}</th>
                            </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="box">
                    <div class="box-header">
                        <span class="title">{{ trans('general.title.search') }}</span>
                    </div>
                    <div class="box-content padded">
                        <form class="fill-up">
                            <div class="form-group">
                                {{ Former::select('application_location')
                                    ->fromQuery( Code::category('pmn')->get(), function($model){return $model->value;}, 'name' )
                                    ->disabled(!User::find($user->id)->hasRole('Pentadbir Sistem'))
                                    ->ng_model('application_location')
                                    ->ui_select2()
                                    ->as_ui_table_filters()
                                }}
                            </div>
                            <div class="form-group">
                                {{ Former::text('first_name')
                                    ->class('form-control')
                                    ->ng_model('user.first_name')
                                    ->as_ui_table_filters()
                                }}
                            </div>
                            <div class="form-group">
                                {{ Former::text('idno_ic')
                                    ->class('form-control')
                                    ->ng_model('user.profile.idno_ic')
                                    ->as_ui_table_filters()
                                }}
                            </div>
                            @if( count($status_role) > 0 )
                            <div class="form-group">
                                {{ Former::select('status')
                                    ->fromQuery( $status_role, 'title', 'id' )
                                    ->ng_model('status')
                                    ->ui_select2()
                                    ->as_ui_table_filters()
                                }}
                            </div>
                            @endif
                        </form>
                        <btn class="btn btn-xs btn-default" ng-click="resetFilter()">{{ trans('general.title.reset') }}</btn>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        function controllerDetails($scope){
            //i: Initial filter value
            $scope.filterDefault = function(){
                $scope.application_location = '{{  User::find($user->id)->profile->data->center_id }}';
                $scope.$eval("user.first_name = ''");
                $scope.$eval("user.profile.idno_ic = ''");
                $scope.status = '{{ array_values($status_role)[0]["id"] }}';
            };
            $scope.filterDefault();

            $scope.table_options = {
                "sPaginationType": "full_numbers",
                "sDom": '<"">tr<"table-footer"ip>',
                "bProcessing": true,
                "bServerSide": true,
                "sAjaxSource": appBase + 'api/v1/details',
                "aDefaultFilters": ['status','application_location']
            };

            $scope.table_columns = [
                {"aTargets":[0],'mData':'created_at','sType':'date'},
                {"aTargets":[1],'mData':'user.profile.idno_ic'},
                {"aTargets":[2],'mData':'user.full_name'},
                {"aTargets":[3],'mData':'status','mRender':function(data, type, full){
                    return '<span class="label label-' + data['label'] + '">' + data['title'] + '</span>';
                }},
                {"aTargets":[4],'mData':'url_update','mRender':function(data, type, full){
                    return '<a href="' + data + '{{ isset($mode) ? "/{$mode}" : '' }}"><i class="fa fa-cog"></i></a>';
                }}
            ];

            $scope.resetFilter = function(){
                $scope.filterDefault();
                $scope.asTableReset('details-manage');
            }
        }
    </script>
@stop