@extends('themes/default::layouts.fluid')

@section('base')
<div class="main-content">
    <div class="container" ng-controller="controllerDetail">
        @include('core::partials.error')
        <div class="row">
            <div class="area-top clearfix">
                <div class="pull-left header">
                    <h3 class="title">
                        <i class="fa fa-tasks"></i> {{ $detail->description }}
                    </h3>
                    <h5>
                        <span class="visible-lg">
                            <span class="label label-{{ $record->status['label'] }}">{{ $record->status['title'] }}</span>
                            <span>{{ trans('general.title.created_at').' '.$record->created_when }}, </span>
                            <span>{{ trans('general.title.updated_at').' '.$record->updated_at->formatLocalized('%A, %d %B %Y') }}</span>
                        </span>
                    </h5>
                </div>
            </div>
        </div>

        <div class="row padded">
            <ul class="row nav nav-tabs nav-tabs-left" style="padding-left:15px;">
                <li class="active"><a href="#home" data-toggle="tab"><i class="fa fa-home"></i> <span>{{ trans('advance::advance.title.detail_form') }}</span></a></li>
                <li><a href="#documents" data-toggle="tab"><i class="fa fa-file"></i> <span>{{ trans('advance::cards.title.document') }}</span></a></li>
            </ul>
        </div>
        <div class="row">
            <div class="col-md-8">
                <div class="tab-content">
                    <!-- ------------------------------------------------
                        Tab Home
                    -->
                    <div id="home" class="tab-pane active">
                        @include('advance::partials.box')
                    </div>
                    <!-- ------------------------------------------------
                        Tab Document
                    -->
                    <div id="documents" class="tab-pane">
                        @include('advance::staff.partials.document')
                    </div>
                </div>
                @include('advance::partials.compose')
            </div>
            <div class="col-md-4">
                @if( $record->status['raw'] == 4 )
                    @include('advance::cards.approval')
                @endif
                @if( $record->status['raw'] == 7 )
                    @include('advance::cards.documents')
                @endif
                @include('advance::partials.conversations')
            </div>
        </div>
    </div>
</div>
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIPeople = appBase + 'api/v1/peoples/';
        var urlAPIDocument = appBase + 'api/v1/documents/';

        function controllerDetail($scope,Rest,Validation){
            //i: Variables
            $scope.validation = Validation;
            $scope.detail_params = {model:'details', id:'{{ (isset($record) ? $record->uuid : "") }}'};

            //i: Getting detail model
            $scope.detail = Rest.get($scope.detail_params);

            //i: Attach guardian detail
            $scope.getGuardian = function(guardianId){
                $scope.detail.guardian_id = guardianId;
                $scope.detail.guardian = Rest.get({model:'peoples', id:guardianId});
            }

            //i: Attach guarantor detail
            $scope.getGuarantor = function(guarantorId){
                $scope.detail.guarantor_id = guarantorId;
                $scope.detail.guarantor = Rest.get({model:'peoples', id:guarantorId});
            }

            $scope.$watchCollection($scope.validation.$error.cards, function(){
                $scope.boxes = angular.copy($scope.validation.$error.cards);
            })
        }
    </script>
@stop