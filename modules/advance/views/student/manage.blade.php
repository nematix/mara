@extends('themes/default::layouts.fluid')

@section('base')
<div class="main-content">
    @include('advance::partials.header-sub')

    <div class="container">
        @include('core::partials.error')

        @if( !empty($records) )
        <div class="row">
            <div class="col-lg-9">
                @foreach( $records as $record )
                <div class="box">
                    <div class="box-content col-fixed padded bg-{{ $record->status['label'] }}">
                        <div class="col-fixed-30 no-padding">
                            <img src="data:image/png;base64,{{ Barcode::getBarcodePNG($detail->name.'://'.$record->uuid,'QRCODE',2,2) }}">
                        </div>
                        <div class="content">
                            <span class="title"><a href="{{ $record->url_update }}"><i class="fa fa-link"></i> {{ $detail->config->detail_title }}</a></span>
                            <div>Application ID {{ $record->uuid }}</div>
                            <div>Last update at {{ $record->updated_at->toDayDateTimeString() }}</div>
                        </div>
                        <div class="col-fixed-100">
                            <span class="time pull-right" title="Created at {{ $record->created_at->toDayDateTimeString() }}"><i class="fa fa-clock-o"></i> {{ $record->created_when }}</span>
                            <span class="label label-{{ $record->status['label'] }} pull-right">{{ $record->status['title'] }}</span>
                        </div>
                    </div>
                </div>
                @if( $record->status['raw'] == 7 )
                    @include('advance::cards.documents')
                @endif
                @endforeach

                @if( count($records) == 0 )
                <div class="box">
                    <div class="box-content padded">
                        Sila klik untuk <a href="{{ url('application/advance/new') }}"><span class="label label-info">{{ trans('advance::advance.title.application_new') }}</span></a>
                    </div>
                </div>
                @endif
            </div>

            <div class="col-lg-3">
                <div class="box">
                    <div class="box-header">
                        <div class="title">{{ trans('advance::advance.title.action') }}</div>
                    </div>
                    <div class="box-content padded">
                        <ul class="list-unstyled" style="margin:0;">
                            <li><a href="{{ url('application/advance/new') }}" {{ count($records) > 0 ? 'hidden' : '' }}><i class="fa fa-plus"></i> {{ trans('advance::advance.title.application_new') }}</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif

    </div>
</div>
@stop