@extends('themes/default::layouts.fluid')

@section('base')
<div class="main-content">
    <div class="container" ng-controller="controllerDetail">
        @include('core::partials.error')

        <div class="row">
            <div class="area-top clearfix">
                <div class="pull-left header">
                    <h3 class="title">
                        <i class="fa fa-tasks"></i> {{ $detail->description }}
                    </h3>
                    <h5>
                        <span class="visible-lg">
                            @if( isset($record) )
                            <span class="label label-{{ $record->status['label'] }}">{{ $record->status['title'] }}</span>
                            <span>{{ trans('general.title.created_at').' '.$record->created_when }}, </span>
                            <span>{{ trans('general.title.updated_at').' '.$record->updated_at->formatLocalized('%A, %d %B %Y') }}</span>
                            @endif
                        </span>
                    </h5>
                </div>
            </div>
        </div>

        @if( isset($record) )
            <div class="row padded">
                <ul class="nav nav-tabs nav-tabs-left">
                    <li class="active"><a href="#home" data-toggle="tab"><i class="fa fa-home"></i> <span>{{ trans('advance::advance.title.detail_form') }}</span></a></li>
                    <li><a href="#documents" data-toggle="tab"><i class="fa fa-file"></i> <span>{{ trans('advance::cards.title.document') }}</span></a></li>
                </ul>
            </div>
            <div class="row">
                <div class="tab-content">
                    <!-- ------------------------------------------------
                        Tab Home
                    -->
                    <div id="home" class="tab-pane active">
                        @include('advance::partials.steps')
                    </div>
                    <!-- ------------------------------------------------
                        Tab Document
                    -->
                    <div id="documents" class="tab-pane">
                        @include('advance::partials.document')
                    </div>
                </div>

            </div>
        @endif
    </div>
</div>
@stop

@section('javascript')
    @parent
    <script language="JavaScript" type="text/javascript">
        var urlAPIPeople = appBase + 'api/v1/peoples/';
        var curScope = {};
        var curDetailStatus = {{ $record->status['id'] or 0 }}

        asng.controller('controllerDetail',['$scope','$http','$compile','Rest','Validation',function($scope,$http,$compile,Rest,Validation){
            /** Vars */
            var detailParams = {model:'details', id:'{{ $record->uuid }}'};

            /** Globals */
            var curScope = $scope;

            /** Validation services */
            $scope.validation = Validation;

            /** Getting detail model */
            $scope.detail = Rest.get(detailParams);

            /** Update detail */
            $scope.updateDetail = function(eventSuccess){
                $scope.detail.$update(detailParams).then(function(){
                    if( typeof eventSuccess == 'function' ) eventSuccess();
                });
            };

            /** Button : Update */
            $scope.detailUpdate = function(){
                curScope.updateDetail();
            };


            /** Button : Submit */
            $scope.detailSubmit = function(){
                $scope.detail.status.id = 1;
                $scope.updateDetail(function(){
                    location.reload(true);
                });
            };

            /** On iCheck checked */
            $scope.$on('atlantis.ui.icheck.checked', function(e,elem){
                if( $(elem.target).attr('name') == 'final' ){
                    var btnSubmit = $('#advance-steps').find('#btn-submit');

                    /** Show/hide submit button base on validation condition */
                    if( Validation.$error.controls.$count <= 1 && $scope.detail.status.id == 0){
                        btnSubmit.removeClass('hidden');
                    }else{
                        btnSubmit.addClass('hidden');
                    }
                }
            });

            /** On iCheck unchecked */
            $scope.$on('atlantis.ui.icheck.unchecked', function(e,elem){
                if( $(elem.target).attr('name') == 'final' ){
                    $('#advance-steps').find('#btn-submit').addClass('hidden');
                }
            });

            /** Steps validation callback */
            $scope.stepCallback = function(e,elem){
                /** Custom validation for final card */
                if( elem.attr('as-ui-card') == 'final' ){
                    /** Cancel further step processing */
                    e.preventDefault();

                    /** If all control are validated then, true */
                    if( Validation.$error.controls.$count == 0 ){
                        return true;
                    }else{
                        return 'warning'
                    }
                }
            };

            /** Final validation process */
            $scope.$watch('validation.$error.controls.$valid', function(){
                if($scope.detail.status){
                    /** If application status is not new, auto checked */
                    if( $scope.detail.status.raw >= 3 ){
                        $('#final').iCheck('check');
                    }else{
                        $('#final').iCheck('check');
                        $('#final').iCheck('enable');
                    }
                }
            });
        }]);
    </script>
@stop