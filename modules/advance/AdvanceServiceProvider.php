<?php namespace Modules\Advance;

use Atlantis\Core\Module\ServiceProviderFactory;


class AdvanceServiceProvider extends ServiceProviderFactory {
    protected $title = 'Permohonan Pendahuluan Pinjaman Pelajaran';

    /**
     * Module Register
     *
     */
    public function register(){
        parent::register('advance');

        $this->registerServiceHelpers();
        $this->registerServiceContextReaction();
        $this->registerServiceValidator();
        $this->registerServiceApi();
    }

    /**
     * Module boot
     *
     */
    public function boot(){
        parent::boot('advance');

        /** Set proper ms locale */
        if($this->app['config']->get('app.locale') == 'ms'){
            setlocale(LC_ALL, "ms_MY.UTF-8");
        }
    }


    /**
     * Registering context reaction
     *
     */
    public function registerServiceContextReaction(){
        /** Registering Reaction : StatusDetail */
        $this->app['context']->extendReaction('detail_status',function(){
            /** Get arguments */
            list($model,$attribute,$original,$parameters) = func_get_args();

            /** Check for attribute */
            if($attribute != $parameters->attribute) return null;

            /** Get statuses collection */
            $statuses = $model->workflows('resource.status')->all();

            /** Process all status with count */
            $pmn_id = \User::find(\Sentry::getUser()->id)->profile->data->center_id;
            foreach($statuses as $key => &$status){
                $status['total'] = $model->records()->where('status',$key)->where('application_location',$pmn_id)->count();
            }

            /** Compose status structure */
            $statuses = [
                'all'   => $statuses,
                'context'  => array_only($statuses,$parameters->value)
            ];

            return $statuses;
        });
    }


    /**
     * Registering helpers
     *
     */
    public function registerServiceHelpers(){
        $this->app['atlantis.helpers']->extend('advance',new AdvanceServiceHelpers());
    }


    /**
     * Registering validator
     *
     */
    public function registerServiceValidator(){
        $this->app['validator']->extend('account_series', function($attribute, $value, $parameters){
            $account = \Modules\Advance\Account::find($parameters[0]);

            /** If series value lesser and same then current, return false */
            if($value <= $account->account_number) return false;

            return true;
        });
    }


    /**
     * Registering API
     *
     */
    public function registerServiceApi(){
        $this->app['atlantis.module']->extend('advance.api','Modules\Advance\Api\AdvanceApiServiceProvider');
    }

}