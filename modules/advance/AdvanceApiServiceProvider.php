<?php namespace Modules\Advance\Api;

use Atlantis\Api\ServiceProviderFactory;


class AdvanceApiServiceProvider extends ServiceProviderFactory {

    /** @var string API Description */
    protected $title = 'Advance API';

    /** @var string Api name */
    protected $name = 'advance';

}