<?php namespace Modules\Mara;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Atlantis\Core\Client\Facades\Client;
use Atlantis\Core\Module\ServiceProviderFactory as ServiceProvider;


class MaraServiceProvider extends ServiceProvider {
    protected $title = 'Mara Framework';

    public function register(){
        parent::register('mara');

        #i: Register your service if any

    }

    public function boot(){
        parent::boot('mara');

        #i: Misc task; register menu, migration and etc
        App::down(function()
        {
            Client::javascript()->put(array(
                'appBase' => url('/') . '/'
            ));

            $theme = app('config')->get('core::app.theme.current');
            app('atlantis.theme')->load($theme);

            $view = app('view')->make('core::layouts.common');
            $view->content = app('view')->make('mara::page.maintenance');

            return Response::make($view, 503);
        });
    }
}