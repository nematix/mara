@extends('themes/default::layouts.page')

@section('menu-top')
@stop

@section('page')
    <div class="row">
        <div class="col-md-12">
            <h3>Menaiktaraf Sistem</h3><hr>

            <div class="alert alert-warning">
                <div class="row">
                    <div class="col-md-12 col-lg-12">
                    <i class="fa fa-wrench fa-lg fa-5x pull-left"></i> Adalah dimaklumkan permohonan pendahuluan pinjaman pelajaran melalui Sistem EPendahuluan masih belum dibuka.
                    Sebarang makluman terkini akan disampaikan dari semasa ke semasa.
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('menu-bottom')
@stop