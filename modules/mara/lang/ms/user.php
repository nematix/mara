<?php return [

    'registered_text_success'           => 'Pendaftaran anda berjaya, e-mel pengaktifan telah dihantar ke <strong>:email</strong> dan sila klik pautan yang disediakan untuk mengaktifkan akaun anda.'

];