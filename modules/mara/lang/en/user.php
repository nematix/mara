<?php return [

    'registered_text_success'           => 'Your registration is successful, activation email have been sent to <strong>:email</strong> and please click the link provided to activate you account.'

];