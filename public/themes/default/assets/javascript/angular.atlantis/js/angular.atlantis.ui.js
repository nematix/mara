/*
 * File:		angular.atlantis.form.js
 * Version:    	1.0
 * Package: 	Component\Atlantis
 * Module: 		Atlantis Form
 * Function:		  
 * Author:     	Azri Jamil | azri{at}nematix.com
 * Info:       	system.nematix.com/atlantis
 * 
 * Copyright 2012 Nematix Technology, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license.
 * 
 * This source file is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 * 
 * 
 */



var atlantisUi = angular.module('atlantis.ui',[]);


/***************************************************************************************************
 * Directive : as-ui-button
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiButton', function($rootScope,$injector) {
    return {
        scope: {},
        link: function (scope, element, attrs) {
            element.on('click', function(e){
                //i: Check for UI Progress attribute
                if(attrs.asUiProgress != undefined){
                    //i: Inject Progress service
                    var progress = $injector.get('Progress');

                    //:
                    if( progress ){
                        var control = {
                            target: element,
                            type: attrs.asUiProgress
                        };

                        //i: Start progress
                        progress.actions[attrs.asUiProgress].start(control);

                        //i: Push control to Progress service
                        progress.controls.push(control);
                    }
                };
            });
        }
    }
});


/***************************************************************************************************
 * Directive : as-ui-icheck
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiIcheck', function($timeout){
    return {
        require: 'ngModel',
        link: function($scope, element, $attrs, ngModel) {
            return $timeout(function() {
                var value = $attrs['value'];

                $scope.$watch($attrs['ngModel'], function(){
                    $(element).iCheck('update');
                })

                return $(element).iCheck({
                    checkboxClass: 'icheckbox_flat-aero',
                    radioClass: 'iradio_flat-aero'

                }).on('ifChanged', function(event) {
                    if ($(element).attr('type') === 'checkbox' && $attrs['ngModel']) {
                        //$scope.$apply(function() {
                            return ngModel.$setViewValue(event.target.checked);
                        //});
                    }
                    if ($(element).attr('type') === 'radio' && $attrs['ngModel']) {
                        //return $scope.$apply(function() {
                            return ngModel.$setViewValue(value);
                        //});
                    }
                    $scope.$emit('atlantis.ui.icheck.changed',event,element)

                }).on('ifChecked', function(event){
                    $scope.$emit('atlantis.ui.icheck.checked',event,element)

                }).on('ifUnchecked', function(event){
                    $scope.$emit('atlantis.ui.icheck.unchecked',event,element)

                });
            });
        }
    };
});


/***************************************************************************************************
 * Directive : ui-datepicker
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiDatepicker', function($filter) {
    return {
        restrict: 'A',
        require : 'ngModel',
        link : function (scope, element, attrs, ngModelCtrl) {
            $(function(){
                var options = {
                    todayBtn: false,
                    changeYear: true,
                    constrainInput: true,
                    dateFormat: 'yy-mm-dd'
                };

                /** Options override if set */
                if( attrs.asUiDatepickerOptions ){
                    angular.extend( options, scope.$eval(attrs.asUiDatepickerOptions) );
                }

                /** Display date format from attribute if set */
                if( attrs.asUiDatepicker.toLowerCase().match(/dd|mm|yyyy/g)){
                    options.dateFormat = attrs.asUiDatepicker;
                }

                if( options.altField != undefined ){
                    /**
                     * AltField/AltFormat date sync
                     * Since asUiValidation were using validationEngine, date validation must be using ISO8601
                     * which is default format for model value.
                     */
                    var altElement = $(options.altField);
                    options.onSelect = function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                        altElement.trigger('change');
                    };

                    /** Propagate change event in alt-element */
                    element.on('change', function(e){
                        element.datepicker('setDate', $(e.currentTarget).val());
                        altElement.trigger('change');
                    });

                    /** Watch for alternate element model */
                    scope.$watch(altElement.attr('ng-model'), function(newValue){
                        if(newValue != undefined){
                            var modelDate = moment(newValue,'YYYY-MM-DD');
                            element.val(modelDate.format('DD-MM-YYYY'));
                        }
                    });

                } else {
                    /** View value from model on select event */
                    options.onSelect = function (date) {
                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    };
                }

                /** Instantiate datepicker */
                element.datepicker(options);

                /** Model to view value */
                ngModelCtrl.$formatters.push(function(value){
                    var modelDate = moment(value,'YYYY-MM-DD');

                    if( modelDate.isValid() )
                        return modelDate.format('DD-MM-YYYY');
                });

                /** View value to model */
                ngModelCtrl.$parsers.push(function(value){
                    var modelDate = moment(value,'DD-MM-YYYY');

                    if( modelDate.isValid() )
                        return modelDate.format('YYYY-MM-DD');
                    else
                        return value;
                });

            });
        }
    }
});


/***************************************************************************************************
 * Directive :
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiTable', function($rootScope) {
    return {
        scope: true,
        link: function (scope, element, attrs){
            var options = {
                "bLengthChange": false,
                "bSort": false,
                "bAutoWidth": false,
                "bDestroy": true
            };

            //[i] Extend default options with user assigned
            if( attrs.asUiTableOptions != undefined ){
                angular.extend( options, scope.$eval(attrs.asUiTableOptions) );
            }

            //[i] Default filters
            scope.filters = options['aDefaultFilters'];

            //[i] Columns definition
            var explicitColumns = [];
            element.find('th').each(function(index, elem) {
                explicitColumns.push($(elem).text());
            });
            if (explicitColumns.length > 0) {
                options["aoColumns"] = explicitColumns;
            } else if (attrs.aoColumns) {
                options["aoColumns"] = scope.$eval(attrs.aoColumns);
            }
            if (attrs.aoColumnDefs) {
                options["aoColumnDefs"] = scope.$eval(attrs.aoColumnDefs);
            }

            //[i] Row callback
            if (attrs.fnRowCallback) {
                options["fnRowCallback"] = scope.$eval(attrs.fnRowCallback);
            }

            //[i] Data transmission callback
            if (attrs.fnServerData) {
                options["fnServerData"] = scope.$eval(attrs.fnServerData);
            }else{
                //[i] Get filters
                scope.$watch('filters', function(value) {
                    if( dataTable == undefined ) return;

                    dataTable.fnSettings().fnServerData = function( sSource, aoData, fnCallback ){
                        scope.fnFiltering(value, sSource,aoData,fnCallback);
                    }
                }, true);
            }

            //[i] Default filtering
            options['fnServerData'] = function( sSource, aoData, fnCallback ){
                scope.fnFiltering(scope.filters,sSource,aoData,fnCallback);
            };

            //[i] Filtering function
            scope.fnFiltering = function(aoValue,sSource,aoData,fnCallback){
                $.each(aoValue, function(i,element_name){
                    //var element = $('#'+element_name.replace(/[.]/g,'\\.'));
                    var element_value = scope.$eval(element_name);
                    if( !$.isEmptyObject(element_value) ) aoData.push( {'name': 'search['+element_name+']', 'value':element_value } );
                });

                //[i] Execute get request and passback to fnCallback on data return
                $.getJSON( sSource, aoData, function (json) {
                    fnCallback(json)
                });
            };

            /** Instantiate DataTables */
            if( !$rootScope.asTables ) $rootScope.asTables = [];

            var dataTable = element.dataTable(options);
            $rootScope.asTables[element.attr('id')] = dataTable;

            //[i] Watch for aaData change
            scope.$watch(attrs.aaData, function(value) {
                var val = value || null;

                if(val == null){
                    return false;

                }else if (val.$promise) {
                    val.$promise.then(function(data){
                        dataTable.fnClearTable();
                        dataTable.fnAddData(scope.$eval(data));
                    });

                }else if( typeof val == 'object' ){
                    dataTable.fnClearTable();
                    dataTable.fnAddData(scope.$eval(attrs.aaData));
                }
            });

            //[i] Event : Table refresh
            $rootScope.asTableRefresh = function(value){
                $rootScope.asTables[value].fnFilter('');
            };

            //[i] Event : Table reset
            $rootScope.asTableReset = function(value){
                $.each(scope.filters, function(i,element_name){
                    var element = $(':input#'+element_name.replace(/[.]/g,'\\.'));
                    if( !element.attr('ng-disabled') || !element.attr('disabled') ) element.val('');
                });
                $rootScope.asTables[value].fnFilter('');
            }
        }
    }
});


/***************************************************************************************************
 * Directive : as-ui-table-filters
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiTableFilters', function($rootScope) {
    return {
        scope: {},
        require: 'ngModel',
        restrict: 'A',
        link: function (scope, element, attrs) {
            var current_table = {};

            //[i] Table name is not set, get first instance in table
            if(attrs.asUiTableFilters == 'true'){
                var table_key = Object.keys($rootScope.asTables)[0];
                current_table = $rootScope.asTables[table_key];

            //[i] Table name is set, get table instance
            }else if(attrs.asUiTableFilters.length > 0){
                current_table = $rootScope.asTables[attrs.asUiTableFilters];
            }

            //[i] Push filter to table
            if( current_table != undefined ){
                var table_scope = angular.element(current_table).scope();
                var element_name = attrs.ngModel;

                if(table_scope == undefined) return;

                //[i] Add to filters list is stil not available
                if( table_scope.filters.indexOf( element_name ) != undefined ) table_scope.filters.push( element_name );

                element.on('change',function(){
                    current_table.fnFilter('');
                });
            }
        }
    }
});


/***************************************************************************************************
 * Directive : as-ui-editor-wysihtml
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiEditorWysihtml', function() {
    return {
        //template: '<textarea ng-model="model" rows="5" class="wysiwyg"></textarea>',
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, element, attrs, ngModel) {
            var textarea = element;

            scope.$watch('model', function(val) {
                textarea.siblings("iframe").contents().find("body").html(val);
            });

            textarea.wysihtml5({
                "font-styles": true,
                "emphasis": true,
                "lists": true,
                "html": false,
                "link": false,
                "image": false,
                "color": false,
                stylesheets: false,
                events: {
                    "blur": function() {
                        scope.$apply(function() {
                            var html = textarea.siblings("iframe").contents().find("body").html();
                            return ngModel.$setViewValue(html);
                        });
                    }
                }
            });
        }
    };
})


/***************************************************************************************************
 * Directive : as-ui-card
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiCard', ['Validation', function(Validation) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs){
            /** Getting control under card with validation */
            var controls = element.find('input[as-ui-validation], select[as-ui-validation]');

            /** Preparing all found control */
            angular.forEach(controls, function(control){
                /** Getting model controller of control's */
                var modelController = angular.element(control).data('$ngModelController');

                /** Extending control model controller */
                angular.extend(modelController,{card:attrs.asUiCard});
            });

            /** Fire event */
            scope.$emit('atlantis.ui.card.init',attrs.asUiCard)
        }
    }
}]);


/***************************************************************************************************
 * Directive : as-ui-validation
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiValidation', ['ui.config','Validation', function(uiConfig,Validation){
    return {
        require: 'ngModel',
        restrict: 'A',
        link: function(scope, element, attrs, ngModel){
            var config = uiConfig.validation;

            /** Global validation config */
            if(!Validation.config.validate) return;

            /** Check for engine */
            if($.validationEngine == undefined) return $.error('ValidationEngine instance not loaded!');

            /** VE config */
            if( $.validationEngine.defaults.atlantis == undefined ){
                angular.extend(config,{atlantis:true});
                angular.extend($.validationEngine.defaults,config);
            }

            /** View -> Model */
            ngModel.$parsers.unshift(function(viewValue){
                var valid = element.validationEngine('validate');
                ngModel.$setValidity('asUiValidation', !valid);

                /** Event : Validated */
                scope.$emit('atlantis.ui.model.parsed',element);

                return viewValue;
            });

            /** Model -> View */
            ngModel.$formatters.push(function(modelValue){
                /** Check for SELECT element */
                if( element.prop('tagName') == 'SELECT' ){
                    element.val(modelValue);
                    var valid = element.validationEngine('validate');
                    ngModel.$setValidity('asUiValidation', !valid);

                    /** Event : Validated */
                    scope.$emit('atlantis.ui.model.formatted',element);
                }

                return modelValue;
            });

            /** Render */
            ngModel.$render = function(){
                /** Setting value to element */
                element.val(ngModel.$viewValue);

                /** Validate element */
                var valid = element.validationEngine('validate');

                /**
                 * Set validity, validationEngine will return true if the element is invalid,
                 * we need to invert the boolean value for model validation.
                 */
                ngModel.$setValidity('asUiValidation', !valid);

                /** Event : Validated */
                scope.$emit('atlantis.ui.model.rendered',element)
            };

            /** Event : Validation Init */
            scope.$emit('atlantis.ui.validation.init',element)
        }
    }
}]);


/***************************************************************************************************
 * Directive : as-ui-wizard-step
 *
 ***************************************************************************************************/
atlantisUi.directive('asUiWizardStep', ['ui.config','Validation','Debounce', function(uiConfig,Validation,Debounce){
    return {
        restrict: 'A',
        scope: {
            asUiCallbackValidation: '&'
        },
        link: function(scope, element, attrs){
            /** pSteps config */
            var config = {
                validation_rule: function() {
                    var step_name = this.attr('as-ui-card');

                    /** Event callback with override */
                    if( typeof scope.asUiCallbackValidation == 'function') {
                        var event = jQuery.Event('validation');
                        var result = scope.asUiCallbackValidation({$event:event,element:this})

                        if( event.isDefaultPrevented() ) return result;
                    }

                    if( Validation.$error.cards[step_name].$valid ){
                        return true;

                    /** Return warning on else */
                    } else {
                        return 'warning';
                    }
                }
            };

            /** Get validation service */
            scope.validation = Validation;

            /** Extending config */
            angular.extend(config,uiConfig.wizardstep);

            /** Instantiate steps */
            $(element).psteps(config);

            scope.triggerValidatePsteps = function(){
                $(element).trigger('validate.psteps');
                return true;
            };

            scope.traverse = Debounce(scope.triggerValidatePsteps, 700, false);

            /** Steps validation */
            scope.$watch('validation.$error.controls.$count',function(){
                $(element).trigger('validate.psteps');
            });

            scope.$root.$on('atlantis.ui.model.rendered',function(e,elem){
                scope.traverse();
            });
        }
    }
}]);


/***************************************************************************************************
 * Service : Validation
 *
 ***************************************************************************************************/
atlantisUi.factory('Validation',function($rootScope){
    var serviceProvider = {
        $error: {
            controls: {
                $count: 0,
                $valid: true
            },
            cards: {}
        },
        cards: {},
        controls: [],
        config: {
            validate: true
        },
        $refresh: function(){
            /** Count error for all card */
            angular.forEach(serviceProvider.cards, function(card,key){
                var error_card_count = 0;

                angular.forEach(card, function(control,key){
                    if( control.$invalid ) {
                        error_card_count++;
                    }
                },[error_card_count]);

                if( serviceProvider.$error.cards[key] !== undefined ){
                    serviceProvider.$error.cards[key].$count = error_card_count;

                    //i: Validity
                    if( error_card_count > 0 ){
                        serviceProvider.$error.cards[key].$valid = false
                    }else{
                        serviceProvider.$error.cards[key].$valid = true
                    }
                }
            });

            return this.$update();
        },
        $update: function(){
            var error_count = 0;

            //i: Count error for all control
            angular.forEach(serviceProvider.controls, function(control){
                if( control.$invalid ) error_count++;
            },[error_count]);


            //i: Error count
            serviceProvider.$error.controls.$count = error_count;

            //i: Validity
            if( error_count > 0 ){
                serviceProvider.$error.controls.$valid = false
            }else{
                serviceProvider.$error.controls.$valid = true
            }

            $rootScope.$broadcast('atlantis.validation.update',serviceProvider.$error);

            return error_count;
        }
    };

    /** Listen on element validation init */
    $rootScope.$on('atlantis.ui.validation.init',function(e,elem){
        var model_controller = angular.element(elem).data('$ngModelController');

        /** Push control into array */
        serviceProvider.controls.push(model_controller)
    });

    /** Listen on card init */
    $rootScope.$on('atlantis.ui.card.init',function(e,card){
        serviceProvider.cards[card] = [];

        /** Traverse all control to check card child */
        angular.forEach(serviceProvider.controls, function(control){
            if( control.card == card ){
                serviceProvider.cards[card].push(control)
                serviceProvider.$error.cards[card] = {
                    $count:0,
                    $valid:true
                }
            }
        },[card]);
    });

    $rootScope.$on('atlantis.ui.model.parsed',function(e,elem){
        //i: Refresh, should be improved without traversing
        serviceProvider.$refresh()
    });

    $rootScope.$on('atlantis.ui.model.formatted',function(e,elem){
        //i: Refresh, should be improved without traversing
        serviceProvider.$refresh()
    });

    $rootScope.$on('atlantis.ui.model.rendered',function(e,elem){
        //i: Refresh, should be improved without traversing
        serviceProvider.$refresh()
    });

    return serviceProvider;
});


/***************************************************************************************************
 * Service : Debounce
 *
 ***************************************************************************************************/
atlantisUi.factory('Debounce', ['$timeout','$q', function($timeout, $q) {
    return function(func, wait, immediate) {
        var timeout;
        var deferred = $q.defer();
        return function() {
            var context = this, args = arguments;
            var later = function() {
                timeout = null;
                if(!immediate) {
                    deferred.resolve(func.apply(context, args));
                    deferred = $q.defer();
                }
            };

            var callNow = immediate && !timeout;
            if ( timeout ) {
                $timeout.cancel(timeout);
            }

            timeout = $timeout(later, wait);

            if (callNow) {
                deferred.resolve(func.apply(context,args));
                deferred = $q.defer();
            }
            return deferred.promise;
        };
    };
}]);