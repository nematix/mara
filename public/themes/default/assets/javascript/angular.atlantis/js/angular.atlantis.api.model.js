/*
 * File:		atlantis.model.js
 * Version:    	1.0
 * Package: 	Core
 * Module: 		Angular
 * Function:	Provide a Model API and functionality.
 * Author:     	Azri Jamil | azri{at}nematix.com
 * Info:       	github.com/atlantisx
 *
 * Copyright 2012 Nematix Technology, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license.
 *
 * This source file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 *
 *
 */


angular.module('service.api.model', ['restmod','service.api.rpc'])
    .config(function(restmodProvider) {
        restmodProvider.rebase({
            $config: {
                urlPrefix: appBase + 'api/',
                style: 'Atlantis',
                primaryKey: 'id',
                jsonMeta: 'meta',
                jsonLinks: 'links'
            }
        });
    })
    .factory('Model', function(restmod,Rpc) {

        var modelProvider = function(modelName,params){
            this.params = {
                version: 1
            };
            this.modelName = modelName;
            var self = this;

            /** Check default params supplied */
            if( typeof params == 'object' ){
                angular.extend(this.params,params);
            }

            //@info API path & version prefix
            //var versionPrefix = this.params.version ? 'v'+this.params.version+'/' : '/';
            //var apiRelativePath = modelName;

            return restmod.model(modelName)
                .mix({

                    /** Notification hook */
                    $hooks: {
                        'after-save': function(){
                            if( this._status ) _a.alert.server(this);
                        },
                        'before-request': function(request){
                            request = angular.extend(request, {'headers':{'version':self.params.version}});
                        }
                    },

                    /** Rpc extension */
                    'Record.$rpc': function(methodName,parameters){
                        var dispatcher = this.$dispatcher();
                        var that = this;

                        /** Create Rpc service and execute requested method */
                        var modelRpc = Rpc.create(modelName,{serviceUrl:this.$url()});
                        modelRpc.execute(methodName,parameters).success(function(response){
                            /** Dispatch restmod hook */
                            /*that.$decorate(dispatcher, function(){
                                that.$dispatch('after-save');
                            });*/

                            _a.alert.rpc(response);
                        });
                    }

                    //@todo Add authentication, anynomous function??
                });
        };

        return {
            create: function(modelName,params){
                return new modelProvider(modelName,params);
            }
        };

    });