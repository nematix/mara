/*
 * File:		atlantis.preloader.js
 * Version:    	1.0
 * Package: 	Component\Atlantis
 * Module: 		Atlantis Preloader
 * Function:		  
 * Author:     	Azri Jamil | azri{at}nematix.com
 * Info:       	system.nematix.com/atlantis
 * 
 * Copyright 2012 Nematix Technology, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license.
 * 
 * This source file is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 * 
 * 
 * Credits :-
 * 
 * Original author of Sticky v1.0
 * Daniel Raftery - http://thrivingkings.com/sticky
 * 
 */


var atlantisProgress = angular.module('atlantis.progress', []);


/***************************************************************************************************
 * Service : Progress
 *
 ***************************************************************************************************/
atlantisProgress.value('Progress',{
    controls: [],
    actions: {
        ladda: {
            start: function(control){
                if( typeof Ladda != undefined ){
                    //i: Default
                    if( control.target.attr('data-style') == undefined ) control.target.attr('data-style', 'zoom-out');

                    //i: Init & start ladda
                    var ladda = Ladda.create(control.target[0]);
                    ladda.start();

                    //i: Save ladda object to control
                    angular.extend(control,{ladda:ladda});
                }

                return true;
            },
            stop: function(control){
                if( typeof Ladda != undefined ){
                    //i: Stop ladda
                    control.ladda.stop();
                }

                return true;
            }
        },
        spinner: {
            start: function(control){
                if( typeof Spinner != undefined ){
                    var lines = 12, height = 20, radius = height * .2, length = radius * .6, width = radius < 7 ? 2 : 3;
                    var options = {
                        color: "#ccc",
                        lines: lines,
                        radius: radius,
                        length: length,
                        width: width,
                        zIndex: "auto",
                        top: "auto",
                        left: "auto"
                    };

                    var spinner = new Spinner(options).spin();
                    control.target.after(spinner.el);
                }
                return true;
            },
            stop: function(control){
                if( control.target.next().hasClass('spinner') ) control.target.next().remove();

                return true;
            }
        }
    }
});


/***************************************************************************************************
 * HTTP Interceptor
 *
 ***************************************************************************************************/
atlantisProgress.provider('progressHttpInterceptor', function() {
    this.$get = function(){
        var self = this;

        return {
            transformRequest: function(data){
                var $injector = angular.injector(['atlantis.progress']);
                var progress = $injector.get('Progress')

                angular.forEach(progress.controls, function(control){
                    var progress_object = self.progress_type[control.type];

                    if( progress_object != undefined ){
                        progress_object.start(control);
                    }
                });

                return data;
            },
            transformResponse: function(data){
                //i: Get module injector
                var $injector = angular.injector(['atlantis.progress']);
                var progress = $injector.get('Progress')

                //i: Iterate all control
                angular.forEach(progress.controls, function(control,key){
                    //i: Getting progress action object
                    var progress_action = progress.actions[control.type];

                    //i: If action exist invoke stop method
                    if( progress_action != undefined ){
                        progress_action.stop(control);

                        //i: Remove control from queue
                        progress.controls.splice(key,1);
                    }
                });

                return data;
            }
        }
    };
});



/***************************************************************************************************
 * Built-In Sticky v1.0.1
 * 
 * Changelog:-
 * 
 * ADDED : $sticky.clear() function to clear all the stickiness.
 * 
 ***************************************************************************************************/
(function ($) {

	// Using it without an object
    $.sticky = function (note, options, callback) {

        return $.fn.sticky(note, options, callback);
    };

    $.fn.sticky = function (note, options, callback) {
        // Default settings
        var position = 'top-right'; // top-left, top-right, bottom-left, or bottom-right
        
        var settings = {
            'speed': 'fast', // animations: fast, slow, or integer
            'duplicates': true, // true or false
            'autoclose': 5000 // integer or false
        };

        // Passing in the object instead of specifying a note
        if (!note) {
            note = this.html();
        }

        if (options) {
            $.extend(settings, options);
        }

        // Variables
        var display = true;
        var duplicate = 'no';

        // Somewhat of a unique ID
        var uniqID = Math.floor(Math.random() * 99999);

        // Handling duplicate notes and IDs
        $('.sticky-note').each(function () {
            if ($(this).html() == note && $(this).is(':visible')) {
                duplicate = 'yes';
                if (!settings['duplicates']) {
                    display = false;
                }
            }
            if ($(this).attr('id') == uniqID) {
                uniqID = Math.floor(Math.random() * 9999999);
            }
        });

        // Make sure the sticky queue exists
        if (!$('body').find('.sticky-queue').html()) {
            $('body').append('<div class="sticky-queue ' + position + '"></div>');
        }

        // Can it be displayed?
        if (display) {
            // Building and inserting sticky note
            $('.sticky-queue').prepend('<div class="sticky border-' + position + '" id="' + uniqID + '"></div>');
            //$('#' + uniqID).append('<img src="close.png" class="sticky-close" rel="' + uniqID + '" title="Close" />');
            $('#' + uniqID).append('<div class="sticky-note" rel="' + uniqID + '">' + note + '</div>');

            // Smoother animation
            var height = $('#' + uniqID).height();
            $('#' + uniqID).css('height', height);

            //$('#' + uniqID).slideDown(settings['speed']);
            $('#' + uniqID).show();
            
            display = true;
        }

        // Listeners
        $('.sticky').ready(function () {
            // If 'autoclose' is enabled, set a timer to close the sticky
            if (settings['autoclose']) {
                $('#' + uniqID).delay(settings['autoclose']).fadeOut(settings['speed']);
            }
        });
        // Closing a sticky
        $('.sticky-close').click(function () {
            $('#' + $(this).attr('rel')).dequeue().fadeOut(settings['speed']);
        });


        // Callback data
        var response = {
            'id': uniqID,
                'duplicate': duplicate,
                'displayed': display,
                'position': position
        }

        // Callback function?
        if (callback) {
            callback(response);
        } else {
            return (response);
        }
    }

    $.sticky.clear = function(){
    	$('.sticky-queue').children().each(function(){
    		$(this).dequeue().fadeOut('fast');
    	})
    	//$('.sticky-queue').dequeue().fadeOut(settings['speed']);
    }

})(jQuery);