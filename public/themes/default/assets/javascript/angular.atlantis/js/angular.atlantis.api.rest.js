/*
 * File:		atlantis.rest.js
 * Version:    	1.0
 * Package: 	Core
 * Module: 		Angular
 * Function:	Provide a REST API and functionality.	  
 * Author:     	Azri Jamil | azri{at}nematix.com
 * Info:       	system.nematix.com/atlantis
 * 
 * Copyright 2012 Nematix Technology, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license.
 * 
 * This source file is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 * 
 * 
 */


/***************************************************************************************************
 * API REST Service
 *
 ***************************************************************************************************/
angular.module('service.api.rest', ['ngResource']).
    factory('Rest', function($rootScope,$resource,$q){
        return $resource(
            appBase + 'api/:version/:model/:id',
            {version:'v1', model:'table'},
            {
                index: {method:'GET', isArray:true},

                get: {method:'GET', param: {id:'@id'}, interceptor : {
                    response: function(response){
                        $rootScope.$broadcast('api.get');
                        return response || $q.when(response);
                    }
                }},

                save: {method:'POST', interceptor : {
                    response: function(response){
                        if(_a) _a.alert.server(response.data);
                        $rootScope.$broadcast('api.save');
                        return response || $q.when(response);
                    },
                    responseError: function(response){
                        if(_a) _a.alert.server(response.data);
                        $rootScope.$broadcast('api.save');
                        return response || $q.when(response);
                    }
                }},

                update: {method:'PUT', interceptor : {
                    response: function(response){
                        if(_a) _a.alert.server(response.data);
                        $rootScope.$broadcast('api.update');
                        return response || $q.when(response);
                    },
                    responseError: function(response){
                        if(_a) _a.alert.server(response.data);
                        $rootScope.$broadcast('api.update');
                        return response || $q.when(response);
                    }
                }},

                destroy: {method:'DELETE', interceptor : {
                    response: function(response){
                        if(_a) _a.alert.server(response.data);
                        $rootScope.$broadcast('api.destroy');
                        return response || $q.when(response);
                    },
                    responseError: function(response){
                        if(_a) _a.alert.server(response.data);
                        $rootScope.$broadcast('api.destroy');
                        return response || $q.when(response);
                    }
                }}
            }
        );
    });


/***************************************************************************************************
 * API Resource Service
 *
 * Resource controllers make it easier to build RESTful controllers around resources.
 * For dynamic parameter, model could modified params variable directly from return
 * Resource object. This service is not singleton.
 ***************************************************************************************************/
angular.module('service.api.resource',[]).
    factory('Resource', function($http){

        var serviceProvider = function(url, params){
            this.params = {};
            this.url = url;
            this.headers = { 'Content-Type': 'application/x-www-form-urlencoded' };

            //[i] Check default params supplied
            if( typeof params == 'object' ){
                this.params = params;
            }
        };

        serviceProvider.prototype.index = function(callback){
            params = $.param(this.params) ? '?'+$.param(this.params) : '';
            return $http({
                method: 'GET',
                url: this.url+params,
                headers : this.headers
            }).success(function(response){
                if( typeof callback == 'function' ) callback(response);

            }).error(function(response){
                _a.alert.server(response);
            });
        };

        serviceProvider.prototype.show = function(value,callback){
            params = $.param(this.params) ? '?'+$.param(this.params) : '';
            $http({
                method: 'GET',
                url: this.url + '/' + value + params,
                headers : this.headers

            }).success(function(response){
                if( typeof callback == 'function' ) callback(response);

            }).error(function(response){
                _a.alert.server(response);
            });
        };

        serviceProvider.prototype.store = function(callback){
            $http({
                method: 'POST',
                url: this.url,
                data: $.param(this.params),
                headers : this.headers

            }).success(function(response){
                _a.alert.server(response);
                if( typeof callback == 'function' ) callback(response);

            }).error(function(response){
                _a.alert.server(response);
            });
        };

        serviceProvider.prototype.update = function(value,callback){
            $http({
                method: 'PUT',
                url: this.url + '/' + value,
                data: $.param(this.params),
                headers : this.headers

            }).success(function(response){
                _a.alert.server(response);
                if( typeof callback == 'function' ) callback(response);

            }).error(function(response){
                _a.alert.server(response);
            });
        };

        var serviceFactory = {
            create: function(url, params){
                return new serviceProvider(url, params)
            }
        };

        return serviceFactory;
    });