/*
 * File:		angular.atlantis.api.rpc.js
 * Version:    	1.0
 * Package: 	Core
 * Module: 		Angular
 * Function:	Provide a dialog / prompt API and functionality.
 * Author:     	Azri Jamil | azri{at}nematix.com
 * Info:       	github.com/atlantisx
 *
 * Copyright 2012 Nematix Technology, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license.
 *
 * This source file is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 *
 *
 */


/***************************************************************************************************
 * API Rpc Service
 *
 ***************************************************************************************************/
/*
    Parameters:
        service - Service name
        options - Optionals

    Usage:
        var ping = Rpc.create('Ping');
        user.execute('echo',{max: 10}).success(function(data){
             // POST /api/ping
             // {"method": "echo", "params": {"max": 10}, "id": 1}
        });
*/

angular.module('service.api.rpc',[])
    .factory('Rpc', function($http){
        var defaults = this.defaults = {
            prefix: "api/"
        };

        /**
         * RPC Provider
         *
         * @param {string} service The name of the service.
         * @param {string} options Optionals.
         * @constructor
         */
        var rpcProvider = function(service, options) {
            angular.extend(defaults,options);
            this.service = service.toLowerCase();
        }

        /**
         * Execute RPC Method
         *
         * @param {string} methodName Method name.
         * @param {object} parameters Method parameters
         * @param {angular.$http.Config=} config HTTP config.
         * @return {function(*):angular.$http.HttpPromise} An implementation for the service method.
         */
        rpcProvider.prototype.execute = function(methodName, parameters, config) {
            var serviceUrl = defaults.serviceUrl ? defaults.serviceUrl : appBase + defaults.prefix + this.service;

            return $http.jsonrpc(serviceUrl, methodName, parameters, config);
        };

        return {
            create: function(serviceName, options){
                return new rpcProvider(serviceName, options)
            }
        };

    });


/***************************************************************************************************
 * HTTP JSON Rpc Service
 *
 ***************************************************************************************************/

/*
    Parameters :
        url, method, parameters, config

    Usage:
        $http.jsonrpc('api/v1/ping', 'echo', {"max":10}).success(function(){}).error(function(){});
*/

angular.module('service.http.jsonrpc', []).config([ "$provide", function($provide) {

    return $provide.decorator('$http', ['$delegate', function($delegate){

        /** Extending $http through decoration */
        $delegate.jsonrpc = function(url, method, parameters, config){
            /** Check for url */
            url = !url ? appBase + 'api/' : url;

            var data = {
                jsonrpc: '2.0',
                method: method,
                params: parameters,
                id: 1
            }

            /** Configure transform response override */
            config = configTransformOverride(config);

            /** Execute $http post and return promise */
            return $delegate.post(url, data, angular.extend({'headers':{'Content-Type': 'application/json'}}, config) );
        };

        var configTransformOverride = function(config){
            /** Get all current transformResponse object */
            var transforms = [];
            angular.forEach($delegate.defaults.transformResponse, function(t) {
                transforms.push(t);
            });

            /** Add transformResponse override result check */
            transforms.push(function(data) {
                /** if response id is integer return result/error */
                //return typeof data.id == 'number' ? data.result || data.error : null;
                return typeof data.id == 'number' ? data : null;
            });

            /** Check transformResponse override from config if any */
            config = config || {};
            var configTransforms = config.transformResponse;
            if (angular.isArray(configTransforms)) {
                /** If array then merge */
                [].push.apply(transforms, configTransforms);

            } else if (angular.isFunction(configTransforms)) {
                /** if function then push */
                transforms.push(configTransforms);
            }
            config.transformResponse = transforms;

            return config;
        };

        return $delegate;
    }]);

}]);


/***************************************************************************************************
 * Wire Service
 * Direct connection with back-end controller
 *
 ***************************************************************************************************/
angular.module('wire.service', ['ngResource']).
    factory('Wire', function($rootScope,$resource,$q){
    });