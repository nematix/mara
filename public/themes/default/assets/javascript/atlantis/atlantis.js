/*
 * File:		atlantis.js
 * Version:    	1.0
 * Package: 	Core
 * Module: 		Core
 * Function:	Main API and functionality.	  
 * Author:     	Azri Jamil | azri{at}nematix.com
 * Info:       	system.nematix.com/atlantis
 * 
 * Copyright 2012 Nematix Technology, all rights reserved.
 *
 * This source file is free software, under either the GPL v2 license or a
 * BSD style license.
 * 
 * This source file is distributed in the hope that it will be useful, but 
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
 * or FITNESS FOR A PARTICULAR PURPOSE. See the license files for details.
 * 
 * 
 */


(function(window, undefined) {
	
	var atlantis = (function() {
		var atlantis = function( selector ) {
			return new atlantis.fn.init( selector );
		};
			
		atlantis.fn = atlantis.prototype = {
				init: function( selector ) {
					var nodes = $(selector).toArray();
					for (var i = 0; i < nodes.length; i++) {
			            this[i] = nodes[i];
			        }
					this.length = nodes.length;

					return this;
				},
				version: "0.0.1"
		}
		
		atlantis.fn.init.prototype = atlantis.fn;
		return (window.atlantis = window._a = atlantis);
	})();
	
	atlantis.utils = {
		validateJSON: function(json_string){
			try {
				var a = JSON.parse(json_string);
				return true;
			} catch(e){
				return false;
			}
		},
		JSONToParam: function(json_object){
			var params = '';
			$.each(json_object, function(key,value){
				params += '&' + key + '=' + value;
			})
			
			return params;
		},
		URLSanitize: function(url, data){

			url_array = url.split('?');
			url_array = _.compact(url_array);
			console.log(url);
			if( url_array.length < 1 ) return false;
			
			data_array = data.split('&');
			data_array = _.compact(data_array);
			
			if( url_array.length == 1 ){
				url = url_array[0] + '?' + data_array.join('&');
			} else {
				url = url_array.shift();
				url = url + '?' + url_array.concat(data_array).join('&');
			}
			
			return url;
		},
        numberToWord: function(number){
            var names = [
                {"0":"zero","1":"one","2":"two","3":"three","4":"four","5":"five","6":"six","7":"seven","8":"eight","9":"nine" },
                {"0":"ten","1":"eleven","2":"twelve","3":"thirteen","4":"fourteen","5":"fifteen","6":"sixteen","7":"seventeen","8":"eighteen","9":"nineteen"},{"2":"twenty","3":"thirty","4":"forty","5":"fifty","6":"sixty","7":"seventy","8":"eighty","9":"ninety"},
                ["","thousand","million","billion","trillion","quadrillion","quintillion","sextillion","septillion","octillion","nonillion","decillion","undecillion","duodecillion","tredecillion","quattuordecillion", "quindecillion","sexdecillion","septdecillion","octdecillion","novemdecillion","vigintillion"]
            ];

            var to_words = function(s, n){
                var ns = s.slice(0,3);
                return (ns.length < 1) ? "" : to_words(s.slice(3, s.length), n + 1) + ((ns.length > 1) ? ((ns.length == 3 && ns[2] != "0") ? names[0][ns[2]] + " hundred " + ((ns[1] == "1") ? names[1][ns[0]] + " " : (ns[1] != "0") ? names[2][ns[1]] + " " + ((ns[0] != "0") ? names[0][ns[0]] + " " : "") : (ns[0] != "0" ? names[0][ns[0]] + " " : "")) : ((ns[1] == "1") ? names[1][ns[0]] + " " : (ns[1] != "0") ? names[2][ns[1]] + " " + ((ns[0] != "0") ? names[0][ns[0]] + " " : "") : (ns[0] != "0" ? names[0][ns[0]] + " " : ""))) + (((ns.length == 3 && (ns[0] != "0" || ns[1] != "0" || ns[2] != "0")) || (ns.length == 2 && (ns[0] != "0" || ns[1] != "0")) || (ns.length == 1 && ns[0] != "0")) ? "<span class='magnitude'>" + names[3][n] + "</span> " : "") : ((ns.length == 1 && ns[0] != "0") ? names[0][ns[0]] + " " : "") + (((ns.length == 3 && (ns[0] != "0" || ns[1] != "0" || ns[2] != "0")) || (ns.length == 2 && (ns[0] != "0" || ns[1] != "0")) || (ns.length == 1 && ns[0] != "0")) ? "<span class='magnitude'>" + names[3][n] + "</span> " : ""));
            };

            to_words(number.replace(/[^0-9]/g, '').split('').reverse(), 0);

            //document.getElementById('input').addEventListener('keyup', function(){
            //    document.getElementById('output').innerHTML = to_words(this.value.replace(/[^0-9]/g, '').split('').reverse(), 0);
            //}, false);
        }
	};

    atlantis.url = {
        params: function(name){
            var queries = {};

            if(document.location.search == '') return undefined;

            $.each(document.location.search.substr(1).split('&'), function(c,q){
                var i = q.split('=');
                queries[i[0].toString()] = i[1].toString();
            });

            if( typeof name === 'undefined' ){
                return queries;
            } else {
                return queries[name];
            }
        }
    };
	
	function getIO(lib_name){
		var $this = $(this);
		data = $this.data(lib_name);
		return data.options;
	}
    
	function setIO(lib_name, io){ 
		var $this = $(this);
		data = $this.data(lib_name);
		
		if ( !data ) {
			$this.data(lib_name, {
		       target : $this,
		       options : io
			});
		}
	}

})(window);


