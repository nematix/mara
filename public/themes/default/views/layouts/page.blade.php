@extends('themes/default::layouts.fixed')

@section('stylesheet')
    @parent
    <style>
        #wrap {
            background: url({{ url('themes/core/assets/images/bg-public.jpg') }}) fixed center;
        }
    </style>
@stop

@section('content')
    <div class="row header">
        <div class="col-lg-6 col-xs-12">
            <a href="{{ url('/') }}/"><img src="{{ asset('assets/img/logo.png') }}"></a>
        </div>
        <div class="col-lg-6 hidden-xs" style="margin-top: 30px;">
            @section('menu-top')
            <ul class="nav nav-pills navbar-right">
                <li><a href="{{ url('public/page/eligibility') }}">Syarat Kelayakan</a></li>
                <li><a href="{{ url('public/page/guide') }}">Panduan Permohonan</a></li>
                <li><a href="{{ url('/') }}/">Laman Utama</a></li>
            </ul>
            @show
        </div>
    </div>

    <div class="panel panel-default" style="margin-bottom: 0;">
        <div class="panel-heading">
            <div>Sistem EPendahuluan</div>
        </div>
        <div class="panel-body padded">
            @yield('page')
        </div>
    </div>

    <div class="row visible-md visible-lg">
        <div class="col-md-12">
            @section('menu-bottom')
            <ul class="nav nav-pills navbar-mini navbar-right">
                <li><a href="{{ url('public/page/terms') }}">Terma</a></li>
                <li><a href="{{ url('public/page/privacy') }}">Dasar Privasi</a></li>
                <li><a href="http://www.mara.gov.my/c/document_library/get_file?uuid=4c758742-f993-4ab5-b1cd-7da5c67cee2a&groupId=30564">Notis Privasi</a></li>
                <li><a href="{{ url('public/page/safety') }}">Dasar Keselamatan</a></li>
                <li><a href="{{ url('public/page/disclaimer') }}">Penafian</a></li>
                <li><a href="{{ url('user/login/staff') }}">Login Pegawai</a></li>
            </ul>
            @show
        </div>
    </div>
@stop