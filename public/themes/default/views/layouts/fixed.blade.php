@section('stylesheet')
    @parent
    @stylesheets('layout-fixed')
@stop

@section('base')
<div id="wrap" class="fixed">
    @section('container')
    <div class="container">
        <div class="col-md-8 col-md-offset-2">
            @yield('content')
        </div>
    </div>
    @show
</div>
@stop

@section('javascript')
    @parent
    @javascripts('layout-fixed')
@stop