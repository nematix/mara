@section('stylesheet')
    @parent
    @stylesheets('layout-fixed-box')
@stop

@section('base')
<div id="wrap" class="fixed-box">
    <div class="container">
        <div class="col-lg-4 col-lg-offset-4">
            @section('box-before')
            @show
            @section('box')
            <div class="box">
                <div class="box-header">
                    @yield('box-header')
                </div>
                <div class="box-content padded">
                    @include('core::partials.error')
                    @yield('box-content')
                </div>
            </div>
            @show
            @section('box-after')
            @show
        </div>
    </div>
</div>
@stop

@section('javascript')
    @parent
    @javascripts('layout-fixed-box')
@stop