@section('stylesheet')
    @parent
    @stylesheets('layout-fluid')
@stop

@section('navbar')
    @include('admin::partials.navbar')
@stop

@section('base')
    @if( isset($sidebar) )
    <div class="main-content">
        @section('container')
        <div class="container">
        </div>
        @show
    </div>
    @else
    <div id="wrap">
        @section('container')
        <div class="container">
        </div>
        @show
    </div>
    @endif
@show

@section('javascript')
    @parent
    @javascripts('layout-fluid')
@stop