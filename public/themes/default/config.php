<?php return array(

    'info' => array(
        "name"          => "Default",
        "author"        => "Azri Jamil",
        "description"   => "Atlantis Default Theme",
        "version"       => "1.0.0"
    ),

    'register' => array(
        'prefixes' => array(
            'component'  => public_path() . '/components/'
        ),
        'attributes' => array(

        )
    ),

    'assets' => array(
        'default' => array(
            'storage'   => 'file',
            'path'      => app('atlantis.theme')->base_path() . '/default/assets'
        ),

        'stylesheet' => array(
            'component!bootstrap/css/bootstrap.css',
            'component!bootstrap/css/bootstrap-theme.css',
            'component!jquery-ui/themes/base/base.css',
            'component!jquery-ui/themes/base/theme.css',
            'component!font-awesome/css/font-awesome.css',
            'javascript/jquery.touch/jquery.touch.css',
            'stylesheet/font.opensans/font.opensans.css',
            'javascript/atlantis/css/*.css',
            'javascript/angular.atlantis/css/*.css',
            'stylesheet/theme.elements/*.less',
            'stylesheet/theme.components/{*.css,*.less}',
            'stylesheet/theme.widgets/*.less',
            'stylesheet/theme.layouts/*.less'
        ),
        'javascript' => array(
            'component!jquery/jquery.js',
            'component!jquery/jquery-migrate.js',
            'component!jquery-ui/jquery-ui-built.js',
            'component!bootstrap/js/bootstrap.js',
            'component!angularjs/angular.js',
            'component!angularjs/angular-resource.js',
            'component!underscore/underscore.js',

            #i: Atlantis-Angular Modules
            'javascript/angular.atlantis/js/angular.atlantis.filters.js',
            'javascript/angular.atlantis/js/angular.atlantis.api.rest.js',
            'javascript/angular.atlantis/js/angular.atlantis.api.rpc.js',
            'javascript/angular.atlantis/js/angular.atlantis.api.model.js',
            'javascript/angular.atlantis/js/angular.atlantis.progress.js',
            'javascript/angular.atlantis/js/angular.atlantis.ui.js',
            'javascript/angular.atlantis/angular.atlantis.js',

            #i: Atlantis Core
            'javascript/atlantis/atlantis.js',
            'javascript/atlantis/js/atlantis.alert.js',

            'javascript/jquery.icheck/*.js',
            'javascript/jquery.select2/jquery.select2.js',
            'javascript/jquery.validation/jquery.validationEngine.js',
            'javascript/jquery.validation/jquery.validationEngine-'.app('config')->get('app.locale').'.js',
            'javascript/jquery.datatables/jquery.datatables.js',
            'javascript/jquery.datatables/jquery.datatables.responsive.js',
            'javascript/jquery.noty/jquery.noty.js',
            'javascript/jquery.noty/layouts/topRight.js',
            'javascript/jquery.noty/themes/default.js',
            'javascript/jquery.touch/jquery.touch.js',

            'javascript/bootstrap.ladda/spin.js',
            'javascript/bootstrap.ladda/bootstrap.ladda.js',
            'javascript/bootstrap.wysihtml/*.js',
            'javascript/bootstrap.typeahead/*.js',

            'javascript/angular.ui-bootstrap/angular.ui-bootstrap.js',
            'javascript/angular.xeditable/js/xeditable.js',
            'javascript/angular.ui-select2/angular.ui-select2.js',
            'javascript/angular.restmod/angular.restmod.js',

            'javascript/moment/moment.js'
        )
    )

);