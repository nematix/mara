<?php return array(

    'info' => array(
        "inherit"       => "default",
        "name"          => "Admin",
        "author"        => "Azri Jamil",
        "description"   => "Admin Theme",
        "version"       => "1.0.0"
    ),

    'register' => array(
        'prefixes' => array(
        ),
        'attributes' => array(
        )
    ),

    'assets' => array(
        'default' => array(
            'storage'   => 'file',
            'path'      => app('atlantis.theme')->base_path() . '/core/assets'
        ),

        'stylesheet' => array(
            'javascript/jquery.customscroll/jquery.customscroll.css',
            'javascript/bootstrap.dialog/bootstrap.dialog.css',
            'stylesheet/jquery.ui/*css',
            'stylesheet/theme.components/{*.css,*.less}',
            'stylesheet/theme.elements/{*.css,*.less}',
            'stylesheet/theme.widgets/{*.css,*.less}',
            'stylesheet/theme.overrides/{*.css,*.less}',
            'stylesheet/theme.layouts/{*.css,*.less}',
            'stylesheet/theme.responsive/{*.css,*.less}'
        ),
        'javascript' => array(
            'javascript/jquery.customscroll/jquery.customscroll.js',
            'javascript/jquery.fineuploader/jquery.fineuploader-4.3.1.js',
            'javascript/jquery.psteps/*.js',
            'javascript/jquery.maskedinput/*.js',
            'javascript/bootstrap.dialog/bootstrap.dialog.js',
            'javascript/packery/packery.js'
        )
    )

);